<?php

namespace App;

use App\Models\Lists;
use App\Models\UserItem;
use App\Models\UserWatch;

class UserWatchObserver
{
    public function created(UserWatch $model)
    {
        $userItem = UserItem::query()
            ->firstOrNew(
                ['user_id' => $model->user_id, 'item_id' => $model->item_id],
                ['list_id' => Lists::ID_WATCH]
            );
        $seasonNumber = $model->video->itemSeason->number;
        $seriesNumber = $model->video->itemSeries->number;
        $hasChanges = false;
        if ($userItem->season_number < $seasonNumber) {
            $hasChanges = true;
            $userItem->season_number = $seasonNumber;
        }
        if (
            $userItem->season_number <= $seasonNumber
            && $userItem->series_number < $seriesNumber
        ) {
            $hasChanges = true;
            $userItem->series_number = $seriesNumber;
        }
        $hasChanges && $userItem->save();
    }
}
