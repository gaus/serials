<?php

namespace App\Console;

use App\Console\Commands\AutoListParser;
use App\Console\Commands\AutoParser;
use App\Console\Commands\Check404Video;
use App\Console\Commands\ItemAggregate;
use App\Console\Commands\ParserApply;
use App\Console\Commands\ParserDoramy;
use App\Console\Commands\ParserKadu;
use App\Console\Commands\ParserKinopoisk;
use App\Console\Commands\ParserLostfilmhd720;
use App\Console\Commands\ParserRosserial;
use App\Console\Commands\ParserYadoma;
use App\Console\Commands\ParserYandex;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ItemAggregate::class,
        Check404Video::class,
        AutoParser::class,
        AutoListParser::class,
        ParserKinopoisk::class,
        ParserRosserial::class,
        ParserDoramy::class,
        ParserLostfilmhd720::class,
        ParserYandex::class,
        ParserYadoma::class,
        ParserApply::class,
        ParserKadu::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command(AutoParser::class)
             ->everyMinute()
             ->withoutOverlapping();

         $schedule->command(AutoListParser::class)
             ->hourly()
             ->withoutOverlapping();

         //Отключил проверку из-за того что много ложных срабатываний
//         $schedule->command(Check404Video::class)
//             ->everyMinute()
//             ->withoutOverlapping();

         $schedule->command(ItemAggregate::class)
             ->hourly()
             ->withoutOverlapping();

         $schedule->command(ParserApply::class)
             ->dailyAt('05:00')
             ->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
