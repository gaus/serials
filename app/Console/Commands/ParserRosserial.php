<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\Parser;
use App\Models\ParserItem;
use App\Parsers\Rosserial;
use Illuminate\Console\Command;

class ParserRosserial extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:rosserial';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse items from rosserial';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        /** @var Parser $parser */
//        $parser = Parser::query()->find(3);
//        $parser->parser->setConsole($this);
//        $parser->parser->parseItems();

//        $limit = 5;
        /** @var ParserItem[] $parserItems */
        $parserItems = ParserItem::query()
//            ->where('id', 16614)
            ->where('parser_id', 3)
            ->whereNull('last_activity')
            ->orderBy('id')
//            ->limit($limit)
            ->get();
        $count = count($parserItems);
        foreach ($parserItems as $n => $parserItem) {
            $n++;
            $this->line("{$n} / {$count}");
            $parserItem->parser->parser->setConsole($this);
            $parserItem->parser->parser->parseItem($parserItem);
            $parserItem->saveLastActivity();
            $this->line('');
        }
    }
}
