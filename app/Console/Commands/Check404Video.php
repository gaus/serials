<?php

namespace App\Console\Commands;

use App\Models\ItemVideo;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Check404Video extends Command
{
    protected $signature = 'check404video {limit?}';

    protected $description = '';

    public function handle(): void
    {
        $limit = $this->argument('limit') ?? 50;
        /** @var ItemVideo[] $videos */
        $videos = ItemVideo::query()
            ->orderByRaw('checked_404_at asc NULLS FIRST')
            ->orderBy('id')
            ->limit($limit)
            ->get();
        $progress = $this->output->createProgressBar(count($videos));
        $progress->start();
        foreach ($videos as $video) {
            sleep(1);
            if ($this->checkUrl($video->src)) {
                $video->has_404 = null;
            } else {
                $video->has_404++;
            }
            $video->checked_404_at = DB::raw('NOW()');
            $video->save();
            $progress->advance();
        }
        $progress->finish();
    }

    protected function checkUrl(string $url): bool
    {
        $ch = curl_init();
        if (starts_with($url, '//')) {
            $url = "http:$url";
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
            'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
            'Referer: ' . env('APP_URL'),
            'Connection: keep-alive',
            'Upgrade-Insecure-Requests: 1',
        ]);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $statusCode = $info['http_code'];

        return $statusCode === 200 && $result;
    }
}
