<?php

namespace App\Console\Commands;

use App\Models\Parser;
use Illuminate\Console\Command;

class AutoListParser extends Command
{
    protected $signature = 'auto-list-parser';

    protected $description = 'Schedule auto list parser task';

    public function handle(): void
    {
        /** @var Parser $parser */
        $parser = Parser::query()
            ->where('is_active', true)
            ->whereRaw("(last_activity < NOW() - interval '7 day' OR last_activity ISNULL)")
            ->orderByRaw('last_activity NULLS FIRST')
            ->first();
        if ($parser) {
            $parser->parser->parseItems();
            $parser->saveLastActivity();
        }
    }
}
