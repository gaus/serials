<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\Parser;
use App\Models\ParserItem;
use App\Models\ParserValueAlias;
use App\Parsers\KinopoiskParser;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ParserApply extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:apply';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse items from kinopoisk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var KinopoiskParser $kinopoiskParser */
        $kinopoiskParser = Parser::find(1)->parser;
        $query = ParserItem::query()
            ->where('parser_id', '!=', 1)
            ->whereNull('item_id')
            ->orderBy('id')
            ->with(['item', 'parserItemEvents']);
        $total = $query->count();
        $allFound = $created = 0;
        $progress = $this->output->createProgressBar($total);
        $progress->start();
        $limit = 1000;
        $offset = 0;
        do {
            /** @var ParserItem[] $parserItems */
            $parserItems = $query
                ->limit($limit)
                ->offset($offset)
                ->get();
            $offset += $limit;
            foreach ($parserItems as $parserItem) {
                $data = $parserItem->mergedParserItemEventsData();
                $items = [];
                if (!empty($data['data']['kinopoisk_id'])) {
                    $items = Item::query()
                        ->where('kinopoisk_id', $data['data']['kinopoisk_id'])
                        ->get();
                    if (count($items) === 0 && $parserItem->parser_id === Parser::ID_KINOPOISK) {
                        $parserItem->createOrUpdateItemFromData();
                        $progress->advance();
                        $created++;
                        continue;
                    }
                    if (count($items) === 0 && $parserItem->parser_id !== Parser::ID_KINOPOISK) {
                        $kinopoiskParser->addParserItemByKinopoiskId($data['data']['kinopoisk_id']);
                        $progress->advance();
                        continue;
                    }
                }
                if (!count($items)) {
                    $countries = $data['attrs']['страна'] ?? null;
                    $names = [];
                    foreach (array_only($data['data'] ?? [], ['name_ru', 'name_en', 'name_original']) as $name) {
                        $nameSplit = explode('/', $name);
                        $nameSplit[] = $name;
                        $nameSplit = array_map(function ($value) { return mb_strtolower($value); }, $nameSplit);
                        $nameSplit = array_map(function ($value) {
                            return preg_replace('/[\s,.\-?\/!:;]*/', '', $value);
                        }, $nameSplit);
                        $names = array_merge($names, array_filter($nameSplit));
                    }
                    $yearStart = $data['data']['year_start'] ?? null;
                    if (!$names || !$countries || !$yearStart) {
                        $progress->advance();
                        continue;
                    }

                    $range = ($data['videos'][0]['season_number'] ?? 0) > 1
                        ? (($data['videos'][0]['season_number'] ?? 0) + 1)
                        : 1;
                    $countries = array_map(function ($value) {
                        return ParserValueAlias::alias('attr_value.name', $value);
                    }, $countries);
                    $countriesCond = "'" . implode("','", $countries) . "'";
                    $items = Item::query()
                        ->where(function ($query) use ($names) {
                            return $query
                                ->orWhereIn(DB::raw("REGEXP_REPLACE(LOWER(name_ru), '[[:space:],.\-?/!:;]*', '', 'g')"), $names)
                                ->orWhereIn(DB::raw("REGEXP_REPLACE(LOWER(name_en), '[[:space:],.\-?/!:;]*', '', 'g')"), $names)
                                ->orWhereIn(DB::raw("REGEXP_REPLACE(LOWER(name_original), '[[:space:],.\-?/!:;]*', '', 'g')"), $names);
                        })
                        ->whereBetween('year_start', [$yearStart - $range, $yearStart + 1])
                        ->whereRaw(
                            'EXISTS (SELECT 1 FROM item_attrs ia INNER JOIN attr_values av ON av.id = ia.attr_value_id AND av.attr_id = 1 AND av.value IN(' . $countriesCond . ') WHERE items.id = ia.item_id)'
                        )
                        ->get();
                }
                if (count($items) === 1) {
                    $item = $items->first();
                    $parserItem->item_id = $item->id;
                    $parserItem->save();
                    $parserItem->setRelation('item', $item);
                    $parserItem->updateItemVideos($data['videos'] ?? []);
                    $allFound++;
                }
                $progress->advance();
            }
        } while (count($parserItems) === $limit);
        $progress->finish();
        $this->line("created $created, found $allFound / $total items");
    }

    protected function applyAllItems(): void
    {
        $query = ParserItem::query()
            ->where('id', '>', 28142)
            ->whereNotNull('item_id')
            ->orderBy('id')
            ->with(['item', 'parserItemEvents']);

        $total = $query->count();
        $progress = $this->output->createProgressBar($total);
        $progress->start();
        $query->each(function (ParserItem $parserItem) use ($progress) {
            $progress->advance();
            $data = $parserItem->mergedParserItemEventsData();
            $parserItem->updateItemFromData($data);
        }, 100);
        $progress->finish();
    }
}
