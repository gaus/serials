<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\ParserLog;
use Illuminate\Console\Command;
use App\Parsers\Doramy;

class ParserVideo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:video';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse item videos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $parser = new Doramy();
        $items = ParserLog::dontWork(new Item(), Doramy::class, 'parseItemVideo')
            ->selectRaw('items.*')
            ->where('category_id', 2)
            ->limit(300)
            ->get();
        $n = 1;
        foreach ($items as $item) {
            $this->line($n.' / '.count($items));
            $n++;
            $this->line('search: ' . $item->id . ' ' . $item->name_en);
            $search = $parser->search($item->name_ru . ' ' . $item->year_start);
            if (is_null($search)) {
                ParserLog::writeLog($item->id, Doramy::class, 'parseItemVideo', ParserLog::RESULT_FAIL);
                $this->line('empty response');
            } elseif (empty($search)) {
                ParserLog::writeLog($item->id, Doramy::class, 'parseItemVideo', ParserLog::RESULT_NOT_FOUND);
                $this->line('not found');
            } else {
                $itemArr = $parser->parsePageItem($search[0]);
                if (is_null($itemArr)) {
                    ParserLog::writeLog($item->id, Doramy::class, 'parseItemVideo', ParserLog::RESULT_FAIL);
                    $this->line('empty response');
                } elseif (empty($itemArr['videos'])) {
                    ParserLog::writeLog($item->id, Doramy::class, 'parseItemVideo', ParserLog::RESULT_NOT_FOUND);
                    $this->line('not found videos');
                } elseif ($save = $parser->saveItemVideos($item, $itemArr['videos'])) {
                    ParserLog::writeLog(
                        $item->id,
                        Doramy::class,
                        'parseItemVideo',
                        ParserLog::RESULT_SUCCESS,
                        [
                            'url' => $search[0],
                            'searchResults' => count($search),
                            'videosFound' => count($itemArr['videos']),
                            'saveCount' => $save,
                        ]
                    );
                    $this->line('found ' . count($itemArr['videos']) . ' videos | save '.$save.'/'.count($itemArr['videos']));
                } else {
                    $this->line('save videos error');
                }
            }
            $this->line('');
        }
    }
}
