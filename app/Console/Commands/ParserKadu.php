<?php

namespace App\Console\Commands;

use App\Models\Parser;
use App\Models\ParserItem;
use Illuminate\Console\Command;

class ParserKadu extends Command
{
    protected $signature = 'parser:kadu';

    protected $description = 'Parse items from kadu.ru';

    public function handle(): void
    {
//        /** @var Parser $parser */
//        $parser = Parser::query()->find(7);
//        $parser->parser->setConsole($this);
//        $parser->parser->parseItems();

//        $limit = 10;
        /** @var ParserItem[] $parserItems */
        $parserItems = ParserItem::query()
            ->where('parser_id', 7)
            ->whereNull('last_activity')
            ->orderBy('id')
//            ->limit($limit)
            ->get();
        $count = count($parserItems);
        foreach ($parserItems as $n => $parserItem) {
            $n++;
            $this->line("{$n} / {$count}");
            $parserItem->parser->parser->setConsole($this);
            sleep(2);
            try {
                $parserItem->parser->parser->parseItem($parserItem)->save();
            } catch (\Exception $e) {
                $parserItem->saveParserItemEvent(false);
            }
            $parserItem->saveLastActivity();
        }
    }
}
