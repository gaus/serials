<?php

namespace App\Console\Commands;

use App\Models\ParserItem;
use Illuminate\Console\Command;

class AutoParser extends Command
{
    protected $signature = 'auto-parser';

    protected $description = 'Schedule auto parser task';

    public function handle(): void
    {
        /** @var ParserItem[] $parserItems */
        $parserItems = ParserItem::query()
            ->whereRaw('item_id NOTNULL OR (item_id ISNULL and last_activity ISNULL)')
            ->where('is_active', true)
            ->whereRaw('(has_404 IS FALSE OR has_404 ISNULL)')
            ->whereRaw("(last_activity < NOW() - (interval '1 day' * COALESCE(\"interval\", 7)) OR last_activity ISNULL)")
            ->orderByRaw('last_activity NULLS FIRST')
            ->with(['parser', 'parserItemEvents', 'item'])
            ->limit(20)
            ->get();
        $events = 0;
        $this->line((string)count($parserItems));
        foreach ($parserItems as $parserItem) {
            $this->line("start parser item {$parserItem->id}");
            usleep(200);
            $parserItemEvent = $parserItem->parser->parser->parseItem($parserItem);
            if (!$parserItemEvent->success) {
                $parserItem->has_404 = true;
                $parserItemEvent->save();
            } else {
                $data = $parserItem->mergedParserItemEventsData();
                $parserItemEvent->filterNewData($data);
                if (!empty($parserItemEvent->data)) {
                    $parserItemEvent->save();
                    if ($parserItem->item_id) {
                        $parserItem->updateItemFromData($parserItemEvent->data);
                    }
                    $events++;
                }
            }
            $parserItem->saveLastActivity();
        }
        $this->line("processed ".count($parserItems)." parser items, save new events $events");
    }
}
