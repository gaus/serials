<?php

namespace App\Console\Commands;

use App\Models\Item;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ItemAggregate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'item:aggregate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aggregate item attrs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Item::query()
            ->orderBy('id')
            ->with(['attrValues', 'attrValues.attr'])
            ->chunk(1000, function ($items) {
                foreach ($items as $item) {
                    /** @var $item Item */
                    $item->updateAttrs()->save();
                }
            });
        DB::table('item_series')
            ->update([
                'has_video' => DB::raw(
                    'CASE
                        WHEN EXISTS(
                            SELECT 1
                            FROM item_videos
                            WHERE item_series.id = item_videos.item_series_id
                                AND item_videos.has_404 ISNULL
                        ) THEN TRUE
                        ELSE FALSE
                    END'
                ),
            ]);
        DB::table('item_seasons')
            ->update([
                'has_video' => DB::raw(
                    'CASE
                        WHEN EXISTS(
                            SELECT 1
                            FROM item_series
                            WHERE item_series.item_season_id = item_seasons.id AND has_video IS TRUE
                        ) THEN TRUE
                        ELSE FALSE
                    END'
                ),
            ]);
        DB::table('items')
            ->update([
                'season_count' => DB::raw(
                    '(SELECT COUNT(*)
                    FROM item_seasons
                    WHERE item_seasons.item_id = items.id)'
                ),
                'series_count' => DB::raw(
                    '(SELECT COUNT(*)
                    FROM item_series
                        INNER JOIN item_seasons ON item_seasons.id = item_series.item_season_id
                    WHERE item_seasons.item_id = items.id)'
                ),
                'has_video' => DB::raw(
                    'CASE
                        WHEN EXISTS(
                            SELECT 1
                            FROM item_seasons
                            WHERE item_seasons.item_id = items.id AND has_video IS TRUE
                        ) THEN TRUE
                        ELSE FALSE
                    END'
                ),
            ]);
    }
}
