<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\Parser;
use App\Models\ParserItem;
use App\Models\ParserLog;
use Illuminate\Console\Command;
use App\Parsers\Doramy;

class ParserDoramy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:doramy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse items from doramy.su';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        /** @var Parser $parser */
//        $parser = Parser::query()->find(2);
//        $parser->parser->setConsole($this);
//        $parser->parser->parseItems();

//        $limit = 1;
        /** @var ParserItem[] $parserItems */
        $parserItems = ParserItem::query()
//            ->where('id', 16614)
            ->where('parser_id', 2)
            ->whereNull('last_activity')
            ->orderBy('id')
//            ->limit($limit)
            ->get();
        $count = count($parserItems);
        foreach ($parserItems as $n => $parserItem) {
            $n++;
            $this->line("{$n} / {$count}");
            $parserItem->parser->parser->setConsole($this);
            $parserItem->parser->parser->parseItem($parserItem);
            $parserItem->saveLastActivity();
        }
    }
}
