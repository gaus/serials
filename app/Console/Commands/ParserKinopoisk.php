<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Models\Parser;
use App\Models\ParserItem;
use App\Models\ParserValueAlias;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ParserKinopoisk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:kinopoisk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse items from kinopoisk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
//        /** @var Parser $parser */
//        $parser = Parser::query()->find(1);
//        $parser->parser->setConsole($this);
//        $parser->parser->parseItems();

//        $limit = 5;
//        /** @var ParserItem[] $parserItems */
//        $parserItems = ParserItem::query()
//            ->where('parser_id', 1)
//            ->whereNull('last_activity')
//            ->orderBy('id')
//            ->offset(1297)
//            ->limit($limit)
//            ->get();
//        $count = count($parserItems);
//        foreach ($parserItems as $n => $parserItem) {
//            $n++;
//            $this->line("{$n} / {$count}");
//            $parserItem->parser->parser->setConsole($this);
//            usleep(2500);
//            try {
//                $parserItem->parser->parser->parseItem($parserItem)->save();
//                $parserItem->saveLastActivity();
//            } catch (\Exception $e) {
//                $parserItem->saveParserItemEvent(false);
//            }
//        }
//
//        $query = ParserItem::query()
//            ->where('parser_id', 1)
//            ->whereNull('item_id')
//            ->orderBy('id')
//            ->with(['item', 'parserItemEvents']);
//        $total = $query->count();
//        $progress = $this->output->createProgressBar($total);
//        $progress->start();
//        $query->each(function (ParserItem $parserItem) use ($progress, &$count) {
//            $parserItem->createOrUpdateItemFromData();
//            $progress->advance();
//        }, 100);
//        $progress->finish();
    }
}
