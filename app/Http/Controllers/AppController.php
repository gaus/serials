<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Lists;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class AppController extends Controller
{
    public function index(Request $request)
    {
        // редирект на мобильную версию
        $agent = new Agent();
        $isMobileVersion = strpos($request->getHost(), 'm.') === 0;
        if ($agent->isMobile() && !$isMobileVersion) {
            $mobileUrl = str_replace($request->getHost(), 'm.' . $request->getHost(), $request->fullUrl());
            return redirect()->away($mobileUrl);
        } elseif ($agent->isDesktop() && $isMobileVersion) {
            $mobileUrl = str_replace('m.', '', $request->fullUrl());
            return redirect()->away($mobileUrl);
        }

        $title = 'Телесериалчик - смотреть сериалы онлайн';
        $keywords = 'Телесериалчик, смотреть сериалы онлайн, новинки серилов, популярные сериалы';
        $description = 'Огромный выбор сериалов для просмотра online. У нас вы найдёте популярные новинки и уже ставшие классикой хиты.';

        $listsJson = Lists::userLists()->get()->toJson();
        if (preg_match('/^\/item\/([0-9]+)/', $request->getRequestUri(), $matches)) {
            $item = Item::query()->find($matches[1]);
            if ($item) {
                $title = "Смотреть сериал «{$item->name}» все серии онлайн";
                $keywords = "{$item->name} смотреть онлайн";
                $description = $item->description
                    ? str_limit($item->description, 250)
                    : 'Смотреть сериал {{ $item->name }} все серии онлайн';
            }
        }

        return view('app', [
            'listsJson' => $listsJson,
            'item' => $item ?? null,
            'request' => $request,
            'isMobileVersion' => $isMobileVersion,
            'seoTitle' => $title,
            'seoKeywords' => $keywords,
            'seoDescription' => $description,
        ]);
    }

    public function error(string $code)
    {
        abort($code);
    }
}
