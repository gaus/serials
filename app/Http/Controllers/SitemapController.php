<?php

namespace App\Http\Controllers;

use App\Models\Item;
use samdark\sitemap\Sitemap;

/**
 * Class SitemapController
 */
class SitemapController extends Controller
{
    const FILE_NAME = 'sitemap.xml';

    public function index()
    {
        $filePath = public_path(self::FILE_NAME);
        if (!file_exists($filePath) || (time() - filemtime($filePath)) > 60*60*24) {
            $this->createSitemap();
        }

        return response()->file($filePath);
    }

    private function createSitemap(): void
    {
        $filePath = public_path(self::FILE_NAME);
        $sitemap = new Sitemap($filePath);
        $sitemap->addItem(env('APP_URL'));
        $items = Item::query()
            ->select(['id'])
            ->where('published', true)
            ->where('has_video', true)
            ->orderByRaw('COALESCE(rating, rating_kp, rating_imdb) DESC NULLS LAST')
            ->orderBy('id');
        $items->each(function (Item $item) use ($sitemap) {
            $sitemap->addItem($item->url);
        });
        $sitemap->write();
    }
}
