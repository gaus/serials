<?php

namespace App;

use App\Models\UserRating;

class UserRatingObserver
{
    public function created(UserRating $model)
    {
        $model->item->calculateRating();
    }

    public function updated(UserRating $model)
    {
        $model->item->calculateRating();
    }

    public function saved(UserRating $model)
    {
        $model->item->calculateRating();
    }

    public function deleted(UserRating $model)
    {
        $model->item->calculateRating();
    }
}
