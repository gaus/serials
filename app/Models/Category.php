<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'sort',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'sort' => 'integer',
        'seo_title' => 'string',
        'seo_description' => 'string',
        'seo_keywords' => 'string',
    ];
}
