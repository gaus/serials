<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttrValue extends Model
{
    /**
     * @var string
     */
    protected $table = 'attr_values';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'attr_id',
        'value',
        'sort',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'attr_id' => 'integer',
        'value' => 'string',
        'sort' => 'integer',
    ];

    public function attr()
    {
        return $this->belongsTo(Attr::class, 'attr_id', 'id');
    }
}
