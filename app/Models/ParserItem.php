<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class ParserItem
 *
 * @property int    $id
 * @property int    $parser_id
 * @property int    $item_id
 * @property string $url
 * @property bool   $is_active
 * @property string $last_activity
 * @property bool   $has_404
 * @property string $created_at
 * @property string $updated_at
 * @property int $interval
 *
 * @property Parser $parser
 * @property Item $item
 * @property Collection|ParserItemEvent[] $parserItemEvents
 */
class ParserItem extends Model
{
    protected $table = 'parser_items';

    protected $fillable = [
        'parser_id',
        'item_id',
        'url',
        'is_active',
        'has_404',
    ];

    protected $casts = [
        'id' => 'integer',
        'parser_id' => 'integer',
        'item_id' => 'integer',
        'url' => 'string',
        'is_active' => 'boolean',
        'has_404' => 'boolean',
        'last_activity' => 'string',
        'created_at' => 'string',
        'updated_at' => 'string',
    ];

    public function parser()
    {
        return $this->belongsTo(Parser::class, 'parser_id', 'id');
    }

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id', 'id');
    }

    public function parserItemEvents()
    {
        return $this->hasMany(ParserItemEvent::class, 'parser_item_id', 'id');
    }

    public function saveLastActivity(): void
    {
        $this->last_activity = DB::raw('NOW()');
        $this->save();
    }

    public function saveParserItemEvent(bool $success, ?array $data = null): ?ParserItemEvent
    {
        $result = new ParserItemEvent();
        $result->parser_item_id = $this->id;
        $result->success = $success;
        $result->data = $data;

        return $result->save()
            ? $result
            : null;
    }

    public function mergedParserItemEventsData(): array
    {
        $result = [
            'data' => [],
            'attrs' => [],
            'series' => [],
            'videos' => [],
        ];
        foreach ($this->parserItemEvents as $parserItemEvent) {
            if (!empty($parserItemEvent->data['data'])) {
                $result['data'] = array_merge($result['data'], $parserItemEvent->data['data']);
            }
            if (!empty($parserItemEvent->data['attrs'])) {
                $result['attrs'] = array_merge_recursive($result['attrs'], $parserItemEvent->data['attrs']);
                $result['attrs'] = array_map('array_unique', $result['attrs']);
            }
            if (!empty($parserItemEvent->data['videos'])) {
                foreach ($parserItemEvent->data['videos'] as $video) {
                    $key = implode('.', [
                        $video['season_number'] ?? null,
                        $video['series_number'] ?? null,
                        $video['src'] ?? null,
                    ]);
                    $result['videos'][$key] = $video;
                }
            }
            if (!empty($parserItemEvent->data['series'])) {
                $seasons = [];
                foreach ($parserItemEvent->data['series'] as $season) {
                    $series = [];
                    foreach ($season['series'] as $seriesItem) {
                        $series['series-'.$seriesItem['number'] ?? ''] = $seriesItem;
                    }
                    $seasons['season-'.($season['number'] ?? '')] = array_merge($season, ['series' => $series]);
                }
                $result['series'] = array_replace_recursive($result['series'], $seasons);
            }
        }
        $result['videos'] = array_values($result['videos']);
        $result['series'] = array_values($result['series']);
        foreach ($result['series'] as &$season) {
            $season['series'] = array_values($season['series']);
        }

        return $result;
    }

    public function createOrUpdateItemFromData(): ?Item
    {
        $data = $this->mergedParserItemEventsData();
        if (!$this->item_id && !empty($data['data'])) {
            $this->setRelation('item', new Item());
        }
        if ($this->item_id || !empty($data['data'])) {
            $this->updateItemFromData($data);
        }
        if (!$this->item_id && $this->item) {
            $this->item_id = $this->item->id;
            $this->save();
        }

        return $this->item;
    }

    public function updateItemFromData(array $data): void
    {
        if (!$this->item) {
            return;
        }

        if ($this->parser->apply_data && !empty($data['data'])) {
            $this->updateItem($data['data']);
        }
        if ($this->parser->apply_image && !empty($data['data']['image'])) {
            $this->updateImage($data['data']['image']);
        }
        if ($this->parser->apply_attrs && !empty($data['attrs'])) {
            $this->updateItemAttrs($data['attrs']);
        }
        if ($this->parser->apply_series && !empty($data['series'])) {
            $this->updateItemSeries($data['series']);
        }
        if ($this->parser->apply_videos && !empty($data['videos'])) {
            $this->updateItemVideos($data['videos']);
        }
    }

    public function updateItem(array $data): void
    {
        if ($this->item) {
            $newData = [];
            foreach ($data as $key => $value) {
                if ($key === 'image') {
                    continue;
                }
                !$this->item->$key && $newData[$key] = $value;
            }
            $this->item->fill($newData)->save();
        }
    }

    public function updateImage(string $image): void
    {
        if (!$this->item->image && $image) {
            $this->item->image = $this->loadImage($image);
            $this->item->save();
        }
    }

    protected function loadImage(string $url): ?string
    {
        $content = @file_get_contents($url);
        if (empty($content)) {
            return null;
        }

        $fileName = md5($content) . pathinfo($url, PATHINFO_EXTENSION);
        $path = $this->item->uploadPath($fileName);
        if (!is_dir(dirname($path))) {
            mkdir(dirname($path), 0775, true);
        }
        file_put_contents($path, $content);

        return $fileName;
    }

    public function updateItemAttrs(array $attrs): void
    {
        if (!$this->item) {
            return;
        }

        $values = [];
        foreach ($attrs as $attrName => $attrValues) {
            $attrName = ParserValueAlias::alias('attr.name', mb_strtolower($attrName));
            $attrValues = (array) $attrValues;
            $attrValues = array_map('trim', $attrValues);
            $attr = Attr::query()->firstOrCreate(['name' => $attrName]);
            foreach ($attrValues as $attrValue) {
                if (empty($attrValue)) { continue; }
                $attrValue = ParserValueAlias::alias('attr_value.name', $attrValue);
                if (!$attrValue || mb_strlen($attrValue) > 50) {
                    continue;
                }
                $value = AttrValue::query()->firstOrCreate(['attr_id' => $attr->id, 'value' => $attrValue]);
                $values[$value->id] = ['attr_id' => $attr->id];
            }
        }
        if (!empty($values)) {
            $this->item->attrValues()->syncWithoutDetaching($values);
        }
    }

    public function updateItemSeries(array $series): void
    {
        foreach ($series as $sesonArr) {
            if (!isset($sesonArr['number'])) {
                continue;
            }
            $season = ItemSeason::query()->firstOrCreate(['item_id' => $this->item->id, 'number' => $sesonArr['number']]);
            if (!empty($sesonArr['series'])) {
                foreach ($sesonArr['series'] as $seriesArr) {
                    if (!isset($seriesArr['number'])) {
                        continue;
                    }
                    ItemSeries::query()->updateOrCreate(
                        ['item_season_id' => $season->id, 'number' => $seriesArr['number']],
                        $seriesArr
                    );
                }
            }
        }
    }

    public function updateItemVideos(array $videos): void
    {
        foreach ($videos as $videoArr) {
            if (!isset($videoArr['season_number']) || !isset($videoArr['series_number']) || empty($videoArr['src'])) {
                continue;
            }
            $data = [];
            $season = ItemSeason::query()
                ->firstOrCreate(['item_id' => $this->item->id, 'number' => $videoArr['season_number']]);
            $series = ItemSeries::query()
                ->firstOrCreate(['item_season_id' => $season->id, 'number' => $videoArr['series_number']]);
            if (!empty($videoArr['author']) && mb_strlen($videoArr['author']) <= 32) {
                $author = Author::query()
                    ->firstOrCreate([
                        'name' => ParserValueAlias::alias('author.name', $videoArr['author'])
                    ]);
                $data['author_id'] = $author->id;
            }
            if (empty($videoArr['source'])) {
                $videoArr['source'] = parse_url($videoArr['src'], PHP_URL_HOST);
                $videoArr['source'] = implode(
                    '.',
                    array_slice(
                        explode('.', $videoArr['source']),
                        -2
                    )
                );
            }
            $source = Source::query()
                ->firstOrCreate([
                    'name' => ParserValueAlias::alias('source.name', $videoArr['source'])
                ]);
            $data['source_id'] = $source->id;
            $data['video_type_id'] = $videoArr['video_type_id'] ?? 3;
            $data['item_id'] = $this->item->id;
            $data['item_season_id'] = $season->id;
            $data['item_series_id'] = $series->id;
            $data['src'] = $videoArr['src'];
            $itemVideo = ItemVideo::query()->firstOrNew(
                ['item_series_id' => $series->id, 'src' => $data['src']]
            );
            foreach ($data as $key => $value) {
                !$itemVideo->$key && $itemVideo->$key = $value;
            }
            if (!$itemVideo->id || $itemVideo->isDirty()) {
                $itemVideo->save();
            }
        }
    }
}
