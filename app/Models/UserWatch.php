<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserWatch
 * @property ItemVideo $video
 */
class UserWatch extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_watches';
    
    protected $itemVideoClass = ItemVideo::class;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'item_id',
        'item_series_id',
        'item_video_id',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'item_id' => 'integer',
        'item_series_id' => 'integer',
        'item_video_id' => 'integer',
    ];

    public function video()
    {
        return $this->belongsTo($this->itemVideoClass, 'item_video_id', 'id');
    }
}
