<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Lists extends Model
{
    public const ID_PLANNED = 1;
    public const ID_VIEWED = 2;
    public const ID_ABANDONED = 3;
    public const ID_WATCH = 4;

    /**
     * @var string
     */
    protected $table = 'lists';

    protected $itemClass = Item::class;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'sort',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'name' => 'string',
        'number' => 'integer',
    ];

    public function userItems()
    {
        return $this->belongsToMany($this->itemClass, 'user_items', 'list_id', 'item_id')
            ->withPivot('id', 'season_number', 'series_number')
            ->whereRaw('(user_items.user_id IS NULL OR user_items.user_id = '.Auth::user()->id.')')
            ->with('seasons');
    }

    public static function userLists()
    {
        if (Auth::check()) {
            return self::where(function ($query) {
                $query->orWhereNull('user_id')
                    ->orWhere('user_id', Auth::id());
            })
                ->orderBy('sort');
        } else {
            return self::whereNull('user_id')->orderBy('sort');
        }
    }
}
