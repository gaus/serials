<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ParserValueAlias
 *
 * @property int    $id
 * @property string $type
 * @property string $value
 * @property string $alias
 */
class ParserValueAlias extends Model
{
    protected $table = 'parser_value_aliases';

    protected $fillable = [
        'id',
        'type',
        'value',
        'alias',
    ];

    protected $casts = [
        'id' => 'integer',
        'type' => 'string',
        'value' => 'string',
        'alias' => 'string',
    ];

    protected static $_map = [];

    public static function alias(string $type, string $value): string
    {
        return self::getTypeMap($type)[$value] ?? $value;
    }

    protected static function getTypeMap(string $type): array
    {
        if (!array_key_exists($type, self::$_map)) {
            self::$_map[$type] = [];
            foreach (self::query()->where('type', $type)->get() as $item) {
                self::$_map[$type][$item->alias] = $item->value;
            }
        }

        return self::$_map[$type];
    }
}