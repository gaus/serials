<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attr extends Model
{
    /**
     * @var string
     */
    protected $table = 'attrs';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'sort',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'sort' => 'integer',
    ];

    public function values()
    {
        return $this->hasMany(AttrValue::class, 'attr_id', 'id')->orderBy('attr_values.sort');
    }
}
