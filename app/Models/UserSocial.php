<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserSocial
 * @property int    $id
 * @property int    $user_id
 * @property string $type
 * @property string $social_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 */
class UserSocial extends Model
{
    protected $table = 'user_socials';

    protected $fillable = [
        'user_id',
        'type',
        'social_id',
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'type' => 'string',
        'social_id' => 'string',
        'created_at' => 'string',
        'updated_at' => 'string',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
