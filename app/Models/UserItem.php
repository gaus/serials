<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserItem extends Model
{
    const LIST_ID_DEFAULT = 1;

    /**
     * @var string
     */
    protected $table = 'user_items';

    protected $itemClass = Item::class;
    protected $userClass = User::class;
    protected $listsClass = Lists::class;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'item_id',
        'list_id',
        'season_number',
        'series_number',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'item_id' => 'integer',
        'list_id' => 'integer',
        'season_number' => 'integer',
        'series_number' => 'integer',
    ];

    public function item()
    {
        return $this->belongsTo($this->itemClass, 'item_id', 'id');
    }

    public function userList()
    {
        return $this->belongsTo($this->listsClass, 'list_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo($this->userClass, 'user_id', 'id');
    }
}
