<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ParserItemEvent
 *
 * @property int    $id
 * @property int    $parser_item_id
 * @property bool   $success
 * @property array  $data
 * @property string $created_at
 *
 * @property ParserItem $parserItem
 */
class ParserItemEvent extends Model
{
    protected $table = 'parser_item_events';

    public $timestamps = false;

    protected $fillable = [
        'parser_item_id',
        'success',
        'data',
    ];

    protected $casts = [
        'id' => 'integer',
        'parser_item_id' => 'integer',
        'success' => 'boolean',
        'data' => 'array',
        'created_at' => 'string',
    ];

    public function parserItem()
    {
        return $this->belongsTo(ParserItem::class, 'parser_item_id', 'id');
    }

    public function filterNewData(array $data): void
    {
        $newData = [];
        if (!empty($this->data['data'])) {
            $newData['data'] = array_diff_assoc($this->data['data'], $data['data'] ?? []);
        }
        if (!empty($this->data['attrs'])) {
            $newAttrs = [];
            foreach ($this->data['attrs'] as $key => $values) {
                if (empty($data['attrs'][$key]) || array_diff($values, $data['attrs'][$key])) {
                    $newAttrs[$key] = $values;
                }
            }
            $newData['attrs'] = $newAttrs;
        }
        if (!empty($this->data['series'])) {
            $newSeasons = [];
            foreach ($this->data['series'] as $season) {
                if (!isset($season['number'])) {
                    continue;
                }
                $oldSeason = null;
                foreach ($data['series'] ?? [] as $oldSeasonItem) {
                    if (!isset($oldSeasonItem['number'])) {
                        continue;
                    }
                    if ($oldSeasonItem['number'] === $season['number']) {
                        $oldSeason = $oldSeasonItem;
                        break;
                    }
                }
                if (!$oldSeason) {
                    $newSeasons[] = $season;
                    continue;
                }
                if (!empty($season['series'])) {
                    $newSeries = [];
                    foreach ($season['series'] as $seriesItem) {
                        $oldSeriesItem = null;
                        foreach ($oldSeason['series'] ?? [] as $oldSeriesItemData) {
                            if ($seriesItem['number'] === $oldSeriesItemData['number']) {
                                $oldSeriesItem = $oldSeriesItemData;
                                break;
                            }
                        }
                        if (!$oldSeriesItem || array_diff_assoc($seriesItem, $oldSeriesItem)) {
                            $newSeries[] = $seriesItem;
                        }
                    }
                    if ($newSeries) {
                        $season['series'] = $newSeries;
                        $newSeasons[] = $season;
                    }
                }
            }
            $newData['series'] = $newSeasons;
        }
        if (!empty($this->data['videos'])) {
            $newVideos = [];
            foreach ($this->data['videos'] as $video) {
                if (!isset($video['season_number']) || !isset($video['series_number'])) {
                    continue;
                }
                $oldVideo = null;
                foreach ($data['videos'] ?? [] as $oldVideoData) {
                    if (
                        $video['season_number'] === ($oldVideoData['season_number'] ?? null)
                        && $video['series_number'] === ($oldVideoData['series_number'] ?? null)
                        && $video['src'] === $oldVideoData['src']
                    ) {
                        $oldVideo = $oldVideoData;
                        break;
                    }
                }
                if (!$oldVideo || array_diff_assoc($video, $oldVideo)) {
                    $newVideos[] = $video;
                }
            }
            $newData['videos'] = $newVideos;
        }
        $this->data = array_filter($newData);
    }
}
