<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemAttr extends Model
{
    protected $table = 'item_attrs';

    public $timestamps = false;

    protected $fillable = [
        'item_id',
        'attr_id',
        'attr_value_id',
    ];
}
