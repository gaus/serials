<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSeries extends Model
{
    /**
     * @var string
     */
    protected $table = 'item_series';
    
    protected $itemSeasonClass = ItemSeason::class;

    /**
     * @var array
     */
    protected $fillable = [
        'item_season_id',
        'number',
        'name_ru',
        'name_original',
        'released_at',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_season_id' => 'integer',
        'name_ru' => 'string',
        'name_original' => 'string',
        'number' => 'integer',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'released_at',
    ];

    public function season()
    {
        return $this->belongsTo($this->itemSeasonClass, 'item_season_id', 'id');
    }
}
