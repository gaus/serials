<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParserLog extends Model
{
    const RESULT_SUCCESS = 'success';
    const RESULT_FAIL = 'fail';
    const RESULT_NOT_FOUND = 'not fund';

    /**
     * @var string
     */
    protected $table = 'parser_logs';

    protected $itemClass = Item::class;

    /**
     * @var array
     */
    protected $fillable = [
        'item_id',
        'parser',
        'action',
        'result',
        'info',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_id' => 'integer',
        'parser' => 'string',
        'action' => 'string',
        'result' => 'string',
        'info' => 'array',
    ];

    public function item()
    {
        return $this->belongsTo($this->itemClass, 'item_id', 'id');
    }

    public static function writeLog($item_id, string $parser, string $action, string $result, array $info = null)
    {
        return self::create([
            'item_id' => $item_id,
            'parser' => $parser,
            'action' => $action,
            'result' => $result,
            'info' => $info,
        ]);
    }

    /**
     * Фильмы для которых еще не выполнен парсинг
     * 
     * @param Item $item
     * @param string $parser
     * @param string $action
     * @return Item
     */
    public static function dontWork(Item $item, string $parser, string $action)
    {
        return $item->leftJoin('parser_logs', function ($join) use ($parser, $action) {
                $join->on('parser_logs.item_id', '=', 'items.id')
                    ->where('parser_logs.parser', $parser)
                    ->where('parser_logs.action', $action)
                    ->where('parser_logs.result', '!=', self::RESULT_FAIL);
            })
            ->whereNull('parser_logs.id');
    }
}
