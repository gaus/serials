<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property Item $item Сериал
 */
class UserRating extends Model
{
    public $itemClass = Item::class;
    
    /**
     * @var string
     */
    protected $table = 'user_ratings';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'item_id',
        'value',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'item_id' => 'integer',
        'value' => 'integer',
    ];
    
    public function item()
    {
        return $this->belongsTo($this->itemClass, 'item_id', 'id');
    }
}
