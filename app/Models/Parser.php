<?php

namespace App\Models;

use App\Parsers\AbstractParser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Parser
 *
 * @property int    $id
 * @property string $name
 * @property string $url
 * @property string $class
 * @property bool   $is_active
 * @property string $last_activity
 * @property bool   $apply_data
 * @property bool   $apply_image
 * @property bool   $apply_attrs
 * @property bool   $apply_series
 * @property bool   $apply_videos
 *
 * @property AbstractParser $parser
 */
class Parser extends Model
{
    const ID_KINOPOISK = 1;

    protected $table = 'parsers';

    public $timestamps = false;

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'url' => 'string',
        'class' => 'string',
        'is_active' => 'boolean',
        'last_activity' => 'string',
    ];

    /**
     * @var AbstractParser
     */
    protected $_parser;

    public function getParserAttribute(): AbstractParser
    {
        if (null === $this->_parser) {
            $this->_parser = new $this->class($this);
        }

        return $this->_parser;
    }

    public function saveLastActivity(): void
    {
        $this->last_activity = DB::raw('NOW()');
        $this->save();
    }
}
