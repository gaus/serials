<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * Видео.
 *
 * @property int $id
 * @property int $item_id
 * @property int $item_season_id
 * @property int $item_series_id
 * @property int $video_type_id
 * @property int $author_id
 * @property int $source_id
 * @property int $quality
 * @property string $src
 * @property string $created_at
 * @property string $updated_at
 * @property int $has_404
 * @property string $checked_404_at
 *
 * @property Author $author
 * @property Source $source
 * @property VideoType $videoType
 * @property ItemSeries $itemSeries
 * @property ItemSeason $itemSeason
 * @property Item $item
 */
class ItemVideo extends Model
{
    /**
     * @var string
     */
    protected $table = 'item_videos';
    
    protected $authorClass = Author::class;
    protected $sourceClass = Source::class;
    protected $videoTypeClass = VideoType::class;
    protected $itemSeriesClass = ItemSeries::class;
    protected $itemSeasonClass = ItemSeason::class;
    protected $itemClass = Item::class;

    /**
     * @var array
     */
    protected $fillable = [
        'item_id',
        'item_season_id',
        'item_series_id',
        'video_type_id',
        'author_id',
        'source_id',
        'quality',
        'src',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_id' => 'integer',
        'item_season_id' => 'integer',
        'item_series_id' => 'integer',
        'video_type_id' => 'integer',
        'author_id' => 'integer',
        'source_id' => 'integer',
        'quality' => 'string',
        'src' => 'string',
    ];

    public function author()
    {
        return $this->belongsTo($this->authorClass, 'author_id', 'id');
    }

    public function source()
    {
        return $this->belongsTo($this->sourceClass, 'source_id', 'id');
    }

    public function videoType()
    {
        return $this->belongsTo($this->videoTypeClass, 'video_type_id', 'id');
    }

    public function itemSeries()
    {
        return $this->belongsTo($this->itemSeriesClass, 'item_series_id', 'id');
    }

    public function itemSeason()
    {
        return $this->belongsTo($this->itemSeasonClass, 'item_season_id', 'id');
    }

    public function item()
    {
        return $this->belongsTo($this->itemClass, 'item_id', 'id');
    }

    public function nextSeriesVideo()
    {
        $seasonId = $this->itemSeason ? $this->itemSeason->id : 0;
        $seasonNumber = $this->itemSeason ? $this->itemSeason->number : 0;
        $seriesNumber = $this->itemSeries ? $this->itemSeries->number : 0;
        $videoTypeId = $this->video_type_id ?? 0;
        $authorId = $this->author_id ?? 0;
        $sourceId = $this->source_id ?? 0;
        $sortSql = <<<SQL
CASE
  WHEN item_series.number > {$seriesNumber} AND item_seasons.id = {$seasonId} THEN 1
  WHEN item_seasons.number > {$seasonNumber} THEN 2
  ELSE 3
END as sort
SQL;
        $relevantSql = <<<SQL
CASE
  WHEN item_videos.author_id = {$authorId} AND item_videos.source_id = {$sourceId} AND item_videos.video_type_id = {$videoTypeId} THEN 1
  WHEN item_videos.author_id = {$authorId} AND item_videos.video_type_id = {$videoTypeId} THEN 2
  WHEN item_videos.source_id = {$sourceId} AND item_videos.video_type_id = {$videoTypeId} THEN 3
  WHEN item_videos.video_type_id = {$videoTypeId} THEN 4
  ELSE 5
END as relevant
SQL;

        return self::select(DB::raw('item_videos.*'), DB::raw($sortSql), DB::raw($relevantSql))
            ->join('item_series', 'item_series.id', '=', 'item_videos.item_series_id')
            ->join('item_seasons', 'item_seasons.id', '=', 'item_videos.item_season_id')
            ->where('item_videos.item_id', $this->item_id)
            ->whereNull('item_videos.has_404')
            ->orderBy('sort')
            ->orderBy('item_seasons.number')
            ->orderBy('item_series.number')
            ->orderBy('relevant')
            ->first();
    }
    
    public function userPrioritetOrder($model, $seasonId = null, $seriesId = null)
    {
        if ($seasonId && !$seriesId) {
            $sortSql = <<<SQL
CASE
  WHEN item_series.number > {$this->itemSeries->number} AND item_seasons.id = {$this->itemSeason->id} THEN 1
  ELSE 2
END as sort
SQL;
        }
        if (!$seasonId && !$seriesId) {
            $sortSql = <<<SQL
CASE
  WHEN item_series.number > {$this->itemSeries->number} AND item_seasons.id = {$this->itemSeason->id} THEN 1
  WHEN item_seasons.number > {$this->itemSeason->number} THEN 2
  ELSE 3
END as sort
SQL;
        }
        $relevantSql = <<<SQL
CASE
  WHEN item_videos.author_id = {$this->author_id} AND item_videos.source_id = {$this->source_id} AND item_videos.video_type_id = {$this->video_type_id} THEN 1
  WHEN item_videos.author_id = {$this->author_id} AND item_videos.video_type_id = {$this->video_type_id} THEN 2
  WHEN item_videos.source_id = {$this->source_id} AND item_videos.video_type_id = {$this->video_type_id} THEN 3
  WHEN item_videos.video_type_id = {$this->video_type_id} THEN 4
  ELSE 5
END as relevant
SQL;
        if (!empty($sortSql)) {
            $model = $model->addSelect(DB::raw($sortSql))->orderBy('sort');
        }
        $model = $model->addSelect(DB::raw($relevantSql))->orderBy('relevant');

        return $model;
    }

    public static function firstItemVideo(int $itemId): ?self
    {
        return self::query()
            ->select('item_videos.*')
            ->join('item_series', 'item_series.id', '=', 'item_videos.item_series_id')
            ->join('item_seasons', 'item_seasons.id', '=', 'item_videos.item_season_id')
            ->where('item_videos.item_id', $itemId)
            ->whereNull('item_videos.has_404')
            ->orderBy('item_seasons.number')
            ->orderBy('item_series.number')
            ->first();
    }
}
