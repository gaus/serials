<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Item extends Model
{
    const IMAGE_PATH = '/uploads/items/';
    
    /**
     * @var string
     */
    protected $table = 'items';

    protected $itemSeasonClass = ItemSeason::class;
    protected $userWatchesClass = UserWatch::class;
    protected $listsClass = Lists::class;
    protected $itemVideosClass = ItemVideo::class;
    protected $userRatingClass = UserRating::class;
    protected $userItemsClass = UserItem::class;

    /**
     * @var array
     */
    protected $fillable = [
        'category_id',
        'name_ru',
        'name_en',
        'name_original',
        'description',
        'image',
        'season_count',
        'series_count',
        'rating',
        'rating_kp',
        'rating_imdb',
        'started_at',
        'completed_at',
        'year_start',
        'year_complete',
        'kinopoisk_id',
        'published',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'category_id' => 'integer',
        'name_ru' => 'string',
        'name_en' => 'string',
        'name_original' => 'string',
        'description' => 'string',
        'image' => 'string',
        'year_start' => 'integer',
        'year_complete' => 'integer',
        'season_count' => 'integer',
        'series_count' => 'integer',
        'rating' => 'float',
        'rating_kp' => 'float',
        'rating_imdb' => 'float',
        'attrs' => 'array',
        'video_types' => 'array',
        'kinopoisk_id' => 'integer',
        'published' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'started_at',
        'completed_at',
    ];

    protected $aggregateAttrIds = [1, 2, 3];

    public function attrValues()
    {
        return $this->belongsToMany(AttrValue::class, 'item_attrs', 'item_id', 'attr_value_id')
            ->orderBy('sort')
            ->with('attr');
    }

    public function seasons()
    {
        return $this->hasMany($this->itemSeasonClass, 'item_id', 'id')
            ->orderBy('number')
            ->with('series');
    }

    public function userItems()
    {
        return $this->hasMany($this->userItemsClass, 'item_id', 'id')
            ->where('user_id', Auth::check() ? Auth::id() : 0);
    }
    
    public function userLists()
    {
        return $this->belongsToMany($this->listsClass, 'user_items', 'item_id', 'list_id')
            ->withPivot('season_number', 'series_number')
            ->whereRaw('(user_items.user_id IS NULL OR user_items.user_id = '.(Auth::check() ? Auth::user()->id : 0).')')
            ->orderBy('sort');
    }

    public function userWatches()
    {
        return $this->hasMany($this->userWatchesClass, 'item_id', 'id')
            ->where('user_id', (Auth::check() ? Auth::user()->id : 0));
    }

    public function videos()
    {
        return $this->hasMany($this->itemVideosClass, 'item_id', 'id');
    }
    
    public function userRating()
    {
        return $this->belongsTo($this->userRatingClass, 'id', 'item_id')
            ->where('user_id', (Auth::check() ? Auth::user()->id : 0));
    }

    public function userRatings()
    {
        return $this->hasMany($this->userRatingClass, 'item_id', 'id');
    }

    public function getImageSrcAttribute($value)
    {
        return $this->image ?
            url(self::IMAGE_PATH . $this->image) :
            url('/images/item_no_image.png');
    }

    /**
     * @return $this
     */
    public function updateAttrs()
    {
        $attrs = [];
        foreach ($this->attrValues as $itemAttr) {
            if (!in_array($itemAttr->attr_id, $this->aggregateAttrIds)) {
                continue;
            }
            if (!isset($attrs[$itemAttr->attr->slug])) {
                $attrs[$itemAttr->attr->slug] = [
                    'id' => $itemAttr->attr->id,
                    'name' => $itemAttr->attr->name,
                    'sort' => $itemAttr->attr->sort,
                    'values' => [],
                ];
            }
            $attrs[$itemAttr->attr->slug]['values'][$itemAttr->id] = array_only($itemAttr->toArray(), ['id', 'value', 'sort']);
        }

        uasort($attrs, function ($a, $b) {
            return $a['sort'] <=> $b['sort'];
        });
        $this->attrs = $attrs;

        return $this;
    }
    
    public function syncAggregate()
    {
        $seasonCount = 0;
        $seriesCount = 0;
        foreach ($this->seasons as $season) {
            $seasonCount++;
            $seriesCount += count($season->series);
        }
        $this->season_count = $seasonCount;
        $this->series_count = $seriesCount;

        return $this;
    }

    public function videoTypesAggregate()
    {
        $videoTypes = null;
        $videoTypesIds = $this->videos()->select('video_type_id')->distinct()->get()->pluck('video_type_id');
        if (!empty($videoTypesIds)) {
            $videoTypes = [];
            foreach (VideoType::whereIn('id', $videoTypesIds)->orderBy('id')->get() as $videoType) {
                $videoTypes[$videoType->id] = [
                    'id' => $videoType->id,
                    'name' => $videoType->name,
                ];
            }
        }
        $this->published = (bool)$videoTypes;
        $this->video_types = $videoTypes;
        return $this;
    }

    public function calculateRating()
    {
        $result = $this->userRatings()
            ->select(DB::raw('count(*) as rating_count, sum(value) as rating_sum'))
            ->first();
        if ($result->rating_count && $result->rating_sum) {
            $this->rating = $result->rating_sum / $result->rating_count;
        } else {
            $this->rating = null;
        }
        return $this->save();
    }

    public function uploadPath(string &$fileName): string
    {
        $fileName = substr($fileName, 0, 2)
            . '/'
            . substr($fileName, 2, 2)
            . '/'
            . $fileName;

        return public_path('uploads/items/' . $fileName);
    }

    public function getNameAttribute(): ?string
    {
        return $this->name_ru ?? $this->name_en ?? $this->name_original;
    }

    public function getUrlAttribute(): string
    {
        return url("/item/{$this->id}");
    }
}
