<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSeason extends Model
{
    /**
     * @var string
     */
    protected $table = 'item_seasons';
    
    protected $itemClass = Item::class;
    protected $itemSeriesClass = ItemSeries::class;

    /**
     * @var array
     */
    protected $fillable = [
        'item_id',
        'number',
        'started_at',
        'completed_at',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_id' => 'integer',
        'number' => 'integer',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'started_at',
        'completed_at',
    ];

    public function item()
    {
        return $this->belongsTo($this->itemClass, 'item_id', 'id');
    }

    public function series()
    {
        return $this->hasMany($this->itemSeriesClass, 'item_season_id', 'id')->orderBy('number');
    }
}
