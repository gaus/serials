<?php

namespace App\Parsers;

use App\Models\ParserItem;
use App\Models\ParserItemEvent;

class DoramyParser extends AbstractParser
{
    public  function parseItems(): void
    {
        $result = $this->request($this->fullUrl('/news/'));
        if (!$result) {
            throw new \Exception('empty response');
        }

        $doc = \phpQuery::newDocumentHTML($result);
        $pageCount = 0;
        foreach ($doc->find('.swchItem') as $link) {
            if ((int)$link->nodeValue > $pageCount) {
                $pageCount = (int)$link->nodeValue;
            }
        }

        for ($p = 1; $p <= $pageCount; $p++) {
            $this->log("Parse page list $p / $pageCount");
            sleep(3);
            $this->parsePageList($this->fullUrl("/news/?page$p"));
        }
    }

    public function parseItem(ParserItem $parserItem): ParserItemEvent
    {
        $this->log($parserItem->url);
//        sleep(2);
        $result = $this->request($parserItem->url, null, null, $code);
        if ($code === 404) {
            return $this->prepareParserItemEvent($parserItem, false);
        } elseif (!$result) {
            throw new \Exception('empty response');
        }

        $data = [];
        $attrs = [];
        $videos = [];
        $doc = \phpQuery::newDocumentHTML($result);
        foreach ($doc->find('.actorlist td') as $actor) {
            $attrs['в главных ролях'][] = $actor->nodeValue;
        }

        $descriptionArr = [];
        $descriptionSelector = '.actorlist + p';
        while ($descriptionP = $doc->find($descriptionSelector)) {
            if (!$descriptionP->text()) {
                break;
            }
            $descriptionSelector .= ' + p';
            if ($descriptionP->find('span')->text()) {
                continue;
            }
            $descriptionArr[] = $descriptionP->text();
        }
        if (!empty($descriptionArr)) {
            $data['description'] = implode("\r\n", $descriptionArr);
        }

        foreach ($doc->find('.zaebinfo .godser') as $year) {
            $yearValue = $year->nextSibling->nodeValue;
            if ($year->nodeValue && $yearValue) {
                $yearValue = preg_replace('/\\(.*?\\)/', '', $yearValue);
                preg_match_all('/[0-9]{4}/', $yearValue, $matches);
                if (!empty($matches[0][0])) {
                    $data['year_start'] = $matches[0][0];
                }
                if (!empty($matches[0][1])) {
                    $data['year_complete'] = $matches[0][1];
                }
            }
        }
        foreach ($doc->find('.zaebinfo .epizodov') as $duration) {
            $durationValue = $duration->nextSibling->nodeValue;
            if ($duration->nodeValue && $durationValue) {
                $durationValue = preg_replace('/\\(.*?\\)/', '', $durationValue);
                preg_match_all('/по ([0-9]+) мин/', $durationValue, $matches);
                if (!empty($matches[1][0])) {
                    $data['duration'] = $matches[1][0];
                }
            }
        }
        foreach ($doc->find('.zaebinfo .stranaser') as $country) {
            $countryValue = $country->nextSibling->nodeValue;
            if ($country->nodeValue && $countryValue) {
                $attrs['страна'] = explode(',', $countryValue);
                $attrs['страна'] = array_map('trim', $attrs['страна']);
                $key = array_search('Южная Корея', $attrs['страна']);
                if ($key !== false) {
                    $attrs['страна'][$key] = 'Корея Южная';
                }
            }
        }
        foreach ($doc->find('.zaebinfo .zhanrser') as $genre) {
            $genreValue = $genre->nextSibling->nodeValue;
            if ($genre->nodeValue && $genreValue) {
                $attrs['жанр'] = explode(',', $genreValue);
                $attrs['жанр'] = array_map('trim', $attrs['жанр']);
                $attrs['жанр'] = array_map('mb_strtolower', $attrs['жанр']);
            }
        }

        foreach ($doc->find('.exContent select') as $select) {
            $author = null;
            $seasonNumber = 1;
            foreach ($select->childNodes as $option) {
                if ($option->tagName === 'optgroup') {
                    $label = $option->getAttribute('label');
                    $label && $seasonNumber = (int)str_replace('Сезон ', '', $label);
                    continue;
                }
                $video = [
                    'video_type_id' => 1
                ];
                $src = $option->getAttribute('value');
                $name = $option->nodeValue;
                $video['src'] = $src;
                preg_match_all('/Серия\s*([0-9]+)\s*(\\([^)]+\\))?/', $name, $matches);
                !empty($matches[1][0]) && $video['series_number'] = (int)$matches[1][0];
                !empty($matches[2][0]) && !$author && $author = trim($matches[2][0], '()');
                $author && $video['author'] = $author;
                $video['season_number'] = $seasonNumber;
                $videos[] = $video;
            }
        }
        $this->log('');

        return $this->prepareParserItemEvent(
            $parserItem,
            true,
            ['data' => $data, 'attrs' => $attrs, 'videos' => $videos]
        );
    }

    protected function parsePageList(string $url): void
    {
        $result = $this->request($url);
        if (!$result) {
            throw new \Exception('empty response');
        }

        $count = 0;
        $doc = \phpQuery::newDocumentHTML($result);
        foreach ($doc->find('#allEntries article') as $elem) {
            if (($name = pq('.entry-title a', $elem)) && $name->attr('href')) {
                $parserItem = $this->saveParserItem($this->fullUrl($name->attr('href')));
                if (!$parserItem->last_activity) {
                    $data = ['name_ru' => $name->text()];
                    if (($img = pq('.entry-pic img', $elem)) && $img->attr('src')) {
                        $data['image'] = $this->fullUrl($img->attr('src'));
                    }
                    $this->prepareParserItemEvent($parserItem, true, ['data' => $data])->save();
                }
                $count++;
            }
        }
        $this->log("items found $count");
    }
}
