<?php

namespace App\Parsers;

use App\Models\ParserItem;
use App\Models\ParserItemEvent;

class Hd720me extends AbstractParser
{
    protected $listPages = [
        '/serial/',
        '/mults/mult-serial/',
        '/anime/anime-serial/',
    ];

    public function parseItems(): void
    {
        $count = 0;
        foreach ($this->listPages as $pageUrl) {
            $p = 1;
            while (true) {
                $url = $this->fullUrl(
                    $p === 1 ? $pageUrl : "{$pageUrl}page/$p/"
                );
                $this->log($url);
                sleep(2);
                $response = $this->request($url);
                if (!$response) {
                    throw new \Exception('empty response');
                };

                $doc = \phpQuery::newDocumentHTML($response);
                foreach ($doc->find('#dle-content .shortstory .poster-view a') as $elem) {
                    if ($elem && $elem->getAttribute('href')) {
                        $this->saveParserItem($elem->getAttribute('href'));
                        $count++;
                    }
                }
                if (!($next = $doc->find('.navigation a:last-child')) || $next->text() !== 'Вперед') {
                    break;
                }
                $p++;
            }
        }
        $this->log("Parse $count items");
    }

    public function parseItem(ParserItem $parserItem): ParserItemEvent
    {
        $this->log($parserItem->url);
        $response = $this->request($parserItem->url, null, null, $code);
        if ($code === 404) {
            return $this->prepareParserItemEvent($parserItem, false);
        } elseif (!$response) {
            throw new \Exception('empty response');
        };

        $doc = \phpQuery::newDocumentHTML($response);
        $data = [];
        $attrs = [];
        $videos = [];
        if (($name = $doc->find('meta[property="og:title"]')) && $name->attr('content')) {
            $data['name_ru'] = $name->attr('content');
        }
        if (($name2 = $doc->find('.fulltitle2')) && $name2->text()) {
            $nameEn = trim(preg_replace('/Статус.*/', '', $name2->text()), ' —');
            $nameEn && $data['name_en'] = $nameEn;
        }
        if (($img = $doc->find('.fullstory-text .poster-view img')) && $img->attr('src')) {
            $imageSrc = $img->attr('src');
            if (starts_with($imageSrc, '//')) {
                $imageSrc = "http:$imageSrc";
            } elseif (starts_with($imageSrc, '/')) {
                $imageSrc = $this->fullUrl($imageSrc);
            }
            $data['image'] = $imageSrc;
        }
        foreach ($doc->find('.fullstory-text-left-ul li') as $li) {
            $label = pq('strong', $li);
            if (!$label || !$label->text()) continue;
            $label = trim($label->text());
            switch ($label) {
                case 'Рейтинги:':
                    foreach (pq('a', $li) as $value) {
                        if (
                            $value->nodeValue === 'КиноПоиск'
                            && $kinopoiskId = $this->getKinopoiskIdByUrl($value->getAttribute('href'))
                        ) {
                            $data['kinopoisk_id'] = $kinopoiskId;
                        }
                    }
                    break;
                case 'Год:':
                    if (($value = pq('[itemprop="dateCreated"] a', $li)) && $value->text()) {
                        $data['year_start'] = (int) trim($value->text());
                    }
                    break;
                case 'Страна:':
                    foreach (pq('a', $li) as $value) {
                        if ($value->nodeValue) {
                            $attrs['страна'][] = trim($value->nodeValue);
                        }
                    }
                    break;
                case 'Жанр:':
                    $values = explode(', ', trim(str_replace('Жанр:', '', $li->nodeValue)));
                    if ($values) {
                        $attrs['жанр'] = $values;
                    }
                    break;
                case 'Режиссер:':
                    if (($value = pq('[itemprop="director"]', $li)) && $value->text()) {
                        $values = explode(', ', trim($value->text()));
                        $attrs['режиссер'] = $values;
                    }
                    break;
                case 'Актеры:':
                    foreach (pq('a', $li) as $value) {
                        if ($value->nodeValue) {
                            $attrs['в главных ролях'][] = trim($value->nodeValue);
                        }
                    }
                    break;
            }
        }

        foreach ($doc->find('.tranlators_list li') as $li) {
            $author = trim(explode("\r\n", $li->nodeValue)[0]);
            if (($info = pq('.tranlators_list_se', $li)) && $info->text()) {
                $author = trim(str_replace($info->text(), '', $author));
            }
            $authorId = $li->getAttribute('data-i');
            $src = $li->getAttribute('data-url');
            foreach ($doc->find(".episode_select[data-i=$authorId]") as $episodes) {
                $seasonNumber = (int) $episodes->getAttribute('data-s');
                foreach (pq('li', $episodes) as $episode) {
                    $seriesNumber = (int) $episode->getAttribute('data-e');
                    $videos[] = [
                        'season_number' => $seasonNumber,
                        'series_number' => $seriesNumber,
                        'src' => "{$src}?nocontrols=1&season={$seasonNumber}&episode={$seriesNumber}",
                        'author' => $author,
                    ];
                }
            }
        }

        return $this->prepareParserItemEvent(
            $parserItem,
            true,
            ['data' => $data, 'attrs' => $attrs, 'videos' => $videos]
        );
    }
}
