<?php

namespace App\Parsers;

use App\Models\ParserItem;
use App\Models\ParserItemEvent;

class KaduParser extends AbstractParser
{
    public function parseItems(): void
    {
        $url = $this->fullUrl('/series/');
        while ($url) {
            $this->log("parse page $url");
            sleep(2);
            $response = $this->request($url);
            if (!$response) {
                throw new \Exception('empty response');
            };

            $doc = \phpQuery::newDocumentHTML($response);
            $count = 0;
            foreach ($doc->find('#channels-content .channel-gallery li a') as $element) {
                $link = $element->getAttribute('href');
                $this->saveParserItem($this->fullUrl($link));
                $count++;

            }
            $next = $doc->find('#channels-content .pager #thread_next');
            $url = ($next && $next->attr('href'))
                ? $this->fullUrl($next->attr('href'))
                : null;

            $this->log("found items $count");
            $this->log('');
        }
    }

    public function parseItem(ParserItem $parserItem): ParserItemEvent
    {
        $this->log($parserItem->url);
        $response = $this->request($parserItem->url, null, null, $code);
        if ($code === 404) {
            return $this->prepareParserItemEvent($parserItem, false);
        } elseif (!$response) {
            throw new \Exception('empty response');
        };

        $doc = \phpQuery::newDocumentHTML($response);
        $data = [];
        $attrs = [];
        if ($name = $doc->find('h1')) {
            $name = str_replace('(сериал)', '', $name->text());
            $names = array_map('trim', explode('/', $name));
            $data['name_ru'] = $names[0];
            isset($names[1]) && $data['name_en'] = $names[1];
        }
        if ($img = $doc->find('img#avatar')) {
            $data['image'] = $img->attr('src');
        }
        if ($description = $doc->find('[itemprop=description]')) {
            $data['description'] = trim($description->text());
        }
        $attrName = null;
        foreach ($doc->find('#content-sidebar .block-side > *') as $elem) {
            $value = trim($elem->nodeValue);
            if ($elem->tagName === 'b') {
                $attrName = $value;
                continue;
            }
            if ($elem->tagName !== 'a') {
                continue;
            }
            switch ($attrName) {
                case 'Год':
                    $data['year_start'] = $value;
                    break;
                case 'Производство':
                    $attrs['страна'][] = $value;
                    break;
                case 'Режиссёр':
                    $attrs['режиссер'][] = $value;
                    break;
                case 'Актеры':
                    $attrs['в главных ролях'][] = $value;
                    break;
            }
        }
        $videos = $this->parseVideos($doc);

        return $this->prepareParserItemEvent(
            $parserItem,
            true,
            ['data' => $data, 'attrs' => $attrs, 'videos' => $videos]
        );
    }

    protected function parseVideos($doc): array
    {
        $videos = [];
        $seasons = [];
        foreach ($doc->find('#channel-category-line li:not(.invisible) a') as $elem) {
            if ($elem->nodeValue === 'Все') {
                continue;
            }
            $number = null;
            if (
                preg_match_all('/[Сс]езон ([0-9]+)/i', $elem->nodeValue, $matches)
                || preg_match_all('/([0-9]+) [Сс]езон/i', $elem->nodeValue, $matches)
            ) {
                $number = $matches[1][0];
            }
            $seasons[] = [
                'number' => $number,
                'url' => $this->fullUrl($elem->getAttribute('href')),
            ];
        }
        foreach ($seasons as $season) {
            $this->log("parse season {$season['number']}");
            usleep(200);
            $response = $this->request($season['url']);
            if (!$response) {
                throw new \Exception('empty response');
            };
            $doc = \phpQuery::newDocumentHTML($response);

            while (true) {
                foreach ($doc->find('[itemprop=itemListElement]') as $elem) {
                    $name = pq('a', $elem)->attr('title');
                    $seasonNumber = null;
                    $seriesNumber = null;
                    if (preg_match_all('/([0-9]+) сезон/', $name, $matches)) {
                        $seasonNumber = $matches[1][0];
                    }
                    if (preg_match_all('/([0-9]+) серия/', $name, $matches)) {
                        $seriesNumber = $matches[1][0];
                    }
                    $seasonNumber = $seasonNumber ?? $season['number'];
                    if (!$seasonNumber || !$seriesNumber) {
                        continue;
                    }
                    $id = str_replace('v-', '', $elem->getAttribute('id'));
                    $videos[] = [
                        'season_number' => $seasonNumber,
                        'series_number' => $seriesNumber,
                        'src' => "http://kadu.ru/embed/$id",
                    ];
                }
                if (($next = $doc->find('#thread_next')) && $next->attr('href')) {
                    usleep(200);
                    $response = $this->request($this->fullUrl($next->attr('href')));
                    if (!$response) {
                        throw new \Exception('empty response');
                    };
                    $doc = \phpQuery::newDocumentHTML($response);
                    continue;
                }
                break;
            }
        }

        return $videos;
    }

    protected function request(string $url, string $method = null, ?array $data = null, &$statusCode = null): ?string
    {
        $result = parent::request($url, $method, $data);
        $result = mb_convert_encoding($result, "HTML-ENTITIES", 'Windows-1251');
        $result = iconv('Windows-1251', 'UTF-8//IGNORE', $result);

        return $result;
    }
}
