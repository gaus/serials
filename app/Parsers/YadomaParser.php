<?php

namespace App\Parsers;

use App\Models\ParserItem;
use App\Models\ParserItemEvent;

class YadomaParser extends AbstractParser
{
    public function parseItems(): void
    {
        $this->log('parse categories list');
        $response = $this->request($this->fullUrl('/video/serialy'));
        if (!$response) {
            throw new \Exception('empty response');
        }

        $doc = \phpQuery::newDocumentHTML($response);
        foreach ($doc->find('.media_line .media_line_item a:first-child') as $n => $elem) {
            if ($n === 0) continue;
            $categoryUrl = $this->fullUrl($elem->getAttribute('href'));
            $offset = $n === 1 ? 576 : 0;
            while (true) {
                $count = $this->parsePageList("$categoryUrl?order=date&offset=$offset");
                if (!$count) {
                    break;
                }
                $offset += $count;
            }
        }
    }

    public function parseItem(ParserItem $parserItem): ParserItemEvent
    {
        $this->log($parserItem->url);
//        sleep(2);
        $response = $this->request($parserItem->url, null, null, $code);
        if ($code === 404) {
            return $this->prepareParserItemEvent($parserItem, false);
        } elseif (!$response) {
            throw new \Exception('empty response');
        }

        $doc = \phpQuery::newDocumentHTML($response);
        $data = [];
        $attrs = [];
        $videos = [];
        if ($img = $doc->find('#media_poster img')) {
            $img->attr('src') && $data['image'] = 'http:'.$img->attr('src');
        }
        if ($description = $doc->find('#media_description')) {
            $description->text() && $data['description'] = trim($description->text(), "\r\n");
        }
        foreach ($doc->find('table.info tr') as $tr) {
            $td1 = pq('td:first-child', $tr);
            $td2 = pq('td:last-child', $tr);
            $type = trim($td1->text(), ':');
            switch ($type) {
                case 'Название':
                    $td2->text() && $data['name_ru'] = $td2->text();
                    break;
                case 'В оригинале':
                    $td2->text() && $data['name_original'] = $td2->text();
                    break;
                case 'Год выхода':
                    $td2->text() && $data['year_start'] = $td2->text();
                    break;
                case 'Жанр':
                    $values = [];
                    foreach (pq('a', $td2) as $v) {
                        $values[] = mb_strtolower($v->nodeValue);
                    }
                    $values && $attrs['жанр'] = $values;
                    break;
                case 'Режиссер':
                    $values = [];
                    foreach (pq('span', $td2) as $v) {
                        $values[] = $v->nodeValue;
                    }
                    $values && $attrs['режиссер'] = $values;
                    break;
                case 'В ролях':
                    $values = [];
                    foreach (pq('span', $td2) as $v) {
                        $values[] = $v->nodeValue;
                    }
                    $values && $attrs['в главных ролях'] = $values;
                    break;
                case 'Страна':
                    $values = [];
                    foreach (pq('a', $td2) as $v) {
                        $values[] = $v->nodeValue;
                    }
                    $values && $attrs['страна'] = $values;
                    !$values && $td2->text() && $attrs['страна'] = explode(',', $td2->text());
                    break;
            }
        }
        foreach ($doc->find('.media_list.season-tab .media_list_item') as $elem) {
            $videos[] = $this->parseVideoBlock($elem);
        }
        $onPage = 25;
        if (count($videos) >= $onPage) {
            $uid = (int) str_replace('https://yadoma.tv/video/', '', $parserItem->url);
            $offset = count($videos);
            while (true) {
                $this->log("parse video list - offset $offset");
//                sleep(1);
                $response = $this->request("https://api.yadoma.tv/season_series.php?uid=$uid&direction=next&last=$offset&mode=append");
                if (!$response) {
                    throw new \Exception('empty response');
                }
                $response = json_decode($response, true);
                $docMore = \phpQuery::newDocumentHTML($response['content']);
                foreach ($docMore->find('.media_list_item') as $elem) {
                    $videos[] = $this->parseVideoBlock($elem);
                }
                if ($response['loaded'] < $onPage) {
                    break;
                }
                $offset = count($videos);
            }
        }

        return $this->prepareParserItemEvent(
            $parserItem,
            true,
            ['data' => $data, 'attrs' => $attrs, 'videos' => $videos]
        );
    }

    protected function parseVideoBlock($elem): array
    {
        $videoId = $elem->getAttribute('data-uid');
        $link = pq('a.heading', $elem);
        $name = explode(',', $link->text());
        if (count($name) >= 2) {
            $seasonNumber = (int) trim(str_replace('Сезон', '', $name[0]));
            $seriesNumber = (int) trim(str_replace('Серия', '', $name[1]));
        } else {
            $seasonNumber = 1;
            $seriesNumber = (int) trim(str_replace('Серия', '', $name[0] ?? '1'));
        }

        return [
            'season_number' => $seasonNumber,
            'series_number' => $seriesNumber,
            'video_type_id' => 1,
            'src' => "https://api.yadoma.tv/ifr_player.php?mid=$videoId&type=html5&r=core",
        ];
    }

    protected function parsePageList(string $url): int
    {
        $this->log($url);
        sleep(2);
        $response = $this->request($url, null, null, $statusCode);
        if ($statusCode === 404) {
            $this->log('404 error');
            return 0;
        }
        if (!$response) {
            throw new \Exception('empty response');
        }

        $doc = \phpQuery::newDocumentHTML($response);
        $count = 0;
        foreach ($doc->find('.media_line .media_line_item a:first-child') as $elem) {
            $this->parseSeasonsList($this->fullUrl($elem->getAttribute('href')));
            $count++;
        }
        $this->log("items found $count");
        $this->log('');

        return $count;
    }

    protected function parseSeasonsList(string $url): void
    {
        $this->log($url);
        sleep(2);
        $response = $this->request($url, null, null, $statusCode);
        if ($statusCode === 404) {
            $this->log('404 error');
            return;
        }
        if (!$response) {
            throw new \Exception('empty response');
        }

        $doc = \phpQuery::newDocumentHTML($response);
        $count = 0;
        foreach ($doc->find('.seasons_list .season-tab-btn') as $elem) {
            $this->saveParserItem($this->fullUrl($elem->getAttribute('href')));
            $count++;
        }
        $this->log("seasons found $count");
    }
}
