<?php

namespace App\Parsers;

use App\Models\ParserItem;
use App\Models\ParserItemEvent;

class KinopoiskParser extends AbstractParser
{
    public function parseItems(): void
    {
        for ($p = 1; $p <= 300; $p++) {
            $this->log("parse page $p");
            $count = $this->parsePageList("/s/type/film/list/1/order/year/m_act%5Bfrom_year%5D/2009/m_act%5Bto_year%5D/2018/m_act%5Btype%5D/serial/page/$p/");
            $this->log('');
            if (!$count) {
                break;
            }
        }
    }

    public function parseItem(ParserItem $parserItem): ParserItemEvent
    {
        $this->log("parse {$parserItem->url}");
        $this->log('parse item info');
//        sleep(1);
        [$data, $attrs] = $this->parseItemData($parserItem->url);
        $this->log('parse item series');
//        sleep(1);
        $series = $this->parseItemSeries("{$parserItem->url}episodes/");
        $this->log('');

        return $this->prepareParserItemEvent(
            $parserItem,
            true,
            ['data' => $data, 'attrs' => $attrs, 'series' => $series]
        );
    }

    protected function parsePageList(string $url): int
    {
        sleep(3);
        $result = $this->request($this->fullUrl($url));
        if (!$result) {
            throw new \Exception('empty response');
        };
        $doc = \phpQuery::newDocumentHTML($result);
        $count = 0;
        foreach ($doc->find('.search_results .element') as $element) {
            if (($name = pq('.name a', $element)) && $kinopoiskId = $name->attr('data-id')) {
                $this->saveParserItem($this->fullUrl("/film/$kinopoiskId/"));
                $count++;
            }
        }
        $this->log("items $count");

        return $count;
    }

    protected function parseItemData(string $url): array
    {
        $result = $this->request($url);
        if (!$result) {
            throw new \Exception('empty response');
        }

        $data = [];
        $attrs = [];
        $doc = \phpQuery::newDocumentHTML($result);
        if ($kinopoiskId = $this->getKinopoiskIdByUrl($url)) {
            $data['kinopoisk_id'] = $kinopoiskId;
        }
        if ($name = $doc->find('h1.moviename-big')) {
            $data['name_ru'] = $name->text();
            $data['name_ru'] = preg_replace('/\\(.*$/', '', $data['name_ru']);
        }
        if ($nameEn = $doc->find('#headerFilm [itemprop="alternativeHeadline"]')) {
            $data['name_en'] = $nameEn->text();
        }
        if ($image = $doc->find('.popupBigImage img')) {
            $image->attr('src') && strpos($image->attr('src'), 'poster_none') === false
                && $data['image'] = $image->attr('src');
        }
        if ($years = $doc->find('h1.moviename-big a')) {
            $yearsArr = explode(' – ', $years->text());
            if ($yearsArr[0] && is_numeric($yearsArr[0])) {
                $data['year_start'] = $yearsArr[0];
            }
            if (isset($yearsArr[1]) && is_numeric($yearsArr[1])) {
                $data['year_complete'] = $yearsArr[1];
            }
        }
        if ($description = $doc->find('.film-synopsys')) {
            $data['description'] = $description->text();
        }
        if ($runtime = $doc->find('#runtime')) {
            $data['duration'] = (int)$runtime->text();
        }
        if ($rating = $doc->find('.rating_ball')) {
            $data['rating_kp'] = $rating->text();
        }
        $actors = [];
        foreach ($doc->find('[itemprop="actors"] a') as $value) {
            if ($value->nodeValue != '...') {
                $actors[] = $value->nodeValue;
            }
        }
        if (!empty($actors)) {
            $attrs['в главных ролях'] = $actors;
        }
        foreach ($doc->find('#infoTable tr') as $tr) {
            $td1 = pq('td:first-child', $tr);
            $td2 = pq('td:last-child', $tr);
            $attrName = $td1->text();
            $attrValue = null;
            switch ($attrName) {
                case 'страна':
                case 'режиссер':
                case 'жанр':
                    $attrValue = [];
                    foreach (pq('a', $td2) as $value) {
                        if (!in_array($value->nodeValue, ['...', 'слова'])) {
                            $attrValue[] = $value->nodeValue;
                        }
                    }
                    break;
                case 'премьера (мир)':
                    if ($v = pq('.prem_ical', $td2)) {
                        $itemArr['started_at'] = preg_replace(
                            '/^([0-9]{4})([0-9]{2})([0-9]{2})$/',
                            "$1-$2-$3",
                            $v->attr('data-date-premier-start-link')
                        );
                    }
                    break;
                case 'премьера (РФ)':
                    if (empty($itemArr['started_at']) && $value = pq('.prem_ical', $td2)) {
                        $itemArr['started_at'] = preg_replace(
                            '/^([0-9]{4})([0-9]{2})([0-9]{2})$/',
                            "$1-$2-$3",
                            $value->attr('data-date-premier-start-link')
                        );
                    }
                    break;
                case 'возраст':
                    if ($value = pq('span', $td2)) {
                        $attrValue = [preg_replace('/[^0-9]/', '', $value->text()).'+'];
                    }
                    break;
            }
            if (!empty($attrName) && !empty($attrValue)) {
                $attrs[$attrName] = $attrValue;
            }
        }
        $data = array_map('trim', array_filter($data));

        return [$data, $attrs];
    }

    protected function parseItemSeries(string $url)
    {
        $result = $this->request($url);
        if (!$result) {
            throw new \Exception('empty response');
        }

        $doc = \phpQuery::newDocumentHTML($result);
        $seasons = [];
        foreach ($doc->find('#hidden_form ~ table table table') as $seasonTable) {
            $season = [];
            $series = [];
            foreach (pq('tr', $seasonTable) as $tr) {
                $h1 = pq('h1', $tr)->text();
                if (!$h1) {
                    $h1 = pq('h1 b', $tr)->text();
                }
                $span = pq('span:not(.episodesOriginalName)', $tr)->text();
                $spanOrigin = pq('.episodesOriginalName', $tr)->text();
                $date = pq('.news', $tr)->text();
                if (!$h1 && !$span) {
                    continue;
                }
                if (strpos($h1, 'Сезон ') !== false) {
                    $season['number'] = (int)str_replace('Сезон ', '', $h1);
                    continue;
                }
                if (strpos($span, 'Эпизод ') !== false) {
                    $seria = [];
                    $seria['number'] = (int)str_replace('Эпизод ', '', $span);
                    $h1 && $seria['name_ru'] = $h1;
                    $spanOrigin && $seria['name_original'] = $spanOrigin;
                    if ($date) {
                        $date = $this->parseDate($date);
                        $date && $seria['released_at'] = $date;
                    }
                    $series[] = $seria;
                }
            }
            $series && $season['series'] = $series;
            $season && $seasons[] = $season;
        }

        return $seasons;
    }

    protected function parseDate(string $date): ?string
    {
        if (!preg_match_all('/([0-9]{2}).*?([а-яА-Я]+).*?([0-9]{4})/m', $date, $matches)) {
            return null;
        }
        $monthes = [
            'января' =>   '01',
            'февраля' =>  '02',
            'марта' =>    '03',
            'апреля' =>   '04',
            'мая' =>      '05',
            'июня' =>     '06',
            'июля' =>     '07',
            'августа' =>  '08',
            'сентября' => '09',
            'октября' =>  '10',
            'ноября' =>   '11',
            'декабря' =>  '12',
        ];
        $month = null;
        foreach ($monthes as $key => $value) {
            if (preg_match("/$key/", $matches[2][0]) !== false) {
                $month = $value;
                break;
            }
        }

        return $month
            ? "{$matches[3][0]}-{$month}-{$matches[1][0]}"
            : null;
    }

    protected function request(string $url, string $method = null, ?array $data = null, &$statusCode = null): ?string
    {
        $result = parent::request($url, $method, $data);
        $result = mb_convert_encoding($result, "HTML-ENTITIES", 'Windows-1251');
        $result = iconv('Windows-1251', 'UTF-8//IGNORE', $result);

        return $result;
    }

    public function addParserItemByKinopoiskId(int $kinopoiskId): ParserItem
    {
        return $this->saveParserItem($this->fullUrl("/film/$kinopoiskId/"));
    }
}
