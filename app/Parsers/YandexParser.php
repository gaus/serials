<?php

namespace App\Parsers;

use App\Models\ParserItem;

class YandexParser extends AbstractParser
{
//    protected $useProxy = true;

    public function parseItems(): void
    {
        // TODO: Implement parseItems() method.
    }

    public function parseItem(ParserItem $parserItem): void
    {
        $this->log($parserItem->url);
        sleep(2);
        $response = $this->request($parserItem->url);
        if (!$response) {
            throw new \Exception('empty response');
        }

        $doc = \phpQuery::newDocumentHTML($response);
        $videos = [];
        foreach ($doc->find('.series-navigator__season-control .radio-button__text') as $seasonElem) {
            $seasonNumber = $seasonElem->nodeValue;
            $seasonVideos = $this->parseItemSeasonPage("{$parserItem->url}{$seasonNumber}-сезон/", $seasonNumber);
            $videos = array_merge($videos, $seasonVideos);
            dd($videos);
        }
        dd($videos);
    }

    protected function parseItemSeasonPage(string $url, string $seasonNumber): array
    {
        $this->log($url);
        sleep(10);
        $response = $this->request($url);
        if (!$response) {
            throw new \Exception('empty response');
        }

        $doc = \phpQuery::newDocumentHTML($response);
        $seasonVideos = [];
        foreach ($doc->find('.series-navigator__episode-row .radio-button__text') as $seriesElem) {
            $seriesNumber = $seriesElem->nodeValue;
            $src = $this->parseItemVideo("");
            if ($src) {
                $seasonVideos[] = [
                    'src' => $src,
                    'season_number' => $seasonNumber,
                    'series_number' => $seriesNumber,
                ];
            }
        }

        return $seasonVideos;
    }

    protected function parseItemVideo(string $url): ?string
    {
        $this->log($url);
        sleep(10);
        $response = $this->request($url);
        if (!$response) {
            throw new \Exception('empty response');
        }

        $doc = \phpQuery::newDocumentHTML($response);
        if ($iframe = $doc->find('.sandbox_player-id')) {
            $src = $iframe->attr('src');
        }

        return $src ?? null;
    }
}
