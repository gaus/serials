<?php

namespace App\Parsers;

use App\Models\ParserItem;
use App\Models\ParserItemEvent;
use Illuminate\Console\Command;

abstract class AbstractParser
{
    /**
     * @var \App\Models\Parser
     */
    protected $parser;

    protected $useProxy = false;

    protected $proxyList = [
        '46.105.57.150:62750',
        '46.105.57.150:12846',
        '46.228.8.90:1080',
        '45.119.112.36:33211',
        '5.152.206.37:1080',
        '46.111.1.13:1080',
        '46.180.156.126:1080',
        '45.119.112.45:33211',
        '46.150.174.90:13311',
        '46.150.167.173:1080',
        '42.115.91.82:33012',
        '46.33.232.63:1080',
        '45.119.112.43:33211',
        '46.10.157.124:55580',
        '37.229.126.233:39880',
        '37.59.8.29:1034',
    ];

    /**
     * @var Command
     */
    protected $console;

    public abstract function parseItems(): void;

    public abstract function parseItem(ParserItem $parserItem): ParserItemEvent;

    public function __construct(\App\Models\Parser $parser)
    {
        $this->parser = $parser;
    }

    public function setConsole(Command $console): void
    {
        $this->console = $console;
    }

    protected function log(string $message): void
    {
        $this->console && $this->console->line($message);
    }

    protected function fullUrl(string $path): string
    {
        if (!starts_with($path, '/') && !ends_with($this->parser->url, '/')) {
            $path = "/$path";
        }

        return $this->parser->url . $path;
    }

    protected function saveParserItem(string $url): ParserItem
    {
        /** @var ParserItem $result */
        $result = ParserItem::query()
            ->firstOrCreate(['parser_id' => $this->parser->id, 'url' => $url]);

        return $result;
    }

    protected function prepareParserItemEvent(ParserItem $parserItem, bool $success, ?array $data = null): ParserItemEvent
    {
        $result = new ParserItemEvent();
        $result->parser_item_id = $parserItem->id;
        $result->success = $success;
        $result->data = $data;

        return $result;
    }

    protected function getKinopoiskIdByUrl(string $url): ?int
    {
        $result = null;
        if (preg_match('/([0-9]+)\/$/', $url, $matches)) {
            $result = (int) $matches[1];
        }

        return $result;
    }

    protected function request(string $url, string $method = null, ?array $data = null, &$statusCode = null): ?string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//        $this->cookie && curl_setopt($ch, CURLOPT_COOKIE, $this->cookie);
//        $this->cookie && curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        $method && curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        $data && curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
            'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
            'Referer: https://www.google.ru/',
        ]);
        if ($this->useProxy) {
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
            curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, false);
//            curl_setopt($ch, CURLOPT_PROXYPORT, '');
            curl_setopt($ch, CURLOPT_PROXY, array_rand($this->proxyList));
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        }
//        if ($this->requestInterval && $this->requestCount > 0) {
//            sleep($this->requestInterval);
//        }
        $result = curl_exec($ch);
//        $this->requestCount++;
        $info = curl_getinfo($ch);
        curl_close($ch);
        $statusCode = $info['http_code'];

//        dd($info);
//        echo htmlentities($result); exit;
//        $info['http_code'] !== 200 && dd($info['http_code'], $result);
        return $info['http_code'] === 200
            ? $result
            : null;
    }
}
