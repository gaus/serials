<?php

namespace App\Parsers;

use App\Models\ParserItem;
use App\Models\ParserItemEvent;

class IviParser extends AbstractParser
{
    protected $listPages = [
        '/mobileapi/catalogue/v5/?sort=pop&genre=171&genre_operator=and&fields=id%2Ctitle%2Cfake%2Cpreorderable%2Cavailable_in_countries%2Chru%2Cposter_originals%2Ccontent_paid_types%2Ccompilation_hru%2Ckind%2Cadditional_data%2Crestrict%2Chd_available%2C3d_available%2Cuhd_available%2Chdr10_available%2Chas_5_1%2Cseasons_count%2Cseasons_content_total%2Cseasons%2Cepisodes%2Cseasons_description%2Civi_rating_10_count%2Cseasons_extra_info%2Ccount%2Cused_to_be_paid&from={from}&to={to}&app_version=870&session=4b506d085133401081130355_1546859703-0H1fYderHCZLjO8dhmjaDlg',
        '/mobileapi/catalogue/v5/?sort=pop&paid_type=AVOD&genre_operator=and&category=15&fields=id%2Ctitle%2Cfake%2Cpreorderable%2Cavailable_in_countries%2Chru%2Cposter_originals%2Ccontent_paid_types%2Ccompilation_hru%2Ckind%2Cadditional_data%2Crestrict%2Chd_available%2C3d_available%2Cuhd_available%2Chdr10_available%2Chas_5_1%2Cseasons_count%2Cseasons_content_total%2Cseasons%2Cepisodes%2Cseasons_description%2Civi_rating_10_count%2Cseasons_extra_info%2Ccount%2Cused_to_be_paid&from={from}&to={to}&app_version=870&session=4b506d085133401081130355_1546859703-0H1fYderHCZLjO8dhmjaDlg',
    ];

    public function parseItems(): void
    {
        $perPage = 30;
        foreach ($this->listPages as $pageUrl) {
            $from = 0;
            while (true) {
                $to = $from + $perPage - 1;
                $this->log("parse items $from - $to");
                sleep(1);
                $url = $this->fullUrl(str_replace(['from', 'to'], [$from, $to], $pageUrl));
                $response = $this->request($url);
                if (!$response) {
                    throw new \Exception('empty response');
                };

                $json = json_decode($response, true);
                foreach ($json['result'] as $item) {
                    $this->saveParserItem("https://www.ivi.ru/ajax/info/{$item['hru']}?type=json");
                }
                if (!count($json['result'])) {
                    break;
                }
                $from += $perPage;
            }
        }
    }

    public function parseItem(ParserItem $parserItem): ParserItemEvent
    {
        $this->log($parserItem->url);
        $response = $this->request($parserItem->url, null, null, $code);
        if ($code === 404) {
            return $this->prepareParserItemEvent($parserItem, false);
        } elseif (!$response) {
            throw new \Exception('empty response');
        };

        $data = [];
        $attrs = [];
        $videos = [];
        $json = json_decode($response, true);
        $id = $json['compilation']['id'];
        $data['year_start'] = $json['compilation']['year'];
        $data['description'] = $json['compilation']['description'];
        if (isset($json['compilation']['country']) && isset($json['countryList'][$json['compilation']['country']]['title']))
        $attrs['страна'][] = $json['countryList'][$json['compilation']['country']]['title'];

        $from = 0;
        $perPage = 100;
        $first = true;
        while (true) {
            $to = $from + $perPage - 1;
            $this->log("parse videos $from - $to");
            sleep(1);
            $response = $this->request($this->fullUrl("/mobileapi/videofromcompilation/v5/?id=$id&from=$from&to=$to"));
            if (!$response) {
                throw new \Exception('empty response');
            };

            $json = json_decode($response, true);
            foreach ($json['result'] as $item) {
                if ($first) {
                    isset ($item['kp_id']) && $data['kinopoisk_id'] = $item['kp_id'];
                    isset ($item['duration_minutes']) && $data['duration'] = $item['duration_minutes'];
                    isset ($item['compilation_title']) && $data['name_ru'] = $item['compilation_title'];
                    if (isset($item['poster_originals'][0]['path'])) {
                        $data['image'] = $item['poster_originals'][0]['path'];
                    }
                    $first = false;
                }
                $videos[] = [
                    'season_number' => $item['season'] ?? 1,
                    'series_number' => $item['episode'],
                    'src' => "https://www.ivi.ru/embeds/video/?id={$item['id']}&autostart=0&app_version=870",
                ];
            }
            if (!count($json['result'])) {
                break;
            }
            $from += $perPage;
        }

        return $this->prepareParserItemEvent(
            $parserItem,
            true,
            ['data' => $data, 'attrs' => $attrs, 'videos' => $videos]
        );
    }
}
