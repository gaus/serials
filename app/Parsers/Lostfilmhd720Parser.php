<?php

namespace App\Parsers;

use App\Models\ParserItem;
use App\Models\ParserItemEvent;

class Lostfilmhd720Parser extends AbstractParser
{
    public function parseItems(): void
    {
        $page = 1;
        $do = true;
        while ($do) {
            $this->log("parse page $page");
            sleep(2);
            $do = (bool) $this->parsePageList($this->fullUrl("/page/$page/"));
            $this->log('');
            $page++;
        }
    }

    public function parseItem(ParserItem $parserItem): ParserItemEvent
    {
        $this->log($parserItem->url);
//        sleep(2);
        $response = $this->request($parserItem->url, null, null, $code);
        if ($code === 404) {
            return $this->prepareParserItemEvent($parserItem, false);
        } elseif (!$response) {
            throw new \Exception('empty response');
        }

        $doc = \phpQuery::newDocumentHTML($response);
        $data = [];
        $attrs = [];
        $videos = [];
        $author = null;
        if ($name = $doc->find('.full-title h1')->text()) {
            $data['name_ru'] = trim(substr($name, 0, strpos($name, 'смотреть онлайн')));
        }
        if ($description = $doc->find('.desc-text div')->text()) {
            $data['description'] = trim($description);
        }
        foreach ($doc->find('.m-info .info-item') as $row) {
            $label = trim(pq('.info-label', $row)->text(), ':');
            $values = [];
            foreach (pq('.info-desc a', $row) as $value) {
                $values[] = trim($value->nodeValue);
            }
            if (!$values) {
                continue;
            }
            switch ($label) {
                case 'Жанр':
                    $attrs['жанр'] = array_map('mb_strtolower', $values);
                    $pos = array_search('сериал', $attrs['жанр']);
                    if ($pos !== false) {
                        unset($attrs['жанр'][$pos]);
                    }
                    break;
                case 'Страна':
                    $attrs['страна'] = $values;
                    break;
                case 'Режиссер':
                    $attrs['режиссер'] = $values;
                    break;
                case 'Актеры':
                    $attrs['в главных ролях'] = $values;
                    break;
                case 'Дата выхода':
                    isset($values[0]) && $data['year_start'] = $values[0];
                    break;
                case 'Перевод':
                    $author = $values[0] ?? null;
                    break;
            }
        }
        if (preg_match('/RalodePlayer\.init\((.+?),\{"scount"/m', $response, $matches)) {
            $videoJson = json_decode($matches[1], true);
            foreach ($videoJson as $item) {
                if (empty($item['items']) || empty($item['name'])) {
                    continue;
                }
                foreach ($item['items'] as $item2) {
                    if (
                        empty($item2['sname'])
                        || empty($item2['scode_begin'])
                        || $item2['sname'] === 'Трейлер'
                        || strpos($item2['scode_begin'], 'http://moonwalk.cc') !== false
                    ) {
                        continue;
                    }
                    $videos[] = [
                        'src' => last(explode('::', $item2['scode_begin'])),
                        'season_number' => (int) $item['name'],
                        'series_number' => (int) $item2['sname'],
                        'video_type' => 1,
                        'author' => $author,
                    ];
                }
            }
        }

        return $this->prepareParserItemEvent(
            $parserItem,
            true,
            ['data' => $data, 'attrs' => $attrs, 'videos' => $videos]
        );
    }

    protected function parsePageList(string $url): int
    {
        $response = $this->request($url);
        if (!$response) {
            throw new \Exception('empty response');
        }

        $count = 0;
        $doc = \phpQuery::newDocumentHTML($response);
        foreach ($doc->find('.short-item') as $elem) {
            if ($link = pq('.short-info a', $elem)->attr('href')) {
                $this->saveParserItem($link);
                $count++;
            }
        }
        $this->log("items found $count");

        return $count;
    }
}
