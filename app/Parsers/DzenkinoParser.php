<?php

namespace App\Parsers;

use App\Models\ParserItem;
use App\Models\ParserItemEvent;

class DzenkinoParser extends AbstractParser
{
    public function parseItems(): void
    {
        $p = 1;
        while (true) {
            $url = $this->fullUrl("/serial-page-$p");
            $this->log("parse page $url");
            usleep(500);
            $response = $this->request($url);
            if (!$response) {
                throw new \Exception('empty response');
            };

            $doc = \phpQuery::newDocumentHTML($response);
            $count = 0;
            foreach ($doc->find('.otherFilmItem a') as $elem) {
                $itemUrl = $this->fullUrl($elem->getAttribute('href'));
                $this->saveParserItem($itemUrl);
                $count++;
            }
            if (!$count) {
                break;
            }
            $p++;
        }
    }

    public function parseItem(ParserItem $parserItem): ParserItemEvent
    {
        $this->log($parserItem->url);
        $response = $this->request($parserItem->url);
        if (!$response) {
            throw new \Exception('empty response');
        };

        $doc = \phpQuery::newDocumentHTML($response);
        $data = [];
        $attrs = [];
        $videos = [];
        $videoAuthor = null;
        $name = $doc->find('h2')->text();
        $data['name_ru'] = trim(str_replace(['HDr', 'HDs', 'HD'], '', $name));
        $image = $doc->find('.movie_poster');
        if ($image && $image->attr('src')) {
            $data['image'] = $image->attr('src');
        }
        foreach ($doc->find('table.info tr') as $tr) {
            $label = trim(pq('td:first-child', $tr)->text());
            $values = [];
            foreach (pq('a', $tr) as $value) {
                $values[] = trim($value->nodeValue);
            }
            switch ($label) {
                case 'Дата первого показа:':
                case 'Год:':
                    $data['year_start'] = reset($values);
                    break;
                case 'Режиссер:':
                    $attrs['режиссер'] = $values;
                    break;
                case 'Жанр:':
                    $attrs['жанр'] = $values;
                    break;
                case 'Страна:':
                    $attrs['страна'] = $values;
                    break;
                case 'Озвучка:':
                    $videoAuthor = reset($values);
                    break;
            }
        }
        if (\in_array($videoAuthor, ['Оригинальная (русская)', 'Не требуется', 'Оригинальная'])) {
            $videoAuthor = null;
        }
        foreach ($doc->find('#castfoto .personname a') as $actor) {
            $attrs['в главных ролях'][] = trim($actor->nodeValue);
        }
        foreach ($doc->find('.singleSerialItemsWrap a') as $seasonLink) {
            $newVideos = $this->parseSeasonVideos($this->fullUrl($seasonLink->getAttribute('href')));
            if ($videoAuthor) {
                foreach ($newVideos as &$newVideo) {
                    $newVideo['author'] = $videoAuthor;
                }
            }
            $videos = array_merge($videos, $newVideos);
        }

        return $this->prepareParserItemEvent(
            $parserItem,
            true,
            ['data' => $data, 'attrs' => $attrs, 'videos' => $videos]
        );
    }

    protected function parseSeasonVideos(string $url): array
    {
        $this->log($url);
        usleep(200);
        $response = $this->request($url);
        if (!$response) {
            throw new \Exception('empty response');
        };

        $doc = \phpQuery::newDocumentHTML($response);
        $videos = [];
        foreach ($doc->find('.singleSerialItemWrap a') as $elem) {
            $img = pq('img', $elem);
            if (
                !preg_match_all('/\/sezon-([0-9]+)-seria-([0-9]+)$/', $elem->getAttribute('href'), $matches)
                || !preg_match_all('/\/([^.\/]+)\.[^.]+$/', $img->attr('src'), $matches2)
            ) {
                continue;
            }
            $videos[] = [
                'season_number' => $matches[1][0],
                'series_number' => $matches[2][0],
                'src' => "http://player1532970000.dzenkinos.kz/service/commercial/{$matches2[1][0]}?illegal=1&autoplay=0"
            ];
        }

        return $videos;
    }
}
