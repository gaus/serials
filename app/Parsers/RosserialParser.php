<?php

namespace App\Parsers;

use App\Models\ParserItem;
use App\Models\ParserItemEvent;

class RosserialParser extends AbstractParser
{
    public function parseItems(): void
    {
        for ($y = (int)date('Y'); $y >= 1990; $y--) {
            $this->log("parse items for $y year");
            sleep(1);
            $this->parseListItems($this->fullUrl("/russkie_serialy/$y"));
            $this->log('');
        }
    }

    public function parseItem(ParserItem $parserItem): ParserItemEvent
    {
        $this->log($parserItem->url);
        $result = $this->request($parserItem->url, null, null, $code);
        if ($code === 404) {
            return $this->prepareParserItemEvent($parserItem, false);
        } elseif (!$result) {
            throw new \Exception('empty response');
        }

        $data = [];
        $attrs = [];
        $videos = [];
        $doc = \phpQuery::newDocumentHTML($result);
        if ($name = $doc->find('.serial-title [itemprop="name"]')->text()) {
            $data['name_ru'] = $name;
        }
        if ($description = $doc->find('.serial-info-description__txt')->text()) {
            $description = trim($description);
            $description = preg_replace('/Официальная страница.*$/m', '', $description);
            $data['description'] = $description;
        }
        foreach ($doc->find('.serial-info-box tr') as $tr) {
            $td1 = pq('th:first-child', $tr)->text();
            $td2 = pq('td:last-child', $tr)->text();
            if ($td1 == 'Год') {
                $data['year_start'] = (int)$td2;
            }
            if ($td1 == 'Страна') {
                $attrs['страна'] = explode(',', trim($td2));
            }
            if ($td1 == 'Жанр') {
                $td2 = mb_strtolower(trim($td2, '"'));
                $td2 = str_replace('криминальный сериал', 'криминал', $td2);
                $attrs['жанр'] = explode(',', $td2);
            }
            if ($td1 == 'Премьера') {
                $startedAt = $this->parseDate($td2);
                $startedAt && $data['started_at'] = $startedAt;
            }
            if ($td1 == 'Режиссёр') {
                $attrs['режиссер'] = explode(',', $td2);
            }
            if ($td1 == 'Актёры') {
                $actors = [];
                foreach (pq('.actors a[itemprop="name"]', $tr) as $actor) {
                    $actors[] = trim($actor->nodeValue);
                }
                !empty($actors) && $attrs['в главных ролях'] = $actors;
            }
        }
        foreach ($attrs as &$attr) {
            $attr = array_map(function ($value) {
                return preg_replace('/((^\s+)|(\s+$))/m', '', $value);
            }, $attr);
        }

        if ($season = $doc->find('.seasonnumberactive')->text()) {
            $seasonNumber = (int)$season;
        } else {
            $seasonNumber = 1;
        }
        foreach ($doc->find('.ul-playlist li') as $li) {
            $seriesNumber = (int)$li->nodeValue;
            $videoId = $li->getAttribute('id');
            $videoId = (int)str_replace('pl', '', $videoId);
            $videoSrc = $this->parseVideoSrc($videoId);
            $videoSrc && $videos[] = [
                'video_type_id' => 3,
                'src' => $videoSrc,
                'season_number' => $seasonNumber,
                'series_number' => $seriesNumber,
            ];
        }

        return $this->prepareParserItemEvent(
            $parserItem,
            true,
            ['data' => $data, 'attrs' => $attrs, 'videos' => $videos]
        );
    }

    protected function parseListItems(string $url): void
    {
        $page = 1;
        $this->log("parse page $page");
        $response = $this->request($url);
        if (!$response) {
            throw new \Exception('empty response');
        }

        while ($response) {
            $doc = \phpQuery::newDocumentHTML($response);
            $this->parseItemsFromDocument($doc);
            if (preg_match('/getListserials\(([0-9]+),\s*([0-9]+)\)/m', $response, $matches)) {
                $response = null;
                $page++;
                $this->log("parse page $page");
                sleep(1);
                $response = $this->request(
                    'http://rosserial.net/getlistserial/',
                    'POST',
                    ['finishid' => $matches[1], 'year' => $matches[2]]
                );
            } else {
                $response = null;
            }
        }
    }

    protected function parseItemsFromDocument($doc): void
    {
        $count = 0;
        foreach ($doc->find('.oneserial') as $element) {
            $link = pq('> a', $element)->attr('href');
            $link && $this->saveParserItem($this->fullUrl($link));
            $count++;
        }
        $this->log("items found $count");
    }

    protected function parseVideoSrc(int $videoId)
    {
        $url = $this->fullUrl('/player/');
//        usleep(300);
        $result = $this->request($url, 'POST', ['id' => $videoId]);
        $doc = \phpQuery::newDocumentHTML($result);
        $src = $doc->find('iframe')->attr('src');
        $src = str_replace('"', '', $src);
        $src = str_replace('\\', '', $src);

        return $src;
    }

    protected function parseDate(string $date)
    {
        if (!preg_match_all('/([0-9]{2}).*?([а-яА-Я]+).*?([0-9]{4})/m', $date, $matches)) {
            return null;
        }
        $monthes = [
            'января' =>   '01',
            'февраля' =>  '02',
            'марта' =>    '03',
            'апреля' =>   '04',
            'мая' =>      '05',
            'июня' =>     '06',
            'июля' =>     '07',
            'августа' =>  '08',
            'сентября' => '09',
            'октября' =>  '10',
            'ноября' =>   '11',
            'декабря' =>  '12',
        ];
        $month = null;
        foreach ($monthes as $key => $value) {
            if (strpos($matches[0][0], $key) !== false) {
                $month = $value;
                break;
            }
        }
        if (!$month) {
            return null;
        }

        return $matches[3][0].'-'.$month.'-'.$matches[1][0];
    }
}
