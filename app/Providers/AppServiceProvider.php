<?php

namespace App\Providers;

use Api\v2\Models\UserRating;
use Api\v2\Models\UserWatch;
use App\UserRatingObserver;
use App\UserWatchObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        UserRating::observe(UserRatingObserver::class);
        UserWatch::observe(UserWatchObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
