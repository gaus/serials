<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->string('name_ru')->default('');
            $table->string('name_en')->default('');
            $table->string('name_original')->default('');
            $table->text('description')->default('');
            $table->string('image', 64)->default('');
            $table->smallInteger('year_start')->nullable();
            $table->smallInteger('year_complete')->nullable();
            $table->date('started_at')->nullable();
            $table->date('completed_at')->nullable();
            $table->integer('season_count')->nullable();
            $table->integer('series_count')->nullable();
            $table->smallInteger('duration')->nullable();
            $table->decimal('rating', 3, 1)->nullable();
            $table->decimal('rating_kp', 3, 1)->nullable();
            $table->decimal('rating_imdb', 3, 1)->nullable();
            $table->integer('kinopoisk_id')->nullable();
            $table->json('attrs')->nullable();
            $table->json('video_types')->nullable();
            $table->boolean('published')->default(false);
            $table->timestamps();

            $table->index('category_id', 'idx_items_category_id');
            $table->index('started_at', 'idx_items_started_at');
            $table->index('completed_at', 'idx_items_completed_at');
            $table->index('rating', 'idx_items_rating');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
