<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParserItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parser_items', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('parser_id');
            $table->integer('item_id')->nullable();
            $table->string('url', 1000);
            $table->boolean('is_active')->default(true);
            $table->dateTime('last_activity')->nullable();
            $table->timestamps();

            $table->foreign('parser_id')->references('id')->on('parsers');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parser_items');
    }
}
