<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->unique('id');
        });
        Schema::table('items', function (Blueprint $table) {
            $table->integer('category_id')->nullable()->change();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
        });
        Schema::table('attrs', function (Blueprint $table) {
            $table->unique('id');
        });
        Schema::table('attr_values', function (Blueprint $table) {
            $table->unique('id');
            $table->foreign('attr_id')->references('id')->on('attrs')->onDelete('cascade');
        });
        Schema::table('item_attrs', function (Blueprint $table) {
            $table->foreign('attr_id')->references('id')->on('attrs')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
        });
        Schema::table('item_seasons', function (Blueprint $table) {
            $table->unique('id');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
        });
        Schema::table('item_series', function (Blueprint $table) {
            $table->unique('id');
            $table->foreign('item_season_id')->references('id')->on('item_seasons')->onDelete('cascade');
        });
        Schema::table('video_types', function (Blueprint $table) {
            $table->unique('id');
        });
        Schema::table('authors', function (Blueprint $table) {
            $table->unique('id');
        });
        Schema::table('sources', function (Blueprint $table) {
            $table->unique('id');
        });
        Schema::table('item_videos', function (Blueprint $table) {
            $table->unique('id');
            $table->integer('source_id')->nullable()->change();
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->foreign('item_season_id')->references('id')->on('item_seasons')->onDelete('cascade');
            $table->foreign('item_series_id')->references('id')->on('item_series')->onDelete('cascade');
            $table->foreign('video_type_id')->references('id')->on('video_types');
            $table->foreign('author_id')->references('id')->on('authors')->onDelete('set null');
            $table->foreign('source_id')->references('id')->on('sources')->onDelete('set null');
        });
        Schema::table('lists', function (Blueprint $table) {
            $table->unique('id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('user_items', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->foreign('list_id')->references('id')->on('lists')->onDelete('cascade');
        });
        Schema::table('user_ratings', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
        });
        Schema::table('user_watches', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->foreign('item_series_id')->references('id')->on('item_series')->onDelete('cascade');
            $table->foreign('item_video_id')->references('id')->on('item_videos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
