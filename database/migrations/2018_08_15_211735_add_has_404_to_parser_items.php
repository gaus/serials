<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHas404ToParserItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parser_items', function (Blueprint $table) {
            $table->boolean('has_404')->nullable();
            $table->index('has_404');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parser_items', function (Blueprint $table) {
            $table->dropColumn('has_404');
        });
    }
}
