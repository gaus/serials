<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->string('name_ru')->nullable()->default(null)->change();
            $table->string('name_en')->nullable()->default(null)->change();
            $table->string('name_original')->nullable()->default(null)->change();
            $table->text('description')->nullable()->default(null)->change();
            $table->string('image', 64)->nullable()->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->string('name_ru')->default('')->nullable(false)->change();
            $table->string('name_en')->default('')->nullable(false)->change();
            $table->string('name_original')->default('')->nullable(false)->change();
            $table->text('description')->default('')->nullable(false)->change();
            $table->string('image', 64)->default('')->nullable(false)->change();
        });
    }
}
