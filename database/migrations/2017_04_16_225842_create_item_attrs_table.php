<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemAttrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_attrs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('attr_id');
            $table->integer('attr_value_id');

            $table->index('item_id', 'idx_item_attrs_item_id');
            $table->index('attr_id', 'idx_item_attrs_attr_id');
            $table->index('attr_value_id', 'idx_item_attrs_attr_value_id');
            $table->unique(
                ['item_id', 'attr_id', 'attr_value_id'],
                'u_item_attrs_item_id_attr_id_attr_value_id'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_attrs');
    }
}
