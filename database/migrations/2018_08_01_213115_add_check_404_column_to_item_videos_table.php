<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheck404ColumnToItemVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_videos', function (Blueprint $table) {
            $table->smallInteger('has_404')->nullable();
            $table->dateTime('checked_404_at')->nullable();
            $table->index('has_404');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_videos', function (Blueprint $table) {
            $table->dropColumn('has_404');
            $table->dropColumn('checked_404_at');
        });
    }
}
