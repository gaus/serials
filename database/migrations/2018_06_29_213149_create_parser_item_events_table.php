<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParserItemEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parser_item_events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parser_item_id');
            $table->boolean('success');
            $table->jsonb('data')->nullable();
            $table->dateTime('created_at')
                ->default(\Illuminate\Support\Facades\DB::raw('NOW()'));

            $table->foreign('parser_item_id')->references('id')->on('parser_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parser_events');
    }
}
