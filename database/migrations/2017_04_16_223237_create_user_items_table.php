<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('item_id');
            $table->integer('list_id');
            $table->integer('season_number')->nullable();
            $table->integer('series_number')->nullable();
            $table->timestamps();

            $table->index('user_id', 'idx_user_items_user_id');
            $table->index('list_id', 'idx_user_items_list_id');
            $table->index('item__id', 'idx_user_items_item_id');
            $table->unique(
                ['user_id', 'list_id', 'item_id'],
                'u_user_items_user_id_list_id_item_id'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_items');
    }
}
