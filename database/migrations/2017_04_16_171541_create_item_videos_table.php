<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('item_season_id')->nullable();
            $table->integer('item_series_id')->nullable();
            $table->integer('video_type_id');
            $table->integer('author_id')->nullable();
            $table->integer('source_id');
            $table->integer('quality')->nullable();
            $table->string('src');
            $table->timestamps();

            $table->index('item_id', 'idx_item_videos_item_id');
            $table->index('item_season_id', 'idx_item_videos_item_season_id');
            $table->index('item_series_id', 'idx_item_videos_item_series_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_videos');
    }
}
