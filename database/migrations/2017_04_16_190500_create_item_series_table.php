<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_series', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_season_id');
            $table->integer('number');
            $table->string('name_ru', 250)->nullable();
            $table->string('name_original', 250)->nullable();
            $table->date('released_at')->nullable();
            $table->timestamps();

            $table->index('item_season_id', 'idx_item_series_item_season_id');
            $table->index('item_season_id', 'idx_item_series_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_series');
    }
}
