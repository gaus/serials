<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasVideoColumnToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->boolean('has_video')->default(false);
        });
        Schema::table('item_series', function (Blueprint $table) {
            $table->boolean('has_video')->default(false);
        });
        Schema::table('item_seasons', function (Blueprint $table) {
            $table->boolean('has_video')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('has_video');
        });
        Schema::table('item_series', function (Blueprint $table) {
            $table->dropColumn('has_video');
        });
        Schema::table('item_seasons', function (Blueprint $table) {
            $table->dropColumn('has_video');
        });
    }
}
