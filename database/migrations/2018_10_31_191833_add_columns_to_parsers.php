<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToParsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parsers', function (Blueprint $table) {
            $table->boolean('apply_data')->default(false);
            $table->boolean('apply_image')->default(false);
            $table->boolean('apply_attrs')->default(false);
            $table->boolean('apply_series')->default(false);
            $table->boolean('apply_videos')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parsers', function (Blueprint $table) {
            $table->dropColumn('apply_data');
            $table->dropColumn('apply_image');
            $table->dropColumn('apply_attrs');
            $table->dropColumn('apply_series');
            $table->dropColumn('apply_videos');
        });
    }
}
