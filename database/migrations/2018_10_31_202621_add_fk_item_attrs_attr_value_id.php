<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkItemAttrsAttrValueId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::delete('DELETE FROM item_attrs WHERE NOT EXISTS (SELECT 1 FROM attr_values WHERE id = item_attrs.attr_value_id)');
        Schema::table('item_attrs', function (Blueprint $table) {
            $table->foreign('attr_value_id', 'fk_item_attrs_attr_value_id')
                ->references('id')
                ->on('attr_values')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_attrs', function (Blueprint $table) {
            $table->dropForeign('fk_item_attrs_attr_value_id');
        });
    }
}
