<?php

return [
    'modules' => [
        [
            'prefix' => 'api/v2',
            'middleware' => 'api',
            'defaultActions' => [
                'index' => [
                    'resource' => \Admin\Api\Components\CollectionResource::class,
                ],
                'show' => [
                    'resource' => \Admin\Api\Components\ModelResource::class,
                ],
                'store' => [
                    'resource' => \Admin\Api\Components\ModelResource::class,
                ],
                'update' => [
                    'resource' => \Admin\Api\Components\ModelResource::class,
                ],
            ],
            'resources' => [
                'item' => [
                    'allowed' => ['index', 'show'],
                    'model' => \Api\v2\Models\Item::class,
                    'actions' => [
                        'index' => [
                            'defaultPerPage' => 32,
                        ],
                    ],
                ],
                'filter' => [
                    'allowed' => ['index'],
                    'actions' => [
                        'index' => [
                            'class' => \Api\v2\Actions\FilterIndex::class,
                            'resource' => \Admin\Api\Components\ModelResource::class,
                        ],
                    ],
                ],
                'item-video' => [
                    'allowed' => ['index', 'show'],
                    'model' => \Api\v2\Models\ItemVideo::class,
                ],
                'list' => [
                    'allowed' => ['index'],
                    'model' => \Api\v2\Models\Lists::class,
                ],
                'user-watch' => [
                    'allowed' => ['store'],
                    'model' => \Api\v2\Models\UserWatch::class,
                    'actions' => ['store' => ['request' => \Api\v2\Requests\UserWatchStore::class]]
                ],
                'user-item' => [
                    'allowed' => ['store', 'update'],
                    'model' => \Api\v2\Models\UserItem::class,
                    'actions' => [
                        'store' => [
                            'class' => \Api\v2\Actions\UserItemStore::class,
                            'request' => \Api\v2\Requests\UserItemStore::class,
                        ],
                        'update' => [
                            'request' => \Api\v2\Requests\UserItemUpdate::class,
                        ],
                    ]
                ],
                'user-rating' => [
                    'allowed' => ['store'],
                    'model' => \Api\v2\Models\UserRating::class,
                    'actions' => [
                        'store' => [
                            'class' => \Api\v2\Actions\UserRatingStore::class,
                            'request' => \Api\v2\Requests\UserRatingStore::class,
                        ],
                    ]
                ],
                'user' => [
                    'allowed' => ['store'],
                    'model' => \Api\v2\Models\User::class,
                    'actions' => [
                        'store' => [
                            'class' => \Api\v2\Actions\UserStore::class,
                            'request' => \Api\v2\Requests\UserStore::class,
                        ],
                    ],
                ],
                'auth' => [
                    'allowed' => ['store', 'destroy'],
                    'actions' => [
                        'store' => [
                            'class' => \Api\v2\Actions\AuthStore::class,
                            'request' => \Api\v2\Requests\AuthStore::class,
                        ],
                        'destroy' => [
                            'class' => \Api\v2\Actions\AuthDestroy::class,
                        ],
                    ],
                ],
                'social-auth' => [
                    'allowed' => ['store'],
                    'actions' => [
                        'store' => [
                            'class' => \Api\v2\Actions\SocialAuthStore::class,
                        ],
                    ],
                ],
            ],
        ],
        [
            'prefix' => 'admin/api',
            'middleware' => 'api',
            'defaultActions' => [
                'index' => [
                    'request' => \Admin\Api\Requests\IndexRequest::class,
                    'resource' => \Admin\Api\Components\CollectionResource::class,
                    'allowedPerPage' => ['100', '50', '20', '10', '0'],
                ],
                'show' => [
                    'request' => \Admin\Api\Requests\Request::class,
                    'resource' => \Admin\Api\Components\ModelResource::class,
                ],
                'store' => [
                    'request' => \Admin\Api\Requests\Request::class,
                    'resource' => \Admin\Api\Components\ModelResource::class,
                ],
                'update' => [
                    'request' => \Admin\Api\Requests\Request::class,
                    'resource' => \Admin\Api\Components\ModelResource::class,
                ],
            ],
            'resources' => [
                'user' => [
                    'model' => \Admin\Api\Models\User::class,
                    'actions' => [
                        'store' => [
                            'request' => \Admin\Api\Requests\StoreUser::class,
                        ],
                        'update' => [
                            'request' => \Admin\Api\Requests\UpdateUser::class,
                        ],
                    ],
                ],
                'item' => [
                    'model' => \Admin\Api\Models\Item::class,
                    'actions' => [
                        'store' => [
                            'request' => \Admin\Api\Requests\StoreItem::class,
                        ],
                        'update' => [
                            'request' => \Admin\Api\Requests\StoreItem::class,
                        ],
                    ],
                ],
                'attr' => [
                    'model' => \Admin\Api\Models\Attr::class,
                ],
                'attr-value' => [
                    'model' => \Admin\Api\Models\AttrValue::class,
                ],
                'item-attr' => [
                    'model' => \Admin\Api\Models\ItemAttr::class,
                ],
                'item-season' => [
                    'model' => \Admin\Api\Models\ItemSeason::class,
                ],
                'item-series' => [
                    'model' => \Admin\Api\Models\ItemSeries::class,
                ],
                'item-video' => [
                    'model' => \Admin\Api\Models\ItemVideo::class,
                ],
                'author' => [
                    'model' => \Admin\Api\Models\Author::class,
                ],
                'source' => [
                    'model' => \Admin\Api\Models\Source::class,
                ],
                'video-type' => [
                    'model' => \Admin\Api\Models\VideoType::class,
                ],
                'parser-item' => [
                    'model' => \Admin\Api\Models\ParserItem::class,
                    'actions' => [
                        'update' => [
                            'class' => \Admin\Api\Actions\ParserItemUpdate::class,
                        ],
                    ],
                ],
                'parser' => [
                    'model' => \Admin\Api\Models\Parser::class,
                ],
            ],
        ],
    ],
];
