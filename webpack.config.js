const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
// const CompressionPlugin = require('compression-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = [
    {
        mode: 'production',
        entry: {
            app: './resources/assets/js/app.js',
            admin: './admin/assets/js/admin.js'
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: [
                        'babel-loader'
                    ]
                }
            ]
        },
        resolve: {
            extensions: ['*', '.js', '.jsx']
        },
        output: {
            path: __dirname + '/public/js',
            publicPath: '/',
            filename: '[name].js'
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            // new CompressionPlugin()
        ],
        optimization: {
            minimizer: [new UglifyJsPlugin()]
        },
        devServer: {
            contentBase: './public',
            hot: true
        }
    },


    {
        mode: 'production',
        entry: {
            app: './resources/assets/sass/app.scss',
            'mobile-app': './resources/assets/sass/mobile/mobile-app.scss',
            // admin: './admin/assets/sass/admin.scss'
        },
        module: {
            rules: [
                {
                    test: /\.(css|scss)$/,
                    exclude: /node_modules/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            'css-loader',
                            {
                                loader: 'postcss-loader',
                                options: {
                                    ident: "postcss",
                                    plugins: [
                                        require('autoprefixer')({ browsers: ['defaults'] })
                                    ],
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    includePaths: [
                                        __dirname + '/node_modules',
                                    ],
                                },
                            },
                        ]
                    })
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin({
                filename:  (getPath) => {
                    return getPath('[name].css');
                },
                allChunks: true
            }),
            // new CompressionPlugin()
        ],
        resolve: {
            extensions: ['*', '.css', '.scss']
        },
        output: {
            path: __dirname + '/public/css',
            publicPath: '/css/',
            filename: '[name].css'
        },
        optimization: {
            minimizer: [new OptimizeCSSAssetsPlugin({})]
        },
        devServer: {
            contentBase: './public',
            hot: true
        }
    }
];
