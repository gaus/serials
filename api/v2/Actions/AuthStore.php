<?php

namespace Api\v2\Actions;

use Gaus57\LaravelApi\Actions\StoreAction;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthStore extends StoreAction
{
    public function run(): Response
    {
        if (!$this->validateRequest($errors)) {
            return $this->resourceResponse($this->resourceErrors, $errors, 422);
        }
        if (Auth::attempt($this->request->only('email', 'password'), true)) {
            return $this->resourceResponse($this->resource, Auth::user(), 201);
        }

        return new JsonResponse(['errors' => ['email' => ['Неверный email или пароль']]], 422);
    }
}
