<?php

namespace Api\v2\Actions;

use App\Models\Attr;
use Gaus57\LaravelApi\Actions\AbstractAction;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;

class FilterIndex extends AbstractAction
{
    /**
     * @inheritdoc
     */
    public function run(): Response
    {
        if (!$this->validateRequest($errors)) {
            return $this->resourceResponse($this->resourceErrors, $errors, 422);
        }

        $filters = [];
        // TODO Нужно сделать вывод только тех значений которые есть в категории
        $filters[] = [
            'name' => 'Год выпуска',
            'value' => 'year',
            'values' => [
                ['name' => '2018', 'value' => '2018'],
                ['name' => '2017', 'value' => '2017'],
                ['name' => '2016', 'value' => '2016'],
                ['name' => '2010-2015', 'value' => '2010-2015'],
                ['name' => '2000-2010', 'value' => '2000-2010'],
                ['name' => 'раньше 2000', 'value' => '-2000'],
            ],
        ];

        $attrs = Attr::orderBy('sort')
            ->with('values')
            ->whereIn('id', [1, 2])
            ->get();
        foreach ($attrs as $attr) {
            $filter = [
                'name' => ucfirst($attr->name),
                'value' => $attr->slug,
                'values' => [],
            ];
            $values = $attr->values()
                ->select('attr_values.sort', 'attr_values.id', 'attr_values.value')
                ->distinct()
                ->join('item_attrs', 'attr_values.id', '=', 'item_attrs.attr_value_id')
                ->join('items', 'items.id', '=', 'item_attrs.item_id')
                ->where('items.published', true)
                ->where('items.has_video', true)
                ->orderBy('attr_values.value')
                ->get();
            foreach ($values as $attrValue) {
                $filter['values'][] = [
                    'name' => $attrValue->value,
                    'value' => $attrValue->id,
                ];
            }
            $filters[] = $filter;
        }

        return $this->resourceResponse($this->resource, new Collection($filters));
    }
}
