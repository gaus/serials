<?php

namespace Api\v2\Actions;

use App\Models\User;
use App\Models\UserSocial;
use Gaus57\LaravelApi\Actions\StoreAction;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class SocialAuthStore extends StoreAction
{
    public function run(): Response
    {
//        if (!$this->validateRequest($errors)) {
//            return $this->resourceResponse($this->resourceErrors, $errors, 422);
//        }
        $data = $this->request->get('data');
        if ($this->checkVkSession($data)) {
            /** @var UserSocial $userSocial */
            $userSocial = UserSocial::query()->firstOrNew([
                'type' => 'vk',
                'social_id' => $data['mid'],
            ]);
            if (!$userSocial->user_id) {
                $user = new User();
                $user->fill([
                    'name' => '',
                    'email' => '',
                    'password' => '',
                ]);
                $user->save();
                $userSocial->user_id = $user->id;
                $userSocial->save();
                $userSocial->setRelation('user', $user);
            }
            Auth::login($userSocial->user, true);

            return $this->resourceResponse($this->resource, $userSocial->user, 201);
        }

        return new JsonResponse(['errors' => ['email' => ['Неверный email или пароль']]], 422);
    }

    protected function checkVkSession(array $data): bool
    {
        $result = false;
        $session = array_only($data, ['expire', 'mid', 'secret', 'sid', 'sig']);
        ksort($session);
        $sign = '';
        foreach ($session as $key => $value) {
            if ($key != 'sig') {
                $sign .= ($key.'='.$value);
            }
        }
        $sign .= env('VK_SECRET');
        $sign = md5($sign);
        if ($session['sig'] === $sign && $session['expire'] > time()) {
            $result = true;
        }

        return $result;
    }
}