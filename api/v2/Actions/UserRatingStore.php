<?php

namespace Api\v2\Actions;

use Gaus57\LaravelApi\Actions\StoreAction;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Response;

class UserRatingStore extends StoreAction
{
    public function run(): Response
    {
        if (!$this->validateRequest($errors)) {
            return $this->resourceResponse($this->resourceErrors, $errors, 422);
        }
        /** @var Model $model */
        $model = new $this->model();
        $model = $model->newQuery()
                ->where('user_id', $this->request->input('user_id'))
                ->where('item_id', $this->request->input('item_id'))
                ->first()
            ?? $model;
        $model->fill($this->request->all())->save();

        return $this->resourceResponse($this->resource, $model, 201);
    }
}
