<?php

namespace Api\v2\Actions;

use Api\v2\Models\User;
use Gaus57\LaravelApi\Actions\StoreAction;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class UserStore extends StoreAction
{
    public function run(): Response
    {
        if (!$this->validateRequest($errors)) {
            return $this->resourceResponse($this->resourceErrors, $errors, 422);
        }
        /** @var User $model */
        $model = new $this->model();
        $model->fill($this->request->all())->fill(['name' => ''])->save();
        Auth::login($model, true);

        return $this->resourceResponse($this->resource, $model, 201);
    }
}
