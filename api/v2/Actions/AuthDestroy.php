<?php

namespace Api\v2\Actions;

use Gaus57\LaravelApi\Actions\DestroyAction;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthDestroy extends DestroyAction
{
    public function run(): Response
    {
        Auth::logout();

        return new Response(null, 204);
    }
}
