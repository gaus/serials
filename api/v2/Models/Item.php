<?php

namespace Api\v2\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourcesInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Item extends \App\Models\Item implements FieldsResourceInterface, FindResourcesInterface
{
    public function scopeFindResources(Builder $query, Request $request): Builder
    {
        foreach ($request->all() as $key => $value) {
            switch ($key) {
                case 'year':
                    $years = [];
                    foreach (explode(',', $value) as $v) {
                        if (strpos($v, '-') !== false) {
                            $vArr = explode('-', $v);
                            $years = array_merge($years, range($vArr[0], $vArr[1]));
                        } else {
                            $years[] = $v;
                        }
                    }
                    $query->whereIn('year_start', $years);
                    break;
                case 'country':
                case 'genre':
                $query->where(function ($query) use ($value, $key) {
                        foreach (explode(',', $value) as $v) {
                            $v = (int)$v;
                            $query->orWhereRaw('attrs::JSONB @> \'{"' . $key . '":{"values":{"' . $v . '":{"id":' . $v . '}}}}\'');
                        }
                    });
                    break;
                case 'q':
                    foreach (preg_split('/(,|\s)/', $value) as $v) {
                        $query->where(function ($query) use ($v) {
                            $query->orWhere(DB::raw('UPPER(name_ru)'), 'like', '%'.mb_strtoupper($v).'%')
                                ->orWhere(DB::raw('UPPER(name_en)'), 'like', '%'.mb_strtoupper($v).'%')
                                ->orWhere(DB::raw('UPPER(name_original)'), 'like', '%'.mb_strtoupper($v).'%');

                        });
                    }
                    break;
                case 'scenario':
                    if ($value === 'show') {
                        $query->with([
                            'attrValues',
                            'seasons',
                            'seasons.series',
                            'userRating',
                        ]);
                    }
                    if ($value === 'user-list') {
                        $query->with([
                            'seasons',
                            'seasons.series',
                        ]);
                    }
                    break;
            }
        }
        switch ($request->get('sort', 'rating')) {
            case 'rating':
                $query->orderByRaw('COALESCE(rating, rating_kp, rating_imdb) DESC NULLS LAST');
                break;
            case 'started_at':
                $query->orderByRaw('year_start DESC NULLS LAST')
                    ->orderBy('started_at', 'desc');
                break;
        }
        $query->with(['userItems'])
            ->where('published', true)
            ->where('has_video', true)
            ->orderBy('rating_kp', 'desc')
            ->orderBy('id'/*, 'desc'*/);

        return $query;
    }

    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'name' => ($this->name_ru ?? $this->name_en) ?? $this->name_original,
            'name_ru' => $this->name_ru,
            'name_en' => $this->name_en,
            'name_original' => $this->name_original,
            'description' => $this->description,
            'image_src' => $this->image_src,
            'season_count' => $this->season_count,
            'series_count' => $this->series_count,
            'duration' => $this->duration,
            'rating' => $this->rating,
            'rating_kp' => $this->rating_kp,
            'rating_imdb' => $this->rating_imdb,
            'started_at' => $this->started_at,
            'completed_at' => $this->completed_at,
            'year_start' => $this->year_start,
            'year_complete' => $this->year_complete,
            'has_video' => $this->has_video,
            'attrs' => $this->attrs,
            'video_types' => $this->video_types,
            'user_item' => Auth::check()
                ? (count($this->userItems) > 0 ? $this->userItems->first()->toArray() : null)
                : null,
            $resource->mergeWhenScenario('user-list', function () {
                return [
                    'seasons' => $this->getSeasons(),
                    'pivot' => $this->pivot,
                ];
            }),
            $resource->mergeWhenScenario('show', function () {
                // Все характеристики сериала
                $attrs = [];
                foreach ($this->attrValues as $value) {
                    if (!isset($attrs[$value->attr_id])) {
                        $attrs[$value->attr_id] = $value->attr->toArray();
                        $attrs[$value->attr_id]['values'] = [];
                    }
                    $attrs[$value->attr_id]['values'][] = array_only(
                        $value->toArray(),
                        ['id', 'value', 'sort']
                    );
                }
                usort($attrs, function ($a, $b) {
                    return $a['sort'] <=> $b['sort'];
                });

                return [
                    'attrs_all' => $attrs,
                    'seasons' => $this->getSeasons(),
                    'user_rating' => $this->userRating
                        ? $this->userRating->toArray()
                        : null,
                ];
            }),
        ];
    }

    protected function getSeasons(): array
    {
        $seasons = [];
        foreach ($this->seasons as $season) {
            $seasonArr = array_only(
                $season->toArray(),
                ['id', 'item_id', 'number', 'started_at', 'has_video', 'completed_at']
            );
            $seasonArr['series'] = [];
            foreach ($season->series as $seria) {
                $seasonArr['series'][$seria->id] = array_only(
                    $seria->toArray(),
                    ['id', 'item_season_id', 'number', 'has_video', 'released_at']
                );
            }
            $seasons[$season->id] = $seasonArr;
        }

        return $seasons;
    }
}
