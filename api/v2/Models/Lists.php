<?php

namespace Api\v2\Models;

use Admin\Api\Components\CollectionResource;
use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourcesInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class Lists extends \App\Models\Lists implements FieldsResourceInterface, FindResourcesInterface
{
    protected $itemClass = Item::class;

    public function scopeFindResources(Builder $query, Request $request): Builder
    {
        if (Auth::check() && $request->get('scenario') === 'user-list') {
            $query->with([
                'userItems',
                'userItems.userItems',
                'userItems.seasons',
                'userItems.seasons.series',
            ]);
        }

        return $query;
    }

    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'sort' => $this->sort,
            $resource->mergeWhenScenario(
                'user-list',
                function () {
                    return [
                        'items' => Auth::check()
                            ? new CollectionResource($this->userItems)
                            : null
                    ];
                }
            ),
        ];
    }
}
