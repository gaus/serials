<?php

namespace Api\v2\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserRating extends \App\Models\UserRating implements FieldsResourceInterface
{
    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'item_id' => $this->item_id,
            'user_id' => $this->user_id,
            'value' => $this->value,
        ];
    }
}
