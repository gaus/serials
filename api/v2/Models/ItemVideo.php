<?php

namespace Api\v2\Models;

use App\Models\UserWatch;
use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourcesInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemVideo extends \App\Models\ItemVideo implements
    FieldsResourceInterface,
    FindResourceInterface,
    FindResourcesInterface
{
    public function findResource(Request $request, string $id): ?Model
    {
        if ($id === 'next') {
            $nextVideo = null;
            /** @var ItemVideo $video */
            if ($videoId = $request->get('video_id')) {
                $video = self::find($videoId);
            } elseif ($itemId = $request->get('item_id')) {
                $watch = UserWatch::where('item_id', $itemId)
                    ->orderBy('created_at', 'desc')
                    ->first();
                $video = $watch ? $watch->video : null;
            }
            if ($video) {
                $nextVideo = $video->nextSeriesVideo();
            } elseif ($itemId) {
                $nextVideo = self::firstItemVideo($itemId);
            }

            return $nextVideo;
        }

        /** @var Builder $query */
        $query = self::query()
            ->whereNull('has_404')
            ->whereId($id);
        if ($itemId = $request->get('item_id')) {
            $query->where('item_id', $itemId);
        }

        return $query->first();
    }

    public function scopeFindResources(Builder $query, Request $request): Builder
    {
        /** @var Builder $query */
        $query = self::query()
            ->whereNull('has_404')
            ->with(['author', 'source', 'videoType']);
        if ($itemId = $request->get('item_id')) {
            $query->where('item_id', $itemId);
        }
        if ($itemSeasonId = $request->get('item_season_id')) {
            $query->where('item_season_id', $itemSeasonId);
        }
        if ($itemSeriesId = $request->get('item_series_id')) {
            $query->where('item_series_id', $itemSeriesId);
        }

        return $query;
    }

    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'item_id' => $this->item_id,
            'item_season_id' => $this->item_season_id,
            'item_series_id' => $this->item_series_id,
            'video_type_id' => $this->video_type_id,
            'author_id' => $this->author_id,
            'source_id' => $this->source_id,
            'quality' => $this->quality,
            'src' => $this->src,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'author' => $this->author,
            'source' => $this->source,
            'video_type' => $this->videoType,
        ];
    }
}
