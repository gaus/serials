<?php

namespace Api\v2\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthStore extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'string',
            ],
            'password' => [
                'required',
                'string',
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'email.required' => 'Не заполнен email адрес',
            'password.required' => 'Не заполнен пароль',
        ];
    }
}
