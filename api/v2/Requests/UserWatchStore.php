<?php

namespace Api\v2\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class UserWatchStore extends FormRequest
{
    public function authorize(): bool
    {
        if (!Auth::check()) {
            throw new UnauthorizedHttpException();
        }

        return Auth::check();
    }

    public function rules(): array
    {
        return [
            'user_id' => [
                'required',
                'integer',
                'in:' . Auth::user()->id,
            ],
            'item_video_id' => [
                'required',
                'integer',
                'exists:item_videos,id',
            ],
            'item_id' => [
                'required',
                'integer',
                'exists:items,id',
            ],
            'item_series_id' => [
                'integer',
                'exists:item_series,id',
            ],
        ];
    }
}
