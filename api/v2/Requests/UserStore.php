<?php

namespace Api\v2\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStore extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'string',
                'email',
                'unique:users,email',
            ],
            'password' => [
                'required',
                'string',
                'min:6',
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'required' => 'Поле обязательно для заполнения.',
            'email' => 'Не похоже на email адрес.',
            'email.unique' => 'Пользователь с таким email уже существует.',
            'min' => 'Минимум :min символов.'
        ];
    }
}
