<?php

namespace Api\v2\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class UserItemUpdate extends FormRequest
{
    public function authorize(): bool
    {
        if (!Auth::check()) {
            throw new UnauthorizedHttpException();
        }

        return Auth::check();
    }

    public function rules(): array
    {
        return [
            'user_id' => [
                'integer',
                'in:' . Auth::user()->id,
            ],
            'list_id' => [
                'integer',
                'exists:lists,id',
            ],
            'item_id' => [
                'integer',
                'exists:items,id',
            ],
            'season_number' => [
                'integer',
            ],
            'series_number' => [
                'integer',
            ],
        ];
    }
}
