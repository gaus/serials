<?php

namespace Api\v2\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserRatingStore extends FormRequest
{
    public function authorize(): bool
    {
        return Auth::check();
    }

    public function rules(): array
    {
        return [
            'user_id' => [
                'required',
                'integer',
                'in:' . Auth::user()->id,
            ],
            'item_id' => [
                'required',
                'integer',
                'exists:items,id',
            ],
            'value' => [
                'required',
                'integer',
                'max:10',
                'min:1',
            ],
        ];
    }
}
