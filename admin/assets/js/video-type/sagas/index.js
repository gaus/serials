import { takeLatest } from 'redux-saga/effects'
import Api from '../../common/services/api'
import { Types as VideoTypeTypes } from '../reducers/video-type'
import * as videoType from './video-type'

const api = Api.create()

export default function * sagas () {
    yield [
        takeLatest(VideoTypeTypes.REQUEST_LIST, videoType.getList, api),
        takeLatest(VideoTypeTypes.APPLY_LIST_FILTER, videoType.applyFilter),
    ]
}
