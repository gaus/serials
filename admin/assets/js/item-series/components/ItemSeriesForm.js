import React from 'react'
import FormField from '../../common/components/form/FormField'
import Button from '../../common/components/form/Button'

export default ({
    data = {},
    seasons = [],
    errors = {},
    setData,
    saveData,
}) => {
    return <div>
        <FormField
            type='select'
            name='item_season_id'
            value={data.item_season_id}
            label='Сезон'
            error={errors.item_season_id && errors.item_season_id.join(' ')}
            setData={setData}
            list={seasons}
            listValue='id'
            listName='number'
            withEmpty
        />
        <FormField
            type='number'
            name='number'
            value={data.number}
            label='Номер'
            error={errors.number && errors.number.join(' ')}
            setData={setData}
        />
        <FormField
            type='date'
            name='released_at'
            value={data.released_at}
            label='Дата выхода'
            error={errors.released_at && errors.released_at.join(' ')}
            setData={setData}
        />
        <FormField
            type='text'
            name='name_ru'
            value={data.name_ru}
            label='Русское название'
            error={errors.name_ru && errors.name_ru.join(' ')}
            setData={setData}
        />
        <FormField
            type='text'
            name='name_original'
            value={data.name_original}
            label='Оригинальное название'
            error={errors.name_original && errors.name_original.join(' ')}
            setData={setData}
        />
        <Button
            type='submit'
            className='btn-primary'
            text='Сохранить'
            onClick={() => {
                saveData(data)
            }}
        />
    </div>
}
