import { takeLatest } from 'redux-saga/effects'
import Api from '../../common/services/api'
import { Types as ItemSeriesTypes } from '../reducers/item-series'
import * as itemSeries from './item-series'

const api = Api.create()

export default function * sagas () {
    yield [
        takeLatest(ItemSeriesTypes.REQUEST_LIST, itemSeries.getList, api),
        takeLatest(ItemSeriesTypes.REQUEST_DATA, itemSeries.getData, api),
        takeLatest(ItemSeriesTypes.SAVE_DATA, itemSeries.saveData, api),
        takeLatest(ItemSeriesTypes.DELETE_DATA, itemSeries.deleteData, api),
    ]
}
