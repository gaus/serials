import { call, put } from 'redux-saga/effects'
import { Creators as Action } from '../reducers/item-series'
import { Creators as ActionPreloader } from '../../common/reducers/preloader'
import { Creators as ActionInformer } from '../../common/reducers/informer'

export function * getList (api, {params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getItemSeries, params)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successList(response.data))
    } else {
        yield put(Action.failList())
    }
}

export function * getData (api, {id}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getItemSerie, id, {scenario: 'edit'})
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successData(response.data.data))
    } else {
        yield put(Action.failData())
    }
}

export function * saveData (api, {data, params = {}}) {
    yield put(ActionPreloader.start())
    const response = data.id
        ? yield call(api.putItemSerie, data.id, data, {scenario: 'edit'})
        : yield call(api.postItemSerie, data)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successSaveData(response.data.data))
        yield put(Action.requestList(params))
        data.id
            ? yield put(ActionInformer.push('success', 'Изменения сохранены'))
            : yield put(ActionInformer.push('success', 'Серия серала добавлена'))
        if (!data.id) {
            yield put(Action.toggleCreateModal(false))
        }
    } else {
        yield put(Action.failSaveData(response.data.errors))
        yield put(ActionInformer.push('danger', 'Вы ввели некорректные данные'))
    }
}

export function * deleteData (api, {id, params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.deleteItemSerie, id)
    yield put(ActionPreloader.stop())
    if (response.status === 204) {
        yield put(Action.successDeleteData())
        yield put(Action.requestList(params))
        yield put(ActionInformer.push('success', 'Серия сериала удалена'))
    } else {
        yield put(Action.failDeleteData())
    }
}
