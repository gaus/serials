import { takeLatest } from 'redux-saga/effects'
import Api from '../../common/services/api'
import { Types as AuthorTypes } from '../reducers/author'
import * as author from './author'

const api = Api.create()

export default function * sagas () {
    yield [
        takeLatest(AuthorTypes.REQUEST_LIST, author.getList, api),
        takeLatest(AuthorTypes.APPLY_LIST_FILTER, author.applyFilter),
    ]
}
