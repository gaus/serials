import React, { Component } from 'react'
import { isUndefined } from 'lodash'
import DataTable from '../../common/components/table/DataTable'
import Modal from '../../common/components/content/Modal'

export default class DataVideos extends Component {
    constructor(props) {
        super(props)
        this.state = {
            popup: undefined,
        }
    }

    openPopup = (video) => {
        this.setState({popup: video})
    }

    closePopup = () => {
        this.setState({popup: undefined})
    }

    render() {
        const {videos = []} = this.props
        if (!videos.length) {
            return 'Нет данных'
        }

        return <div>
            <DataTable
                className='table-bordered'
                items={videos}
                map={[
                    {
                        title: 'Номер сезона',
                        field: 'season_number',
                    },
                    {
                        title: 'Номер серии',
                        field: 'series_number',
                    },
                    {
                        title: 'Автор релиза',
                        field: 'author',
                    },
                    {
                        title: 'Тип перевода',
                        field: 'video_type_id',
                    },
                    {
                        title: 'Ссылка',
                        field: 'src',
                    },
                ]}
                actions={[
                    {
                        className: 'fa fa-eye text-aqua',
                        onClick: (e, video) => {
                            this.openPopup(video)
                        },
                    },
                ]}
            />

            <Modal
                title='Смотреть видео'
                isOpen={this.state.popup}
                close={() => this.closePopup()}
            >
                {this.state.popup && this.state.popup.src &&
                    <div>
                        <iframe
                            src={this.state.popup.src}
                            frameBorder='0'
                            scrolling='no'
                            allowFullScreen
                            style={{width: '100%', height: '320px'}}
                        />
                        <h3>
                            {!isUndefined(this.state.popup.season_number) && `Сезон ${this.state.popup.season_number}`}
                            {!isUndefined(this.state.popup.series_number) && ` Серия ${this.state.popup.series_number}`}
                            {!isUndefined(this.state.popup.author) && ` [${this.state.popup.author}]`}
                        </h3>
                    </div>
                }
            </Modal>
        </div>
    }
}
