import React from 'react'
import { Link } from 'react-router'
import SimpleData from '../../common/components/data/SimpleData'

export default ({data = null}) => {
    return data && <SimpleData
        inline
        data={data}
        map={[
            {
                name: 'ID',
                value: 'id',
            },
            {
                name: 'Парсер',
                value: 'parser.name',
            },
            {
                name: 'Сериал',
                value: parserItem => {
                    return parserItem.item && <Link to={`/admin/items/${parserItem.item_id}`}>
                        {parserItem.item.name_ru || parserItem.item.name_en || parserItem.item.name_original}
                    </Link>
                },
            },
            {
                name: 'Url',
                value: parserItem => {
                    return <a
                        target='_blank'
                        href={parserItem.url}
                    >{parserItem.url}</a>
                },
            },
            {
                name: 'Активен',
                value: parserItem => parserItem.last_activity
                    ? <span className='text-green'>да</span>
                    : <span className='text-gray'>нет</span>,
            },
            {
                name: 'Последняя активность',
                value: 'last_activity',
            },
            {
                name: 'Создан',
                value: 'created_at',
            },
        ]}
    />
}
