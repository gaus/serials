import React from 'react'
import { isArray } from 'lodash'
import SimpleData from '../../common/components/data/SimpleData'
import Row from '../../common/components/content/Row'
import Col from '../../common/components/content/Col'

export default ({
    data = {},
    attrs = {},
}) => {
    const attrsData = {...attrs}
    const attrsMap = []
    for (let key in attrsData) {
        if (isArray(attrsData[key])) {
            attrsData[key] = attrsData[key].join(', ')
        }
        attrsMap.push({
            name: key,
            value: key,
        })
    }

    return <div>
        <Row>
            <Col md='8'>
                <SimpleData
                    data={data}
                    map={{
                        'name_ru': 'Русское название',
                        'name_en': 'Английское название',
                        'name_original': 'Оригинальное название',
                        'description': 'Описание',
                        'started_at': 'Дата начала',
                        'completed_at': 'Дата окончания',
                        'rating_kp': 'Рейтинг на Кинопоиске',
                        'rating_imdb': 'Рейтинг на IMDB',
                        'kinopoisk_id': 'ID на Кинопоиск',
                        'duration': 'Продолжительность',
                        'year_start': 'Год начала',
                        'year_complete': 'Год завершения',
                    }}
                    hideEmpty
                />
                <SimpleData
                    data={attrsData}
                    map={attrsMap}
                />
            </Col>
            <Col md='4'>
                {data.image && <SimpleData
                    data={data}
                    map={[
                        {
                            name: 'Изображение',
                            value: (data) => data.image && <img src={data.image} style={{maxWidth: '100%'}} />
                        }
                    ]}
                />}
            </Col>
        </Row>
    </div>
}
