import React from 'react'
import DataTable from '../../common/components/table/DataTable'

export default ({
    series = [],
}) => {
    if (!series.length) {
        return 'Нет данных'
    }

    return <div>
        {series.map((season, i) => {
            return <div>
                <h2>Сезон {season.number}</h2>
                <DataTable
                    className='table-bordered'
                    items={season.series}
                    map={[
                        {
                            title: 'Номер серии',
                            field: 'number',
                        },
                        {
                            title: 'Дата выхода',
                            field: 'released_at',
                        },
                        {
                            title: 'Русское название',
                            field: 'name_ru',
                        },
                        {
                            title: 'Оригинальное название',
                            field: 'name_original',
                        },
                    ]}
                />
            </div>
        })}
    </div>
}
