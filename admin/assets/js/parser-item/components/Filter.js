import React, { Component } from 'react'
import Box from '../../common/components/content/Box'
import Row from '../../common/components/content/Row'
import Col from '../../common/components/content/Col'
import Button from '../../common/components/form/Button'
import Select from '../../common/components/form/fields/Select'

export default class Filter extends Component {
    constructor (props) {
        super(props);
        this.state = {
            query: {...props.query},
        }
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.query !== nextProps.query) {
            this.setState({query: {...nextProps.query}})
        }
    }

    setData = (data) => {
        this.setState({query: {...this.state.query, ...data}})
    };

    render () {
        const { apply, parsers } = this.props;
        const { query } = this.state;

        return <Box>
            <Row>
                <Col md={4}>
                    <input
                        className='form-control'
                        type='text'
                        name='q'
                        placeholder={'Поиск'}
                        value={query && query.q}
                        onInput={({target: {value}}) => {
                            this.setData({q: value})
                        }}
                    />
                </Col>
                <Col md={2}>
                    <Select
                        className='form-control'
                        name='parser_id'
                        value={query && query.parser_id}
                        list={parsers}
                        listValue='id'
                        listName='name'
                        withEmpty
                        onChange={({target: {value}}) => {
                            this.setData({parser_id: value})
                        }}
                    />
                </Col>
                <Col md={4}>
                    <Button
                        text='Применить'
                        onClick={() => {
                            apply(this.state.query)
                        }}
                    />
                </Col>
            </Row>

        </Box>
    }
}
