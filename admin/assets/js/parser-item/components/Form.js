import React from 'react'
import { get } from 'lodash'
import FormField from '../../common/components/form/FormField'
import Button from '../../common/components/form/Button'
import Box from '../../common/components/content/Box'
import Row from '../../common/components/content/Row'
import Col from '../../common/components/content/Col'
import SelectItem from '../../item/components/SelectItem'

export default ({
    data = {},
    errors = {},
    setData,
    saveData,
    parsers,
    items,
    totalItems,
    searchItems,
    withoutItemSelect,
}) => {
    return <Box type='solid'>
        <FormField
            type='select'
            name='parser_id'
            value={data.parser_id}
            label='Парсер'
            error={errors.parser_id && errors.parser_id.join(' ')}
            setData={setData}
            list={parsers}
            listValue='id'
            listName='name'
            withEmpty
        />
        {!withoutItemSelect && <FormField
            type='select-search'
            name='item_id'
            value={data.item_id}
            valueName={data.item_id > 0 && get(data, 'item.name_ru', get(data, 'item.name_en'))}
            label='Сериал'
            error={errors.item_id && errors.item_id.join(' ')}
            setData={setData}
            items={items}
            total={totalItems}
            valueField='id'
            nameField={(item) => item.name_ru || item.name_en || item.name_original}
            itemComponent={(item) => <SelectItem item={item} />}
            search={searchItems}
        />}
        {data.item_id &&
            <Row>
                <Col md={2}>
                    <label className='control-label checkbox'>Обновить:</label>
                </Col>
                <Col md={3}>
                    <FormField
                        type='checkbox'
                        name='apply_data'
                        value={data.apply_data}
                        label='описание'
                        error={errors.apply_data && errors.apply_data.join(' ')}
                        setData={setData}
                    />
                </Col>
                <Col md={3}>
                    <FormField
                        type='checkbox'
                        name='apply_attrs'
                        value={data.apply_attrs}
                        label='атрибуты'
                        error={errors.apply_attrs && errors.apply_attrs.join(' ')}
                        setData={setData}
                    />
                </Col>
                <Col md={2}>
                    <FormField
                        type='checkbox'
                        name='apply_series'
                        value={data.apply_series}
                        label='серии'
                        error={errors.apply_series && errors.apply_series.join(' ')}
                        setData={setData}
                    />
                    </Col>
                <Col md={2}>
                    <FormField
                        type='checkbox'
                        name='apply_videos'
                        value={data.apply_videos}
                        label='видео'
                        error={errors.apply_videos && errors.apply_videos.join(' ')}
                        setData={setData}
                    />
                </Col>
            </Row>
        }
        <FormField
            type='text'
            name='url'
            value={data.url}
            label='Url'
            error={errors.url && errors.url.join(' ')}
            setData={setData}
        />
        <FormField
            type='checkbox'
            name='is_active'
            value={data.is_active}
            label='Парсинг активен'
            error={errors.is_active && errors.is_active.join(' ')}
            setData={setData}
        />
        <Button
            className='btn-primary'
            text='Сохранить'
            onClick={(e) => {
                e.preventDefault();
                saveData(data)
            }}
        />
    </Box>
}
