import { call, put } from 'redux-saga/effects'
import { Creators as Action } from '../reducers/parser-item'
import { Creators as ActionPreloader } from '../../common/reducers/preloader'
import { Creators as ActionInformer } from '../../common/reducers/informer'

export function * getList (api, {params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getParserItems, params)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successList(response.data))
    } else {
        yield put(Action.failList())
    }
}

export function * getData (api, {id}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getParserItem, id, {scenario: 'show'})
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successData(response.data))
    } else {
        yield put(Action.failData())
    }
}

export function * saveData (api, {data, params}) {
    yield put(ActionPreloader.start())
    const response = data.id
        ? yield call(api.putParserItem, data.id, data, {scenario: 'show'})
        : yield call(api.postParserItem, data, {scenario: 'show'})
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successSaveData(response.data))
        yield put(Action.requestList({...params}))
        data.id
            ? yield put(ActionInformer.push('success', 'Изменения сохранены'))
            : yield put(ActionInformer.push('success', 'Страница парсинга добавлена'))
    } else {
        yield put(Action.failSaveData(response.data.errors))
        yield put(ActionInformer.push('danger', 'Вы ввели некорректные данные'))
    }
}

export function * deleteData (api, {id, params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.deleteParserItem, id)
    yield put(ActionPreloader.stop())
    if (response.status === 204) {
        yield put(Action.successDeleteData())
        yield put(Action.requestList(params))
        yield put(ActionInformer.push('success', 'Страница парсинга удалена'))
    } else {
        yield put(Action.failDeleteData())
    }
}
