import { takeLatest } from 'redux-saga/effects'
import Api from '../../common/services/api'
import { Types as ParserItemTypes } from '../reducers/parser-item'
import * as parserItem from './parser-item'

const api = Api.create()

export default function * sagas () {
    yield [
        takeLatest(ParserItemTypes.REQUEST_LIST, parserItem.getList, api),
        takeLatest(ParserItemTypes.REQUEST_DATA, parserItem.getData, api),
        takeLatest(ParserItemTypes.SAVE_DATA, parserItem.saveData, api),
        takeLatest(ParserItemTypes.DELETE_DATA, parserItem.deleteData, api),
    ]
}
