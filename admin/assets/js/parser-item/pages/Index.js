import React, { Component } from 'react'
import { connect } from 'react-redux'
import { get as lodashGet } from 'lodash'
import { push } from 'react-router-redux'
import { Link } from 'react-router'
import { Creators as ParserItemAction } from '../reducers/parser-item'
import { Creators as ParserAction } from '../../parser/reducers/parser'
import { Creators as ItemAction } from '../../item/reducers/item'
import { mergeQuery } from "../../common/helpers/url-helper"
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Box from '../../common/components/content/Box'
import DataTable from '../../common/components/table/DataTable'
import Pagination from '../../common/components/pagination/Pagination'
import Form from '../components/Form'
import Filter from '../components/Filter'
import Modal from '../../common/components/content/Modal'

export class Index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            editPopup: false,
        }
    }

    componentWillMount () {
        this.fetchParserItems()
        this.props.fetchParsers()
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.routing !== nextProps.routing) {
            this.fetchParserItems(lodashGet(nextProps, 'routing.locationBeforeTransitions.query'))
        }
    }

    fetchParserItems = (query) => {
        this.props.fetchList(query || lodashGet(this.props, 'routing.locationBeforeTransitions.query', {}))
    }

    breadcrumb = () => {
        return [
            {
                title: 'Страницы парсинга',
                icon: 'fa-reorder',
            }
        ]
    }

    openEditPopup = (id) => {
        this.props.resetData()
        this.props.resetItemsList()
        id && this.props.fetchData(id)
        this.props.fetchParsers()
        this.setState({editPopup: true})
    }

    closeEditPopup = () => {
        this.setState({editPopup: false})
    }

    render () {
        const items = lodashGet(this.props, 'item.list.data', [])
        const totalItems = lodashGet(this.props, 'item.list.meta.total')
        const parsers = lodashGet(this.props, 'parser.list.data', [])
        const parserItems = lodashGet(this.props, 'parserItem.list.data', [])
        const pagination = lodashGet(this.props, 'parserItem.list.meta', {})
        const query = lodashGet(this.props, 'routing.locationBeforeTransitions.query', {})
        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                Страницы парсинга <small>
                    <a
                        href='#'
                        onClick={(e) => {
                            e.preventDefault()
                            this.openEditPopup()
                        }}
                    >Добавить</a>
                </small>
            </ContentHeader>
            <Content>
                <Filter
                    query={query}
                    parsers={parsers}
                    apply={(data) => {
                        this.props.locationPush(mergeQuery(data))
                    }}
                />
                {parserItems.length > 0 && <Box type='solid'>
                    <DataTable
                        className='table-bordered'
                        items={parserItems}
                        map={[
                            {
                                title: 'ID',
                                field: 'id',
                                sort: 'id',
                            },
                            {
                                title: 'Парсер',
                                field: 'parser.name',
                                sort: 'parser_id',
                            },
                            {
                                title: 'Сериал',
                                field: (parserItem) => parserItem.item &&
                                    <Link to={`/admin/items/${parserItem.item.id}`}>
                                        {parserItem.item.name_ru
                                            || parserItem.item.name_en
                                            || parserItem.item.name_original
                                        }
                                    </Link>,
                                sort: 'item_id',
                            },
                            {
                                title: 'Url',
                                field: parserItem => {
                                    return <a
                                        target='_blank'
                                        href={parserItem.url}
                                    >{parserItem.url}</a>
                                },
                                sort: 'url',
                            },
                            {
                                title: 'Активен',
                                field: parserItem => parserItem.last_activity
                                    ? <span className='text-green'>да</span>
                                    : <span className='text-gray'>нет</span>,
                                sort: 'is_active',
                            },
                            {
                                title: 'Активность',
                                field: 'last_activity',
                                sort: 'last_activity',
                            },
                            {
                                title: 'Создан',
                                field: 'created_at',
                                sort: 'created_at',
                            },
                        ]}
                        actions={[
                            {
                                className: 'fa fa-eye text-aqua',
                                to: parserItem => `/admin/parser-items/${parserItem.id}`,
                            },
                            {
                                className: 'fa fa-edit text-yellow',
                                onClick: (e, parserItem) => {
                                    this.openEditPopup(parserItem.id)
                                },
                            },
                            // {
                            //     className: 'fa fa-trash text-red',
                            //     onClick: (e, parserItem) => {
                            //         if (confirm('Вы уверены что хотите удалить страницу парсинга?')) {
                            //             this.props.deleteParserItem(parserItem.id, query)
                            //         }
                            //     },
                            // },
                        ]}
                        onSort={(e, sort) => {
                            this.props.locationPush(mergeQuery({sort: sort}))
                        }}
                        query={query}
                    />
                    <Pagination {...pagination} />
                </Box>}
            </Content>

            <Modal
                title='Редактировать'
                isOpen={this.state.editPopup}
                close={() => this.closeEditPopup()}
            >
                <Form
                    data={this.props.parserItem.data}
                    errors={this.props.parserItem.errors}
                    setData={this.props.setData}
                    saveData={(data) => {
                        this.props.saveData(data, query)
                    }}
                    parsers={parsers}
                    items={items}
                    totalItems={totalItems}
                    searchItems={(q) => {
                        this.props.fetchItems({q})
                    }}
                />
            </Modal>
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchList: (params) => dispatch(ParserItemAction.requestList(params)),
        fetchData: (id, params) => dispatch(ParserItemAction.requestData(id, params)),
        locationPush: (path) => dispatch(push(path)),
        deleteParserItem: (id) => dispatch(ParserItemAction.deleteData(id)),
        resetData: () => dispatch(ParserItemAction.resetData()),
        setData: (data) => dispatch(ParserItemAction.setData(data)),
        saveData: (data, params) => dispatch(ParserItemAction.saveData(data, params)),
        fetchParsers: (params) => dispatch(ParserAction.requestList(params)),
        fetchItems: (params) => dispatch(ItemAction.requestList(params)),
        resetItemsList: () => dispatch(ItemAction.resetList()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)
