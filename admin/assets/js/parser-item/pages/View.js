import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { get as lodashGet } from 'lodash'
import { Creators as ParserItemAction } from '../reducers/parser-item'
import { Creators as ParserAction } from '../../parser/reducers/parser'
import { Creators as ItemAction } from '../../item/reducers/item'
import NotFound from '../../common/pages/NotFound'
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Tabs from '../../common/components/content/Tabs'
import Form from '../components/Form'
import Show from '../components/Show'
import DataItem from '../components/DataItem'
import DataSeries from '../components/DataSeries'
import DataVideos from '../components/DataVideos'
import BtnApp from '../../common/components/content/BtnApp'
import Modal from '../../common/components/content/Modal'

export class View extends Component {
    constructor(props) {
        super(props)
        this.state = {
            editPopup: false,
        }
    }

    componentWillMount() {
        this.fetchData()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.routing !== nextProps.routing) {
            this.props.resetData()
            this.fetchData(this.props.routeParams.id)
        }
    }

    fetchData = (id) => {
        this.props.fetchData(id || this.props.routeParams.id)
    }

    openEditPopup = () => {
        this.props.resetItemsList()
        this.props.fetchParsers()
        this.setState({editPopup: true})
    }

    closeEditPopup = () => {
        this.setState({editPopup: false})
    }

    breadcrumb = () => {
        return [
            {
                title: 'Страницы парсинга',
                icon: 'fa-reorder',
                link: '/admin/parser-items'
            },
            {
                title: this.props.routeParams.id,
            },
        ]
    }

    render () {
        const data = lodashGet(this.props, 'parserItem.data')
        const itemData = lodashGet(this.props, 'parserItem.data.data.data')
        const itemAttrs = lodashGet(this.props, 'parserItem.data.data.attrs')
        const itemSeries = lodashGet(this.props, 'parserItem.data.data.series')
        const itemVideos = lodashGet(this.props, 'parserItem.data.data.videos')
        const parsers = lodashGet(this.props, 'parser.list.data', [])
        const items = lodashGet(this.props, 'item.list.data', [])
        const totalItems = lodashGet(this.props, 'item.list.meta.total')
        if (!data && !this.props.parserItem.isFetching) {
            return <NotFound />
        }

        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                Страницы парсинга <small>{this.props.routeParams.id}</small>
            </ContentHeader>
            <Content>
                <div className='clearfix'>
                    <BtnApp
                        className='pull-right'
                        text='Редактировать'
                        icon='fa-edit'
                        onClick={() => {
                            this.openEditPopup()
                            this.setState({createData: {}})
                        }}
                    />
                </div>

                <Tabs
                    tabs={[
                        {
                            name: 'Информация о сериале',
                            content: <DataItem data={itemData} attrs={itemAttrs} />,
                        },
                        {
                            name: 'Серии',
                            content: <DataSeries series={itemSeries} />,
                        },
                        {
                            name: 'Видео',
                            content: <DataVideos videos={itemVideos} />,
                        },
                        {
                            name: 'Параметры парсинга',
                            content: <Show data={data} />,
                        },
                    ]}
                />
            </Content>

            <Modal
                title='Редактировать'
                isOpen={this.state.editPopup}
                close={() => this.closeEditPopup()}
            >
                <Form
                    data={this.props.parserItem.data}
                    errors={this.props.parserItem.errors}
                    setData={this.props.setData}
                    saveData={(data) => {
                        this.props.saveData(data)
                    }}
                    parsers={parsers}
                    items={items}
                    totalItems={totalItems}
                    searchItems={(q) => {
                        this.props.fetchItems({q})
                    }}
                />
            </Modal>
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchData: (id) => dispatch(ParserItemAction.requestData(id)),
        setData: (data) => dispatch(ParserItemAction.setData(data)),
        saveData: (data) => dispatch(ParserItemAction.saveData(data)),
        resetData: () => dispatch(ParserItemAction.resetData()),
        locationPush: (path) => dispatch(push(path)),
        setData: (data) => dispatch(ParserItemAction.setData(data)),
        saveData: (data, params) => dispatch(ParserItemAction.saveData(data, params)),
        fetchParsers: (params) => dispatch(ParserAction.requestList(params)),
        fetchItems: (params) => dispatch(ItemAction.requestList(params)),
        resetItemsList: () => dispatch(ItemAction.resetList()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)
