import { call, put } from 'redux-saga/effects'
import { Creators as ActionItemVideo } from '../reducers/item-video'
import { Creators as ActionAuthor } from '../../author/reducers/author'
import { Creators as ActionPreloader } from '../../common/reducers/preloader'

export function * addAuthor (api, {data: {success, ...data}, params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.postAuthor, data)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(ActionItemVideo.successAddAuthor())
        yield put(ActionAuthor.requestList(params))
        if (success) {
            success(response.data.data)
        }
    } else {
        yield put(ActionItemVideo.failAddAuthor())
    }
}
