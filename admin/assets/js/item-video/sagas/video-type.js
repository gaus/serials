import { call, put } from 'redux-saga/effects'
import { Creators as ActionItemVideo } from '../reducers/item-video'
import { Creators as ActionVideoType } from '../../video-type/reducers/video-type'
import { Creators as ActionPreloader } from '../../common/reducers/preloader'

export function * addVideoType (api, {data: {success, ...data}, params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.postVideoType, data)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(ActionItemVideo.successAddVideoType())
        yield put(ActionVideoType.requestList(params))
        if (success) {
            success(response.data.data)
        }
    } else {
        yield put(ActionItemVideo.failAddVideoType())
    }
}
