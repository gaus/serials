import { call, put } from 'redux-saga/effects'
import { Creators as ActionItemVideo } from '../reducers/item-video'
import { Creators as ActionSource } from '../../source/reducers/source'
import { Creators as ActionPreloader } from '../../common/reducers/preloader'

export function * addSource (api, {data: {success, ...data}, params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.postSource, data)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(ActionItemVideo.successAddSource())
        yield put(ActionSource.requestList(params))
        if (success) {
            success(response.data.data)
        }
    } else {
        yield put(ActionItemVideo.failAddSource())
    }
}
