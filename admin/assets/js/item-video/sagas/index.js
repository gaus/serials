import { takeLatest } from 'redux-saga/effects'
import Api from '../../common/services/api'
import { Types as ItemVideoTypes } from '../reducers/item-video'
import * as itemVideo from './item-video'
import * as author from './author'
import * as source from './source'
import * as videoType from './video-type'

const api = Api.create()

export default function * sagas () {
    yield [
        takeLatest(ItemVideoTypes.REQUEST_LIST, itemVideo.getList, api),
        takeLatest(ItemVideoTypes.REQUEST_DATA, itemVideo.getData, api),
        takeLatest(ItemVideoTypes.SAVE_DATA, itemVideo.saveData, api),
        takeLatest(ItemVideoTypes.DELETE_DATA, itemVideo.deleteData, api),
        takeLatest(ItemVideoTypes.ADD_AUTHOR, author.addAuthor, api),
        takeLatest(ItemVideoTypes.ADD_SOURCE, source.addSource, api),
        takeLatest(ItemVideoTypes.ADD_VIDEO_TYPE, videoType.addVideoType, api),
    ]
}
