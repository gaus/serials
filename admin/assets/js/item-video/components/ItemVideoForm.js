import React from 'react'
import FormField from '../../common/components/form/FormField'
import Button from '../../common/components/form/Button'
import { get } from 'lodash'

export default ({
    data = {},
    errors = {},
    seasons = [],
    series = [],
    authors = [],
    totalAuthors,
    searchAuthors,
    createAuthor,
    sources = [],
    totalSources,
    searchSources,
    createSource,
    videoTypes = [],
    totalVideoTypes,
    searchVideoTypes,
    createVideoType,
    setData,
    saveData,
}) => {
    return <div>
        <FormField
            type='text'
            name='src'
            value={data.src}
            label='Ссылка на видео'
            error={errors.src && errors.src.join(' ')}
            setData={setData}
        />
        <FormField
            type='select'
            name='item_season_id'
            value={data.item_season_id}
            label='Сезон'
            error={errors.item_season_id && errors.item_season_id.join(' ')}
            setData={setData}
            list={seasons}
            listValue='id'
            listName='number'
            withEmpty
        />
        <FormField
            type='select'
            name='item_series_id'
            value={data.item_series_id}
            label='Серия'
            error={errors.item_series_id && errors.item_series_id.join(' ')}
            setData={setData}
            list={series}
            listValue='id'
            listName='number'
            withEmpty
        />
        <FormField
            type='select-search'
            name='author_id'
            value={data.author_id}
            valueName={get(data, 'author.name')}
            label='Автор перевода'
            error={errors.author_id && errors.author_id.join(' ')}
            setData={setData}
            items={authors}
            total={totalAuthors}
            valueField='id'
            nameField='name'
            search={searchAuthors}
            create={createAuthor}
        />
        <FormField
            type='select-search'
            name='video_type_id'
            value={data.video_type_id}
            valueName={get(data, 'video_type.name')}
            label='Перевод'
            error={errors.video_type_id && errors.video_type_id.join(' ')}
            setData={setData}
            items={videoTypes}
            total={totalVideoTypes}
            valueField='id'
            nameField='name'
            search={searchVideoTypes}
            create={createVideoType}
        />
        <FormField
            type='select-search'
            name='source_id'
            value={data.source_id}
            valueName={get(data, 'source.name')}
            label='Источник'
            error={errors.source_id && errors.source_id.join(' ')}
            setData={setData}
            items={sources}
            total={totalSources}
            valueField='id'
            nameField='name'
            search={searchSources}
            create={createSource}
        />
        <FormField
            type='text'
            name='quality'
            value={data.quality}
            label='Качество'
            error={errors.quality && errors.quality.join(' ')}
            setData={setData}
        />
        <Button
            type='submit'
            className='btn-primary'
            text='Сохранить'
            onClick={() => {
                saveData(data)
            }}
        />
    </div>
}
