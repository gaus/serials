import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
    toggleCreateModal: ['value'],
    toggleEditModal: ['value'],
    toggleShowModal: ['value'],
    requestList: ['params'],
    successList: ['data'],
    failList: [],
    requestData: ['id'],
    successData: ['data'],
    failData: [],
    setData: ['data'],
    saveData: ['data', 'params'],
    successSaveData: ['data'],
    failSaveData: ['errors'],
    deleteData: ['id', 'params'],
    successDeleteData: [],
    failDeleteData: [],
    addAuthor: ['data', 'params'],
    successAddAuthor: [],
    failAddAuthor: [],
    addSource: ['data', 'params'],
    successAddSource: [],
    failAddSource: [],
    addVideoType: ['data', 'params'],
    successAddVideoType: [],
    failAddVideoType: [],
}, { prefix: 'itemVideo/' })

export const INITIAL_STATE = {
    isFetching: false,
    isCreateModalOpen: false,
    isEditModalOpen: false,
    isShowModalOpen: false,
    list: [],
    data: undefined,
    errors: undefined,
}

export const toggleCreateModal = (state, { value }) => {
    return {...state, errors: undefined, isCreateModalOpen: value}
}

export const toggleEditModal = (state, { value }) => {
    return {...state, errors: undefined, isEditModalOpen: value}
}

export const toggleShowModal = (state, { value }) => {
    return {...state, isShowModalOpen: value}
}

export const requestList = (state) => {
    return {...state, isFetching: true}
}

export const successList = (state, { data }) => {
    return {...state, ...{list: data}, isFetching: false}
}

export const failList = (state) => {
    return {...state, isFetching: false}
}

export const requestData = (state) => {
    return {...state, isFetching: true}
}

export const successData = (state, { data }) => {
    return {...state, ...{data}, isFetching: false}
}

export const failData = (state) => {
    return {...state, isFetching: false}
}

export const setData = (state, {data}) => {
    return {...state, data: {...state.data, ...data}}
}

export const saveData = (state) => {
    return {...state, isFetching: true}
}

export const successSaveData = (state, { data }) => {
    return {...state, ...{data}, errors: undefined, isFetching: false}
}

export const failSaveData = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const deleteData = (state) => {
    return {...state, isFetching: true}
}

export const successDeleteData = (state) => {
    return {...state, isFetching: false}
}

export const failDeleteData = (state) => {
    return {...state, isFetching: false}
}

export const addAuthor = (state) => {
    return {...state, isFetching: true}
}

export const successAddAuthor = (state) => {
    return {...state, isFetching: false}
}

export const failAddAuthor = (state) => {
    return {...state, isFetching: false}
}

export const addSource = (state) => {
    return {...state, isFetching: true}
}

export const successAddSource = (state) => {
    return {...state, isFetching: false}
}

export const failAddSource = (state) => {
    return {...state, isFetching: false}
}

export const addVideoType = (state) => {
    return {...state, isFetching: true}
}

export const successAddVideoType = (state) => {
    return {...state, isFetching: false}
}

export const failAddVideoType = (state) => {
    return {...state, isFetching: false}
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.TOGGLE_CREATE_MODAL]: toggleCreateModal,
    [Types.TOGGLE_EDIT_MODAL]: toggleEditModal,
    [Types.TOGGLE_SHOW_MODAL]: toggleShowModal,
    [Types.REQUEST_LIST]: requestList,
    [Types.SUCCESS_LIST]: successList,
    [Types.FAIL_LIST]: failList,
    [Types.REQUEST_DATA]: requestData,
    [Types.SUCCESS_DATA]: successData,
    [Types.FAIL_DATA]: failData,
    [Types.SET_DATA]: setData,
    [Types.SAVE_DATA]: saveData,
    [Types.SUCCESS_SAVE_DATA]: successSaveData,
    [Types.FAIL_SAVE_DATA]: failSaveData,
    [Types.DELETE_DATA]: deleteData,
    [Types.SUCCESS_DELETE_DATA]: successDeleteData,
    [Types.FAIL_DELETE_DATA]: failDeleteData,
    [Types.ADD_AUTHOR]: addAuthor,
    [Types.SUCCESS_ADD_AUTHOR]: successAddAuthor,
    [Types.FAIL_ADD_AUTHOR]: failAddAuthor,
    [Types.ADD_SOURCE]: addSource,
    [Types.SUCCESS_ADD_SOURCE]: successAddSource,
    [Types.FAIL_ADD_SOURCE]: failAddSource,
    [Types.ADD_VIDEO_TYPE]: addVideoType,
    [Types.SUCCESS_ADD_VIDEO_TYPE]: successAddVideoType,
    [Types.FAIL_ADD_VIDEO_TYPE]: failAddVideoType,
})
