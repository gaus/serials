import React from 'react'
import FormField from '../../common/components/form/FormField'
import Button from '../../common/components/form/Button'

export default ({
    data = {},
    errors = {},
    setData,
    saveData,
}) => {
    return <div>
        <FormField
            type='number'
            name='number'
            value={data.number}
            label='Номер'
            error={errors.number && errors.number.join(' ')}
            setData={setData}
        />
        <FormField
            type='date'
            name='started_at'
            value={data.started_at}
            label='Начало выпуска'
            error={errors.started_at && errors.started_at.join(' ')}
            setData={setData}
        />
        <FormField
            type='date'
            name='completed_at'
            value={data.completed_at}
            label='Конец выпуска'
            error={errors.completed_at && errors.completed_at.join(' ')}
            setData={setData}
        />
        <Button
            type='submit'
            className='btn-primary'
            text='Сохранить'
            onClick={() => {
                saveData(data)
            }}
        />
    </div>
}
