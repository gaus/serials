import { takeLatest } from 'redux-saga/effects'
import Api from '../../common/services/api'
import { Types as ItemSeasonTypes } from '../reducers/item-season'
import * as itemSeason from './item-season'

const api = Api.create()

export default function * sagas () {
    yield [
        takeLatest(ItemSeasonTypes.REQUEST_LIST, itemSeason.getList, api),
        takeLatest(ItemSeasonTypes.REQUEST_DATA, itemSeason.getData, api),
        takeLatest(ItemSeasonTypes.SAVE_DATA, itemSeason.saveData, api),
        takeLatest(ItemSeasonTypes.DELETE_DATA, itemSeason.deleteData, api),
    ]
}
