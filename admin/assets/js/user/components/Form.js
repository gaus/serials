import React from 'react'
import FormField from '../../common/components/form/FormField'
import Button from '../../common/components/form/Button'
import Box from '../../common/components/content/Box'

export default ({
    data = {},
    errors = {},
    setData,
    saveData,
}) => {
    return <Box type='solid'>
        <form
            onSubmit={(e) => {
                e.preventDefault()
                saveData(data)
            }}
        >
            <FormField
                type='text'
                name='name'
                value={data.name}
                label='Имя'
                error={errors.name && errors.name.join(' ')}
                setData={setData}
            />
            <FormField
                type='email'
                name='email'
                value={data.email}
                label='Email'
                error={errors.email && errors.email.join(' ')}
                setData={setData}
            />
            <FormField
                type='text'
                name='password'
                value={data.password}
                label='Пароль'
                error={errors.password && errors.password.join(' ')}
                setData={setData}
            />
            <Button type='submit' className='btn-primary' text='Сохранить' />
        </form>
    </Box>
}
