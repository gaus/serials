import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { get as lodashGet } from 'lodash'
import { Creators as UserAction } from '../reducers/user'
import NotFound from '../../common/pages/NotFound'
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Form from '../components/Form'

export class UserEdit extends Component {
    componentWillMount() {
        this.fetchUser()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.routing !== nextProps.routing) {
            this.fetchUser(this.props.routeParams.id)
        }
    }

    fetchUser = () => {
        this.props.fetchUser(this.props.routeParams.id)
    }

    breadcrumb = () => {
        return [
            {
                title: 'Пользователи',
                icon: 'fa-users',
                link: '/admin/users'
            },
            {
                title: 'Редактировать',
            },
        ]
    }

    render () {
        const userId = lodashGet(this.props, 'user.data.id')

        if (!userId && !this.props.user.isFetching) {
            return <NotFound />
        }

        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                Пользователи <small>Редактировать</small>
            </ContentHeader>
            <Content>
                <Form
                    data={this.props.user.data}
                    errors={this.props.user.errors}
                    setData={this.props.setData}
                    saveData={this.props.saveUser}
                />
            </Content>
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchUser: (id) => dispatch(UserAction.requestUser(id)),
        setData: (data) => dispatch(UserAction.setData(data)),
        saveUser: (data) => dispatch(UserAction.saveUser(data)),
        locationPush: (path) => dispatch(push(path)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserEdit)
