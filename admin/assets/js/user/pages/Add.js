import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { Creators as UserAction } from '../reducers/user'
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Form from '../components/Form'

export class UserAdd extends Component {
    constructor () {
        super()
        this.state = {}
    }

    setData = (data) => {
        this.setState(data)
    }

    breadcrumb = () => {
        return [
            {
                title: 'Пользователи',
                icon: 'fa-users',
                link: '/admin/users'
            },
            {
                title: 'Добавить',
            },
        ]
    }

    render () {
        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                Пользователи <small>Добавить</small>
            </ContentHeader>
            <Content>
                <Form
                    data={this.state}
                    errors={this.props.user.errors}
                    setData={this.setData}
                    saveData={this.props.saveUser}
                />
            </Content>
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        saveUser: (data) => dispatch(UserAction.saveUser(data)),
        locationPush: (path) => dispatch(push(path)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserAdd)
