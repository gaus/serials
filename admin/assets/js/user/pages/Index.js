import React, { Component } from 'react'
import { connect } from 'react-redux'
import { get as lodashGet } from 'lodash'
import { push } from 'react-router-redux'
import { Link } from 'react-router'
import { Creators as UserAction } from '../reducers/user'
import { mergeQuery } from "../../common/helpers/url-helper"
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Box from '../../common/components/content/Box'
import DataTable from '../../common/components/table/DataTable'
import Pagination from '../../common/components/pagination/Pagination'

export class UserIndex extends Component {
    componentWillMount () {
        this.fetchUsers()
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.routing !== nextProps.routing) {
            this.fetchUsers(lodashGet(nextProps, 'routing.locationBeforeTransitions.query'))
        }
    }

    fetchUsers = (query) => {
        this.props.fetchList(query || lodashGet(this.props, 'routing.locationBeforeTransitions.query', {}))
    }

    breadcrumb = () => {
        return [
            {
                title: 'Пользователи',
                icon: 'fa-users',
            }
        ]
    }

    render () {
        const users = lodashGet(this.props, 'user.list.data', [])
        const pagination = lodashGet(this.props, 'user.list.meta', {})
        const query = lodashGet(this.props, 'routing.locationBeforeTransitions.query', {})
        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                Пользователи <small><Link to='/admin/users/add'>Добавить пользователя</Link></small>
            </ContentHeader>
            <Content>
                {users.length > 0 && <Box type='solid'>
                    <DataTable
                        className='table-bordered'
                        items={users}
                        map={[
                            {
                                title: 'ID',
                                field: 'id',
                                sort: 'id',
                            },
                            {
                                title: 'Имя',
                                field: 'name',
                                sort: 'name',
                            },
                            {
                                title: 'Email',
                                field: 'email',
                                sort: 'email',
                            },
                            {
                                title: 'Регистрация',
                                field: 'created_at',
                                sort: 'created_at',
                            },
                        ]}
                        actions={[
                            {
                                className: 'fa fa-edit text-yellow',
                                onClick: (e, item) => {
                                    this.props.locationPush('/admin/users/' + item.id)
                                },
                            },
                            {
                                className: 'fa fa-trash text-red',
                                onClick: (e, item) => {
                                    if (confirm('Вы уверены что хотите удалить пользователя?')) {
                                        this.props.deleteUser(item.id, query)
                                    }
                                },
                            },
                        ]}
                        onSort={(e, sort) => {
                            this.props.locationPush(mergeQuery({sort: sort}))
                        }}
                        query={query}
                    />
                    <Pagination {...pagination} />
                </Box>}
            </Content>
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchList: (params) => dispatch(UserAction.requestList(params)),
        locationPush: (path) => dispatch(push(path)),
        deleteUser: (id, query) => dispatch(UserAction.deleteUser(id, query)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserIndex)
