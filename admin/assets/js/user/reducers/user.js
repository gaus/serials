import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
    requestList: ['params'],
    successList: ['data'],
    failList: [],
    requestUser: ['id'],
    successUser: ['data'],
    failUser: [],
    setData: ['data'],
    saveUser: ['data'],
    successSaveUser: ['data'],
    failSaveUser: ['errors'],
    deleteUser: ['id', 'params'],
    successDeleteUser: [],
    failDeleteUser: [],
}, { prefix: 'user/' })

export const INITIAL_STATE = {
    isFetching: false,
    list: [],
    data: undefined,
    errors: undefined,
}

export const requestList = (state) => {
    return {...state, isFetching: true}
}

export const successList = (state, { data }) => {
    return {...state, ...{list: data}, isFetching: false}
}

export const failList = (state) => {
    return {...state, isFetching: false}
}

export const requestUser = (state) => {
    return {...state, isFetching: true}
}

export const successUser = (state, { data }) => {
    return {...state, ...{data}, isFetching: false}
}

export const failUser = (state) => {
    return {...state, isFetching: false}
}

export const setData = (state, {data}) => {
    return {...state, data: {...state.data, ...data}}
}

export const saveUser = (state) => {
    return {...state, isFetching: true}
}

export const successSaveUser = (state, { data }) => {
    return {...state, ...{data}, errors: undefined, isFetching: false}
}

export const failSaveUser = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const deleteUser = (state) => {
    return {...state, isFetching: true}
}

export const successDeleteUser = (state) => {
    return {...state, isFetching: false}
}

export const failDeleteUser = (state) => {
    return {...state, isFetching: false}
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.REQUEST_LIST]: requestList,
    [Types.SUCCESS_LIST]: successList,
    [Types.FAIL_LIST]: failList,
    [Types.REQUEST_USER]: requestUser,
    [Types.SUCCESS_USER]: successUser,
    [Types.FAIL_USER]: failUser,
    [Types.SET_DATA]: setData,
    [Types.SAVE_USER]: saveUser,
    [Types.SUCCESS_SAVE_USER]: successSaveUser,
    [Types.FAIL_SAVE_USER]: failSaveUser,
    [Types.DELETE_USER]: deleteUser,
    [Types.SUCCESS_DELETE_USER]: successDeleteUser,
    [Types.FAIL_DELETE_USER]: failDeleteUser,
})
