import { call, put } from 'redux-saga/effects'
import { push } from 'react-router-redux'
import { Creators as Action } from '../reducers/user'
import { Creators as ActionInformer } from '../../common/reducers/informer'
import { Creators as ActionPreloader } from '../../common/reducers/preloader'

export function * getUsers (api, {params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getUsers, params)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successList(response.data))
    } else {
        yield put(Action.failList())
    }
}

export function * getUser (api, {id}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getUser, id)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successUser(response.data.data))
    } else {
        yield put(Action.failUser())
    }
}

export function * saveUser (api, {data}) {
    yield put(ActionPreloader.start())
    const response = data.id
        ? yield call(api.putUser, data.id, data)
        : yield call(api.postUser, data)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successSaveUser(response.data.data))
        if (!data.id) {
            yield put(push('/admin/users/' + response.data.data.id))
        }
        data.id
            ? yield put(ActionInformer.push('success', 'Изменения сохранены'))
            : yield put(ActionInformer.push('success', 'Пользователь добавлен'))
    } else {
        yield put(Action.failSaveUser(response.data.errors))
        yield put(ActionInformer.push('danger', 'Вы ввели некорректные данные'))
    }
}

export function * deleteUser(api, {id, params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.deleteUser, id)
    yield put(ActionPreloader.stop())
    if (response.status === 204) {
        yield put(Action.successDeleteUser())
        yield put(Action.requestList(params))
        yield put(ActionInformer.push('success', 'Пользователь удален'))
    } else {
        yield put(Action.failDeleteUser())
    }
}
