import { takeLatest } from 'redux-saga/effects'
import Api from '../../common/services/api'
import { Types as UserTypes } from '../reducers/user'
import * as user from './user'

const api = Api.create()

export default function * sagas () {
    yield [
        takeLatest(UserTypes.REQUEST_LIST, user.getUsers, api),
        takeLatest(UserTypes.REQUEST_USER, user.getUser, api),
        takeLatest(UserTypes.SAVE_USER, user.saveUser, api),
        takeLatest(UserTypes.DELETE_USER, user.deleteUser, api),
    ]
}
