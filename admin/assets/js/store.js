import { createStore, applyMiddleware } from 'redux'
import { browserHistory } from 'react-router'
import { routerMiddleware } from 'react-router-redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction'
import indexReducer from './reducers'
import commonSagas from './common/sagas/index'
import userSagas from './user/sagas/index'
import itemSagas from './item/sagas/index'
import attrSagas from './attr/sagas/index'
import ItemSeasonSagas from './item-season/sagas/index'
import ItemSeriesSagas from './item-series/sagas/index'
import ItemVideoSagas from './item-video/sagas/index'
import AuthorSagas from './author/sagas/index'
import SourceSagas from './source/sagas/index'
import VideoTypeSagas from './video-type/sagas/index'
import ParserItemSagas from './parser-item/sagas/index'
import ParserSagas from './parser/sagas/index'

const middleware = routerMiddleware(browserHistory)
const sagaMiddleware = createSagaMiddleware()

export default function configureStore() {
    const store = composeWithDevTools(
        applyMiddleware(sagaMiddleware),
        applyMiddleware(middleware)
    )(createStore)(indexReducer)
    sagaMiddleware.run(commonSagas)
    sagaMiddleware.run(userSagas)
    sagaMiddleware.run(itemSagas)
    sagaMiddleware.run(attrSagas)
    sagaMiddleware.run(ItemSeasonSagas)
    sagaMiddleware.run(ItemSeriesSagas)
    sagaMiddleware.run(ItemVideoSagas)
    sagaMiddleware.run(AuthorSagas)
    sagaMiddleware.run(SourceSagas)
    sagaMiddleware.run(VideoTypeSagas)
    sagaMiddleware.run(ParserItemSagas)
    sagaMiddleware.run(ParserSagas)
    return store
}
