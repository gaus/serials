import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { get as lodashGet } from 'lodash'
import { Creators as ItemAction } from '../reducers/item'
import { Creators as ParserItemAction } from '../../parser-item/reducers/parser-item'
import { Creators as ParserAction } from '../../parser/reducers/parser'
import { mergeQuery } from '../../common/helpers/url-helper'
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Row from '../../common/components/content/Row'
import Col from '../../common/components/content/Col'
import Box from '../../common/components/content/Box'
import Modal from '../../common/components/content/Modal'
import BtnApp from '../../common/components/content/BtnApp'
import DataTable from '../../common/components/table/DataTable'
import Pagination from '../../common/components/pagination/Pagination'
import ParserItemForm from '../../parser-item/components/Form'
import MenuButtons from '../components/MenuButtons'

export class ItemParsing extends Component {
    constructor (props) {
        super(props)
        this.state = {
            editPopup: false,
        }
    }

    componentWillMount() {
        this.fetchItem()
        this.fetchItemParsing()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.routing !== nextProps.routing) {
            this.fetchItem(this.props.routeParams.id)
            this.fetchItemParsing(lodashGet(nextProps, 'routing.locationBeforeTransitions.query'))
        }
    }

    fetchItem = () => {
        this.props.fetchItemData(this.props.routeParams.id)
    }

    fetchItemParsing = (query) => {
        query = query || lodashGet(this.props, 'routing.locationBeforeTransitions.query', {})
        this.props.fetchList({...query, item_id: this.props.routeParams.id})
    }

    openEditPopup = (id) => {
        this.props.resetData()
        id && this.props.fetchData(id)
        this.props.fetchParsers()
        this.setState({editPopup: true})
    }

    closeEditPopup = () => {
        this.setState({editPopup: false})
    }

    breadcrumb = () => {
        return [
            {
                title: 'Сериалы',
                icon: 'fa-film',
                link: '/admin/items'
            },
            {
                title: `Сериал №${this.props.routeParams.id}`,
                link: `/admin/items/${this.props.routeParams.id}`
            },
            {
                title: 'Список парсинга',
            },
        ]
    }

    render () {
        const query = lodashGet(this.props, 'routing.locationBeforeTransitions.query', {})
        const pagination = lodashGet(this.props, 'parserItem.list.meta', {})
        const list = lodashGet(this.props, 'parserItem.list.data', [])
        const data = lodashGet(this.props, 'parserItem.data')
        const parsers = lodashGet(this.props, 'parser.list.data', [])
        const name = lodashGet(this.props, 'item.data.name_ru')
            || lodashGet(this.props, 'item.data.name_en')
            || lodashGet(this.props, 'item.data.name_original')
            || `Сериал №${this.props.routeParams.id}`

        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                {name} <small>Список парсинга</small>
            </ContentHeader>
            {this.props.item.data && <Content>
                <Row>
                    <Col md='2'>
                        <BtnApp
                            text='Добавить'
                            icon='fa-plus'
                            onClick={() => this.openEditPopup()}
                        />
                    </Col>
                    <Col md='10'>
                        <MenuButtons {...this.props} />
                    </Col>
                </Row>
                {list.length > 0 && <Box type='solid'>
                    <DataTable
                        className='table-bordered'
                        items={list}
                        map={[
                            {
                                title: 'ID',
                                field: 'id',
                                sort: 'id',
                            },
                            {
                                title: 'Парсер',
                                field: 'parser.name',
                                sort: 'parser_id',
                            },
                            {
                                title: 'Url',
                                field: parserItem => {
                                    return <a
                                        target='_blank'
                                        href={parserItem.url}
                                    >{parserItem.url}</a>
                                },
                                sort: 'url',
                            },
                            {
                                title: 'Активен',
                                field: parserItem => parserItem.is_active
                                    ? <span className='text-green'>да</span>
                                    : <span className='text-gray'>нет</span>,
                                sort: 'is_active',
                            },
                            {
                                title: 'Активность',
                                field: 'last_activity',
                                sort: 'last_activity',
                            },
                            {
                                title: 'Создан',
                                field: 'created_at',
                                sort: 'created_at',
                            },
                        ]}
                        actions={[
                            {
                                className: 'fa fa-eye text-aqua',
                                onClick: (e, parserItem) => {
                                    this.props.locationPush(`/admin/parser-items/${parserItem.id}`)
                                },
                            },
                            {
                                className: 'fa fa-edit text-yellow',
                                onClick: (e, parserItem) => {
                                    this.openEditPopup(parserItem.id)
                                },
                            },
                            // {
                            //     className: 'fa fa-trash text-red',
                            //     onClick: (e, parserItem) => {
                            //         if (confirm('Вы уверены что хотите удалить страницу парсинга?')) {
                            //             this.props.deleteParserItem(parserItem.id, query)
                            //         }
                            //     },
                            // },
                        ]}
                        onSort={(e, sort) => {
                            this.props.locationPush(mergeQuery({sort: sort}))
                        }}
                        query={query}
                    />
                    <Pagination {...pagination} />
                </Box>}
                <Modal
                    title={data && data.id ? 'Редактировать' : 'Добавить'}
                    isOpen={this.state.editPopup}
                    close={() => this.closeEditPopup()}
                >
                    <ParserItemForm
                        data={data}
                        errors={this.props.parserItem.errors}
                        setData={this.props.setData}
                        saveData={(data) => {
                            this.props.saveData(
                                {...data, item_id: this.props.routeParams.id},
                                {...query, item_id: this.props.routeParams.id}
                            )
                        }}
                        parsers={parsers}
                        withoutItemSelect
                    />
                </Modal>
            </Content>}
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchItemData: (id) => dispatch(ItemAction.requestData(id)),
        fetchParsers: (params) => dispatch(ParserAction.requestList(params)),
        fetchList: (params) => dispatch(ParserItemAction.requestList(params)),
        fetchData: (id) => dispatch(ParserItemAction.requestData(id)),
        setData: (data) => dispatch(ParserItemAction.setData(data)),
        saveData: (data, params) => dispatch(ParserItemAction.saveData(data, params)),
        deleteData: (data, params) => dispatch(ParserItemAction.deleteData(data, params)),
        locationPush: (path) => dispatch(push(path)),
        resetData: () => dispatch(ParserItemAction.resetData()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemParsing)
