import React, { Component } from 'react'
import { connect } from 'react-redux'
import { get as lodashGet } from 'lodash'
import { push } from 'react-router-redux'
import { Link } from 'react-router'
import { Creators as ItemAction } from '../reducers/item'
import { mergeQuery } from '../../common/helpers/url-helper'
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Box from '../../common/components/content/Box'
import DataTable from '../../common/components/table/DataTable'
import Pagination from '../../common/components/pagination/Pagination'
import Filter from '../components/Filter'

export class ItemIndex extends Component {
    componentWillMount () {
        this.fetchItems()
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.routing !== nextProps.routing) {
            this.fetchItems(lodashGet(nextProps, 'routing.locationBeforeTransitions.query'))
        }
    }

    fetchItems = (query) => {
        this.props.fetchList(query || lodashGet(this.props, 'routing.locationBeforeTransitions.query', {}))
    }

    breadcrumb = () => {
        return [
            {
                title: 'Сериалы',
                icon: 'fa-film',
            }
        ]
    }

    render () {
        const items = lodashGet(this.props, 'item.list.data', [])
        const pagination = lodashGet(this.props, 'item.list.meta', {})
        const query = lodashGet(this.props, 'routing.locationBeforeTransitions.query', {})
        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                Сериалы <small><Link to='/admin/items/add'>Добавить сериал</Link></small>
            </ContentHeader>
            <Content>
                <Filter
                    query={query}
                    apply={(data) => {
                        this.props.locationPush(mergeQuery(data))
                    }}
                />
                {items.length > 0 && <Box type='solid'>
                    <DataTable
                        className='table-bordered'
                        items={items}
                        map={[
                            {
                                title: 'ID',
                                field: 'id',
                                sort: 'id',
                            },
                            {
                                field: (item) => <img src={item.image_src} height='60' />,
                            },
                            {
                                title: 'Год выпуска',
                                field: 'year_start',
                                sort: 'year_start',
                            },
                            {
                                title: 'Год завершения',
                                field: 'year_complete',
                                sort: 'year_complete',
                            },
                            {
                                title: 'Русское название',
                                field: 'name_ru',
                                sort: 'name_ru',
                            },
                            {
                                title: 'Английское название',
                                field: 'name_en',
                                sort: 'name_en',
                            },
                            {
                                title: 'Оригинальное название',
                                field: 'name_original',
                                sort: 'name_original',
                            },
                            {
                                title: 'Добавлено',
                                field: 'created_at',
                                sort: 'created_at',
                            },
                        ]}
                        actions={[
                            {
                                className: 'fa fa-edit text-yellow',
                                onClick: (e, item) => {
                                    this.props.locationPush('/admin/items/' + item.id)
                                },
                            },
                            {
                                className: 'fa fa-trash text-red',
                                onClick: (e, item) => {
                                    if (confirm('Вы уверены что хотите удалить сериал?')) {
                                        this.props.deleteItem(item.id, query)
                                    }
                                },
                            },
                        ]}
                        onSort={(e, sort) => {
                            this.props.locationPush(mergeQuery({sort: sort}))
                        }}
                        query={query}
                    />
                    <Pagination {...pagination} />
                </Box>}
            </Content>
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchList: (params) => dispatch(ItemAction.requestList(params)),
        locationPush: (path) => dispatch(push(path)),
        deleteItem: (id, query) => dispatch(ItemAction.deleteData(id, query)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemIndex)
