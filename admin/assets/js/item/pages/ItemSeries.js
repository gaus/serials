import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { get as lodashGet } from 'lodash'
import { Creators as ItemAction } from '../reducers/item'
import { Creators as ItemSeasonAction } from '../../item-season/reducers/item-season'
import { Creators as ItemSeriesAction } from '../../item-series/reducers/item-series'
import { mergeQuery } from '../../common/helpers/url-helper'
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Row from '../../common/components/content/Row'
import Col from '../../common/components/content/Col'
import Box from '../../common/components/content/Box'
import Modal from '../../common/components/content/Modal'
import BtnApp from '../../common/components/content/BtnApp'
import DataTable from '../../common/components/table/DataTable'
import Pagination from '../../common/components/pagination/Pagination'
import ItemSeriesForm from '../../item-series/components/ItemSeriesForm'
import MenuButtons from '../components/MenuButtons'

export class ItemSeries extends Component {
    constructor () {
        super()
        this.state = {
            createData: {},
        }
    }

    componentWillMount() {
        this.fetchItem()
        this.fetchItemSeasons()
        this.fetchItemSeries()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.routing !== nextProps.routing) {
            this.fetchItem(this.props.routeParams.id)
            this.fetchItemSeasons()
            this.fetchItemSeries(lodashGet(nextProps, 'routing.locationBeforeTransitions.query'))
        }
    }

    fetchItem = () => {
        this.props.fetchItemData(this.props.routeParams.id)
    }

    fetchItemSeasons = () => {
        this.props.fetchSeasonList({
            item_id: this.props.routeParams.id,
            sort: 'number',
            per_page: 0,
        })
    }

    fetchItemSeries = (query) => {
        query = query || lodashGet(this.props, 'routing.locationBeforeTransitions.query', {})
        query = {...query, item_id: this.props.routeParams.id}
        this.props.fetchList(query)
    }

    breadcrumb = () => {
        return [
            {
                title: 'Сериалы',
                icon: 'fa-film',
                link: '/admin/items'
            },
            {
                title: `Сериал №${this.props.routeParams.id}`,
                link: `/admin/items/${this.props.routeParams.id}`
            },
            {
                title: 'Список серий',
            },
        ]
    }

    render () {
        const query = lodashGet(this.props, 'routing.locationBeforeTransitions.query', {})
        const pagination = lodashGet(this.props, 'itemSeries.list.meta', {})
        const seasonList = lodashGet(this.props, 'itemSeason.list.data', [])
        const list = lodashGet(this.props, 'itemSeries.list.data', [])
        const name = lodashGet(this.props, 'item.data.name_ru')
            || lodashGet(this.props, 'item.data.name_en')
            || lodashGet(this.props, 'item.data.name_original')
            || `Сериал №${this.props.routeParams.id}`

        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                {name} <small>Список серий</small>
            </ContentHeader>
            {this.props.item.data && <Content>
                <Row>
                    <Col md='2'>
                        <BtnApp
                            text='Добавить'
                            icon='fa-plus'
                            onClick={() => {
                                this.props.toggleCreateModal(true)
                                this.setState({createData: {}})
                            }}
                        />
                    </Col>
                    <Col md='10'>
                        <MenuButtons {...this.props} />
                    </Col>
                </Row>
                {list.length > 0 && <Box type='solid'>
                    <DataTable
                        className='table-bordered'
                        items={list}
                        map={[
                            {
                                title: 'ID',
                                field: 'id',
                                sort: 'id',
                            },
                            {
                                title: 'Сезон',
                                field: 'season.number',
                                sort: 'season.number',
                            },
                            {
                                title: 'Номер',
                                field: 'number',
                                sort: 'number',
                            },
                            {
                                title: 'Русское название',
                                field: 'name_ru',
                                sort: 'name_ru',
                            },
                            {
                                title: 'Оригинальное название',
                                field: 'name_original',
                                sort: 'name_original',
                            },
                            {
                                title: 'Дата выхода',
                                field: 'released_at',
                                sort: 'released_at',
                            },
                            {
                                title: 'Добавлено',
                                field: 'created_at',
                                sort: 'created_at',
                            },
                        ]}
                        actions={[
                            {
                                className: 'fa fa-edit text-yellow',
                                onClick: (e, itemSeries) => {
                                    this.props.toggleEditModal(true)
                                    this.props.fetchData(itemSeries.id)
                                },
                            },
                            {
                                className: 'fa fa-trash text-red',
                                onClick: (e, itemSeries) => {
                                    if (confirm('Вы уверены что хотите удалить серию серала?')) {
                                        this.props.deleteData(itemSeries.id, {...query, item_id: this.props.routeParams.id})
                                    }
                                },
                            },
                        ]}
                        onSort={(e, sort) => {
                            this.props.locationPush(mergeQuery({sort: sort}))
                        }}
                        query={query}
                    />
                    <Pagination {...pagination} />
                </Box>}
                <Modal
                    title='Добавить серию'
                    isOpen={this.props.itemSeries.isCreateModalOpen}
                    close={() => {
                        this.props.toggleCreateModal(false)
                    }}
                >
                    <ItemSeriesForm
                        data={this.state.createData}
                        errors={this.props.itemSeries.errors}
                        setData={(data) => {
                            this.setState({createData: {...this.state.createData, ...data}})
                        }}
                        saveData={(data) => {
                            this.props.saveData(data, {...query, item_id: this.props.routeParams.id})
                        }}
                        seasons={seasonList}
                    />
                </Modal>
                <Modal
                    title='Редактировать серию'
                    isOpen={this.props.itemSeries.isEditModalOpen}
                    close={() => {
                        this.props.toggleEditModal(false)
                    }}
                >
                    <ItemSeriesForm
                        data={this.props.itemSeries.data}
                        errors={this.props.itemSeries.errors}
                        setData={this.props.setData}
                        saveData={(data) => {this.props.saveData(data, {...query, item_id: this.props.routeParams.id})}}
                        seasons={seasonList}
                    />
                </Modal>
            </Content>}
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        toggleCreateModal: (value) => dispatch(ItemSeriesAction.toggleCreateModal(value)),
        toggleEditModal: (value) => dispatch(ItemSeriesAction.toggleEditModal(value)),
        fetchItemData: (id) => dispatch(ItemAction.requestData(id)),
        fetchSeasonList: (params) => dispatch(ItemSeasonAction.requestList(params)),
        fetchList: (params) => dispatch(ItemSeriesAction.requestList(params)),
        fetchData: (id) => dispatch(ItemSeriesAction.requestData(id)),
        setData: (data) => dispatch(ItemSeriesAction.setData(data)),
        saveData: (data, params) => dispatch(ItemSeriesAction.saveData(data, params)),
        deleteData: (data, params) => dispatch(ItemSeriesAction.deleteData(data, params)),
        locationPush: (path) => dispatch(push(path)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemSeries)
