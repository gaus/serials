import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { get as lodashGet } from 'lodash'
import { Creators as ItemAction } from '../reducers/item'
import { Creators as ItemSeasonAction } from '../../item-season/reducers/item-season'
import { mergeQuery } from '../../common/helpers/url-helper'
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Row from '../../common/components/content/Row'
import Col from '../../common/components/content/Col'
import Box from '../../common/components/content/Box'
import Modal from '../../common/components/content/Modal'
import BtnApp from '../../common/components/content/BtnApp'
import DataTable from '../../common/components/table/DataTable'
import Pagination from '../../common/components/pagination/Pagination'
import ItemSeasonForm from '../../item-season/components/ItemSeasonForm'
import MenuButtons from '../components/MenuButtons'

export class ItemSeasons extends Component {
    constructor () {
        super()
        this.state = {
            createData: {},
        }
    }

    componentWillMount() {
        this.fetchItem()
        this.fetchItemSeasons()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.routing !== nextProps.routing) {
            this.fetchItem(this.props.routeParams.id)
            this.fetchItemSeasons(lodashGet(nextProps, 'routing.locationBeforeTransitions.query'))
        }
    }

    fetchItem = () => {
        this.props.fetchItemData(this.props.routeParams.id)
    }

    fetchItemSeasons = (query) => {
        query = query || lodashGet(this.props, 'routing.locationBeforeTransitions.query', {})
        query = {...query, item_id: this.props.routeParams.id}
        this.props.fetchList(query)
    }

    breadcrumb = () => {
        return [
            {
                title: 'Сериалы',
                icon: 'fa-film',
                link: '/admin/items'
            },
            {
                title: `Сериал №${this.props.routeParams.id}`,
                link: `/admin/items/${this.props.routeParams.id}`
            },
            {
                title: 'Список сезонов',
            },
        ]
    }

    render () {
        const query = lodashGet(this.props, 'routing.locationBeforeTransitions.query', {})
        const pagination = lodashGet(this.props, 'itemSeason.list.meta', {})
        const list = lodashGet(this.props, 'itemSeason.list.data', [])
        const name = lodashGet(this.props, 'item.data.name_ru')
            || lodashGet(this.props, 'item.data.name_en')
            || lodashGet(this.props, 'item.data.name_original')
            || `Сериал №${this.props.routeParams.id}`

        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                {name} <small>Список сезонов</small>
            </ContentHeader>
            {this.props.item.data && <Content>
                <Row>
                    <Col md='2'>
                        <BtnApp
                            text='Добавить'
                            icon='fa-plus'
                            onClick={() => {
                                this.props.toggleCreateModal(true)
                                this.setState({createData: {}})
                            }}
                        />
                    </Col>
                    <Col md='10'>
                        <MenuButtons {...this.props} />
                    </Col>
                </Row>
                {list.length > 0 && <Box type='solid'>
                    <DataTable
                        className='table-bordered'
                        items={list}
                        map={[
                            {
                                title: 'ID',
                                field: 'id',
                                sort: 'id',
                            },
                            {
                                title: 'Номер',
                                field: 'number',
                                sort: 'number',
                            },
                            {
                                title: 'Начало выпуска',
                                field: 'started_at',
                                sort: 'started_at',
                            },
                            {
                                title: 'Конец выпуска',
                                field: 'completed_at',
                                sort: 'completed_at',
                            },
                            {
                                title: 'Добавлено',
                                field: 'created_at',
                                sort: 'created_at',
                            },
                        ]}
                        actions={[
                            {
                                className: 'fa fa-edit text-yellow',
                                onClick: (e, itemSeason) => {
                                    this.props.toggleEditModal(true)
                                    this.props.fetchData(itemSeason.id)
                                },
                            },
                            {
                                className: 'fa fa-trash text-red',
                                onClick: (e, itemSeason) => {
                                    if (confirm('Вы уверены что хотите удалить сезон серала?')) {
                                        this.props.deleteData(itemSeason.id, {...query, item_id: itemSeason.item_id})
                                    }
                                },
                            },
                        ]}
                        onSort={(e, sort) => {
                            this.props.locationPush(mergeQuery({sort: sort}))
                        }}
                        query={query}
                    />
                    <Pagination {...pagination} />
                </Box>}
                <Modal
                    title='Добавить сезон'
                    isOpen={this.props.itemSeason.isCreateModalOpen}
                    close={() => {
                        this.props.toggleCreateModal(false)
                    }}
                >
                    <ItemSeasonForm
                        data={this.state.createData}
                        errors={this.props.itemSeason.errors}
                        setData={(data) => {
                            this.setState({createData: {...this.state.createData, ...data}})
                        }}
                        saveData={(data) => {
                            this.props.saveData({...data, item_id: this.props.routeParams.id}, query)
                        }}
                    />
                </Modal>
                <Modal
                    title='Редактировать сезон'
                    isOpen={this.props.itemSeason.isEditModalOpen}
                    close={() => {
                        this.props.toggleEditModal(false)
                    }}
                >
                    <ItemSeasonForm
                        data={this.props.itemSeason.data}
                        errors={this.props.itemSeason.errors}
                        setData={this.props.setData}
                        saveData={(data) => {this.props.saveData(data, query)}}
                    />
                </Modal>
            </Content>}
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        toggleCreateModal: (value) => dispatch(ItemSeasonAction.toggleCreateModal(value)),
        toggleEditModal: (value) => dispatch(ItemSeasonAction.toggleEditModal(value)),
        fetchItemData: (id) => dispatch(ItemAction.requestData(id)),
        fetchList: (params) => dispatch(ItemSeasonAction.requestList(params)),
        fetchData: (id) => dispatch(ItemSeasonAction.requestData(id)),
        setData: (data) => dispatch(ItemSeasonAction.setData(data)),
        saveData: (data, params) => dispatch(ItemSeasonAction.saveData(data, params)),
        deleteData: (data, params) => dispatch(ItemSeasonAction.deleteData(data, params)),
        locationPush: (path) => dispatch(push(path)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemSeasons)
