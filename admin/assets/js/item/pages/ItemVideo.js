import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { get as lodashGet } from 'lodash'
import { Creators as ItemAction } from '../reducers/item'
import { Creators as ItemSeasonAction } from '../../item-season/reducers/item-season'
import { Creators as ItemSeriesAction } from '../../item-series/reducers/item-series'
import { Creators as ItemVideoAction } from '../../item-video/reducers/item-video'
import { Creators as AuthorAction } from '../../author/reducers/author'
import { Creators as SourceAction } from '../../source/reducers/source'
import { Creators as VideoTypeAction } from '../../video-type/reducers/video-type'
import { mergeQuery } from '../../common/helpers/url-helper'
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Row from '../../common/components/content/Row'
import Col from '../../common/components/content/Col'
import Box from '../../common/components/content/Box'
import Modal from '../../common/components/content/Modal'
import BtnApp from '../../common/components/content/BtnApp'
import DataTable from '../../common/components/table/DataTable'
import Pagination from '../../common/components/pagination/Pagination'
import ItemVideoForm from '../../item-video/components/ItemVideoForm'
import MenuButtons from '../components/MenuButtons'

export class ItemVideo extends Component {
    constructor (props) {
        super(props)
        this.state = {
            createData: {},
        }
    }

    componentWillMount() {
        this.fetchItem()
        this.fetchItemSeasons()
        this.fetchItemVideos()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.routing !== nextProps.routing) {
            this.fetchItem(this.props.routeParams.id)
            this.fetchItemSeasons()
            this.fetchItemVideos(lodashGet(nextProps, 'routing.locationBeforeTransitions.query'))
        }
        if (lodashGet(this.props, 'itemVideo.data.item_season_id') !== lodashGet(nextProps, 'itemVideo.data.item_season_id')) {
            this.props.fetchSeriesList({item_season_id: lodashGet(nextProps, 'itemVideo.data.item_season_id'), per_page: 0})
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if (lodashGet(this.state, 'createData.item_season_id') !== lodashGet(nextState, 'createData.item_season_id')) {
            this.props.fetchSeriesList({item_season_id: lodashGet(nextState, 'createData.item_season_id'), per_page: 0})
        }
    }

    fetchItem = () => {
        this.props.fetchItemData(this.props.routeParams.id)
    }

    fetchItemSeasons = () => {
        this.props.fetchSeasonList({
            item_id: this.props.routeParams.id,
            sort: 'number',
            per_page: 0,
        })
    }

    fetchItemVideos = (query) => {
        query = query || lodashGet(this.props, 'routing.locationBeforeTransitions.query', {})
        query = {...query, item_id: this.props.routeParams.id}
        this.props.fetchList(query)
    }

    setCreateData = (data) => {
        this.setState({createData: {...this.state.createData, ...data}})
    }

    breadcrumb = () => {
        return [
            {
                title: 'Сериалы',
                icon: 'fa-film',
                link: '/admin/items'
            },
            {
                title: `Сериал №${this.props.routeParams.id}`,
                link: `/admin/items/${this.props.routeParams.id}`
            },
            {
                title: 'Список видео',
            },
        ]
    }

    render () {
        const query = lodashGet(this.props, 'routing.locationBeforeTransitions.query', {})
        const pagination = lodashGet(this.props, 'itemVideo.list.meta', {})
        const seasonList = lodashGet(this.props, 'itemSeason.list.data', [])
        const seriesList = lodashGet(this.props, 'itemSeries.list.data', [])
        const authors = lodashGet(this.props, 'author.list.data', [])
        const totalAuthors = lodashGet(this.props, 'author.list.meta.total')
        const sources = lodashGet(this.props, 'source.list.data', [])
        const totalSources = lodashGet(this.props, 'source.list.meta.total')
        const videoTypes = lodashGet(this.props, 'videoType.list.data', [])
        const totalVideoTypes = lodashGet(this.props, 'videoType.list.meta.total')
        const list = lodashGet(this.props, 'itemVideo.list.data', [])
        const name = lodashGet(this.props, 'item.data.name_ru')
            || lodashGet(this.props, 'item.data.name_en')
            || lodashGet(this.props, 'item.data.name_original')
            || `Сериал №${this.props.routeParams.id}`

        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                {name} <small>Список видео</small>
            </ContentHeader>
            {this.props.item.data && <Content>
                <Row>
                    <Col md='2'>
                        <BtnApp
                            text='Добавить'
                            icon='fa-plus'
                            onClick={() => {
                                this.props.toggleCreateModal(true)
                                this.setState({createData: {}})
                            }}
                        />
                    </Col>
                    <Col md='10'>
                        <MenuButtons {...this.props} />
                    </Col>
                </Row>
                {list.length > 0 && <Box type='solid'>
                    <DataTable
                        className='table-bordered'
                        items={list}
                        map={[
                            {
                                title: 'ID',
                                field: 'id',
                                sort: 'id',
                            },
                            {
                                title: 'Сезон',
                                field: 'item_season.number',
                                sort: 'item_season.number',
                            },
                            {
                                title: 'Серия',
                                field: 'item_series.number',
                                sort: 'item_series.number',
                            },
                            {
                                title: 'Перевод',
                                field: 'video_type.name',
                                sort: 'video_type.name',
                            },
                            {
                                title: 'Автор перевода',
                                field: 'author.name',
                                sort: 'author.name',
                            },
                            {
                                title: 'Источник',
                                field: 'source.name',
                                sort: 'source.name',
                            },
                            {
                                title: 'Качество',
                                field: 'quality',
                                sort: 'quality',
                            },
                            {
                                title: 'Добавлено',
                                field: 'created_at',
                                sort: 'created_at',
                            },
                        ]}
                        actions={[
                            {
                                className: 'fa fa-eye text-aqua',
                                onClick: (e, itemVideo) => {
                                    this.props.toggleShowModal(true)
                                    this.props.fetchData(itemVideo.id)
                                },
                            },
                            {
                                className: 'fa fa-edit text-yellow',
                                onClick: (e, itemVideo) => {
                                    this.props.toggleEditModal(true)
                                    this.props.fetchData(itemVideo.id)
                                },
                            },
                            {
                                className: 'fa fa-trash text-red',
                                onClick: (e, itemVideo) => {
                                    if (confirm('Вы уверены что хотите удалить видео?')) {
                                        this.props.deleteData(itemVideo.id, {...query, item_id: this.props.routeParams.id})
                                    }
                                },
                            },
                        ]}
                        onSort={(e, sort) => {
                            this.props.locationPush(mergeQuery({sort: sort}))
                        }}
                        query={query}
                    />
                    <Pagination {...pagination} />
                </Box>}
                <Modal
                    title='Добавить видео'
                    isOpen={this.props.itemVideo.isCreateModalOpen}
                    close={() => {
                        this.props.toggleCreateModal(false)
                    }}
                >
                    <ItemVideoForm
                        data={this.state.createData}
                        errors={this.props.itemVideo.errors}
                        setData={(data) => {
                            this.setState({createData: {...this.state.createData, ...data}})
                        }}
                        saveData={(data) => {
                            this.props.saveData(
                                {...data, item_id: this.props.routeParams.id},
                                {...query, item_id: this.props.routeParams.id}
                            )
                        }}
                        seasons={seasonList}
                        series={seriesList}
                        {...{authors, totalAuthors}}
                        searchAuthors={(search) => {
                            this.props.filterAuthorList({name: search})
                        }}
                        createAuthor={(name) => {
                            this.props.addAuthor(
                                {
                                    name,
                                    success: (author) => {
                                        this.setState(
                                            {createData: {...this.state.createData, author, author_id: author.id}}
                                        )
                                    }
                                },
                                {name}
                            )
                        }}
                        {...{sources, totalSources}}
                        searchSources={(search) => {
                            this.props.filterSourceList({name: search})
                        }}
                        createSource={(name) => {
                            this.props.addSource(
                                {
                                    name,
                                    success: (source) => {
                                        this.setState(
                                            {createData: {...this.state.createData, source, source_id: source.id}}
                                        )
                                    }
                                },
                                {name}
                            )
                        }}
                        {...{videoTypes, totalVideoTypes}}
                        searchVideoTypes={(search) => {
                            this.props.filterVideoTypeList({name: search})
                        }}
                        createVideoType={(name) => {
                            this.props.addVideoType(
                                {
                                    name,
                                    success: (videoType) => {
                                        this.setState(
                                            {createData: {
                                                ...this.state.createData,
                                                video_type: videoType,
                                                video_type_id: videoType.id,
                                            }}
                                        )
                                    }
                                },
                                {name}
                            )
                        }}
                    />
                </Modal>
                <Modal
                    title='Редактировать видео'
                    isOpen={this.props.itemVideo.isEditModalOpen}
                    close={() => {
                        this.props.toggleEditModal(false)
                    }}
                >
                    <ItemVideoForm
                        data={this.props.itemVideo.data}
                        errors={this.props.itemVideo.errors}
                        setData={this.props.setData}
                        saveData={(data) => {
                            this.props.saveData(data, {...query, item_id: this.props.routeParams.id})}
                        }
                        seasons={seasonList}
                        series={seriesList}
                        {...{authors, totalAuthors}}
                        searchAuthors={(search) => {
                            this.props.filterAuthorList({name: search})
                        }}
                        createAuthor={(name) => {
                            this.props.addAuthor(
                                {
                                    name,
                                    success: (author) => {
                                        this.props.setData({author, author_id: author.id})
                                    }
                                },
                                {name}
                            )
                        }}
                        {...{sources, totalSources}}
                        searchSources={(search) => {
                            this.props.filterSourceList({name: search})
                        }}
                        createSource={(name) => {
                            this.props.addSource(
                                {
                                    name,
                                    success: (source) => {
                                        this.props.setData({source, source_id: source.id})
                                    }
                                },
                                {name}
                            )
                        }}
                        {...{videoTypes, totalVideoTypes}}
                        searchVideoTypes={(search) => {
                            this.props.filterVideoTypeList({name: search})
                        }}
                        createVideoType={(name) => {
                            this.props.addVideoType(
                                {
                                    name,
                                    success: (videoType) => {
                                        this.props.setData({video_type: videoType, video_type_id: videoType.id})
                                    }
                                },
                                {name}
                            )
                        }}
                    />
                </Modal>
                <Modal
                    title='Смотреть видео'
                    isOpen={this.props.itemVideo.isShowModalOpen}
                    close={() => {
                        this.props.toggleShowModal(false)
                    }}
                >
                    {this.props.itemVideo.data && <iframe
                        src={this.props.itemVideo.data.src}
                        frameBorder='0'
                        scrolling='no'
                        allowFullScreen
                        style={{width: '100%', height: '320px'}}
                    />}
                </Modal>
            </Content>}
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        toggleCreateModal: (value) => dispatch(ItemVideoAction.toggleCreateModal(value)),
        toggleEditModal: (value) => dispatch(ItemVideoAction.toggleEditModal(value)),
        toggleShowModal: (value) => dispatch(ItemVideoAction.toggleShowModal(value)),
        fetchItemData: (id) => dispatch(ItemAction.requestData(id)),
        fetchSeasonList: (params) => dispatch(ItemSeasonAction.requestList(params)),
        fetchSeriesList: (params) => dispatch(ItemSeriesAction.requestList(params)),
        filterAuthorList: (params) => dispatch(AuthorAction.applyListFilter(params)),
        filterSourceList: (params) => dispatch(SourceAction.applyListFilter(params)),
        filterVideoTypeList: (params) => dispatch(VideoTypeAction.applyListFilter(params)),
        fetchList: (params) => dispatch(ItemVideoAction.requestList(params)),
        fetchData: (id) => dispatch(ItemVideoAction.requestData(id)),
        setData: (data) => dispatch(ItemVideoAction.setData(data)),
        saveData: (data, params) => dispatch(ItemVideoAction.saveData(data, params)),
        deleteData: (data, params) => dispatch(ItemVideoAction.deleteData(data, params)),
        addAuthor: (data, params) => dispatch(ItemVideoAction.addAuthor(data, params)),
        addSource: (data, params) => dispatch(ItemVideoAction.addSource(data, params)),
        addVideoType: (data, params) => dispatch(ItemVideoAction.addVideoType(data, params)),
        locationPush: (path) => dispatch(push(path)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemVideo)
