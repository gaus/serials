import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { get as lodashGet } from 'lodash'
import { Creators as ItemAction } from '../reducers/item'
import { Creators as AttrAction } from '../../attr/reducers/attr'
import { Creators as AttrValueAction } from '../../attr/reducers/attr-value'
import NotFound from '../../common/pages/NotFound'
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Row from '../../common/components/content/Row'
import Col from '../../common/components/content/Col'
import Form from '../components/Form'
import AttrsForm from '../components/AttrsForm'
import MenuButtons from '../components/MenuButtons'

export class ItemEdit extends Component {
    componentWillMount() {
        this.fetchItem()
        this.props.fetchAttrList()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.routing !== nextProps.routing) {
            this.fetchItem(this.props.routeParams.id)
        }
    }

    fetchItem = () => {
        this.props.fetchData(this.props.routeParams.id)
    }

    breadcrumb = () => {
        return [
            {
                title: 'Сериалы',
                icon: 'fa-film',
                link: '/admin/items'
            },
            {
                title: 'Редактировать',
            },
        ]
    }

    render () {
        const attrs = lodashGet(this.props, 'attr.list.data', [])
        const attrValues = lodashGet(this.props, 'item.data.attr_values', [])
        const searchAttrValues = lodashGet(this.props, 'attrValue.list.data', [])
        const itemId = lodashGet(this.props, 'item.data.id')

        if (!itemId && !this.props.item.isFetching) {
            return <NotFound />
        }

        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                Сериалы <small>Редактировать</small>
            </ContentHeader>
            {this.props.item.data && <Content>
                <MenuButtons {...this.props} />
                <Row>
                    <Col md='8'>
                        <Form
                            data={this.props.item.data}
                            errors={this.props.item.errors}
                            setData={this.props.setData}
                            saveData={this.props.saveData}
                        />
                    </Col>
                    <Col md='4'>
                        <AttrsForm
                            attrs={attrs}
                            values={attrValues}
                            searchItems={searchAttrValues}
                            search={(params) => this.props.fetchAttrValueList(params)}
                            create={(data) => this.props.createAttrValue({...data, item_id: itemId})}
                            add={(data) => this.props.addItemAttr({...data, item_id: itemId})}
                            remove={(data) => this.props.removeItemAttr({...data, item_id: itemId})}
                        />
                    </Col>
                </Row>
            </Content>}
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchData: (id) => dispatch(ItemAction.requestData(id)),
        setData: (data) => dispatch(ItemAction.setData(data)),
        saveData: (data) => dispatch(ItemAction.saveData(data)),
        locationPush: (path) => dispatch(push(path)),
        fetchAttrList: () => dispatch(AttrAction.requestList()),
        fetchAttrValueList: (params) => dispatch(AttrValueAction.applyListFilter(params)),
        createAttrValue: (data) => dispatch(ItemAction.createAttrValue(data)),
        addItemAttr: (data) => dispatch(ItemAction.addItemAttr(data)),
        removeItemAttr: (data) => dispatch(ItemAction.removeItemAttr(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemEdit)
