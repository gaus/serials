import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Creators as ItemAction } from '../reducers/item'
import Content from '../../common/components/content/Content'
import ContentHeader from '../../common/components/content/ContentHeader'
import Row from '../../common/components/content/Row'
import Col from '../../common/components/content/Col'
import Form from '../components/Form'

export class ItemAdd extends Component {
    constructor() {
        super()
        this.state = {
            data: {}
        }
    }

    breadcrumb = () => {
        return [
            {
                title: 'Сериалы',
                icon: 'fa-film',
                link: '/admin/items'
            },
            {
                title: 'Добавить',
            },
        ]
    }

    render () {
        return <div>
            <ContentHeader breadcrumb={this.breadcrumb()}>
                Сериалы <small>Добавить</small>
            </ContentHeader>
            <Content>
                <Row>
                    <Col md='8'>
                        <Form
                            data={this.state.data}
                            errors={this.props.item.errors}
                            setData={(data) => {
                                this.setState({data: {...this.state.data, ...data}})
                            }}
                            saveData={this.props.saveData}
                        />
                    </Col>
                    <Col md='4'>

                    </Col>
                </Row>
            </Content>
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        saveData: (data) => dispatch(ItemAction.saveData(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemAdd)
