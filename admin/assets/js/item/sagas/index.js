import { takeLatest } from 'redux-saga/effects'
import Api from '../../common/services/api'
import { Types as ItemTypes } from '../reducers/item'
import * as item from './item'

const api = Api.create()

export default function * sagas () {
    yield [
        takeLatest(ItemTypes.REQUEST_LIST, item.getList, api),
        takeLatest(ItemTypes.REQUEST_DATA, item.getData, api),
        takeLatest(ItemTypes.SAVE_DATA, item.saveData, api),
        takeLatest(ItemTypes.DELETE_DATA, item.deleteData, api),
        takeLatest(ItemTypes.CREATE_ATTR_VALUE, item.createAttrValue, api),
        takeLatest(ItemTypes.ADD_ITEM_ATTR, item.addItemAttr, api),
        takeLatest(ItemTypes.REMOVE_ITEM_ATTR, item.removeItemAttr, api),
    ]
}
