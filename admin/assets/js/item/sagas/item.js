import { call, put } from 'redux-saga/effects'
import { push } from 'react-router-redux'
import { Creators as Action } from '../reducers/item'
import { Creators as ActionInformer } from '../../common/reducers/informer'
import { Creators as ActionPreloader } from '../../common/reducers/preloader'

export function * getList (api, {params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getItems, params)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successList(response.data))
    } else {
        yield put(Action.failList())
    }
}

export function * getData (api, {id}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getItem, id, {scenario: 'edit'})
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successData(response.data.data))
    } else {
        yield put(Action.failData())
    }
}

export function * saveData (api, {data}) {
    yield put(ActionPreloader.start())
    const response = data.id
        ? yield call(api.putItem, data.id, data, {scenario: 'edit'})
        : yield call(api.postItem, data)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successSaveData(response.data.data))
        if (!data.id) {
            yield put(push('/admin/items/' + response.data.data.id))
        }
        data.id
            ? yield put(ActionInformer.push('success', 'Изменения сохранены'))
            : yield put(ActionInformer.push('success', 'Сериал добавлен'))
    } else {
        yield put(Action.failSaveData(response.data.errors))
        yield put(ActionInformer.push('danger', 'Вы ввели некорректные данные'))
    }
}

export function * deleteData (api, {id, params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.deleteItem, id)
    yield put(ActionPreloader.stop())
    if (response.status === 204) {
        yield put(Action.successDeleteData())
        yield put(Action.requestList(params))
        yield put(ActionInformer.push('success', 'Сериал удален'))
    } else {
        yield put(Action.failDeleteData())
    }
}

export function * createAttrValue (api, {data}) {
    const createData = {
        attr_id: data.attr_id,
        value: data.value,
    }
    yield put(ActionPreloader.start())
    const response = yield call(api.postAttrValue, createData)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successCreateAttrValue())
        yield put(Action.addItemAttr({
            attr_id: response.data.data.attr_id,
            attr_value_id: response.data.data.id,
            item_id: data.item_id,
        }))
    } else {
        yield put(Action.failCreateAttrValue())
        yield put(ActionInformer.push('danger', 'Вы ввели некорректные данные'))
    }
}

export function * addItemAttr (api, {data}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.postItemAttr, data)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successAddItemAttr())
        yield put(Action.requestData(data.item_id))
    } else {
        yield put(Action.failAddItemAttr())
    }
}

export function * removeItemAttr (api, {data}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.deleteItemAttr, `${data.item_id},${data.attr_id},${data.attr_value_id}`)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successRemoveItemAttr())
        yield put(Action.requestData(data.item_id))
    } else {
        yield put(Action.failRemoveItemAttr())
    }
}
