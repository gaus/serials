import React from 'react'

export default ({item = {}}) => {
    const info = []
    const names = []
    item.name_ru && names.push(item.name_ru)
    item.name_en && names.push(item.name_en)
    item.name_original && names.push(item.name_original)
    item.year_start && info.push(item.year_start)
    if (item.attrs && item.attrs.country) {
        for (let key in item.attrs.country.values) {
            info.push(item.attrs.country.values[key].value)
        }
    }

    return <div className='serial-select-item'>
        {item.image_src &&
            <span className='serial-select-item__image'><img src={item.image_src} /></span>
        }
        <span className='serial-select-item__info'>
            <div>{names.join(' / ')}</div>
            {info.length > 0 &&
                <div className='text-muted'>{info.join(', ')}</div>
            }
        </span>
    </div>
}
