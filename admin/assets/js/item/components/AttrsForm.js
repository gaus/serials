import React from 'react'
import Box from '../../common/components/content/Box'
import OptionList from '../../common/components/form/OptionList'

export default ({
    attrs,
    values,
    searchItems,
    search,
    create,
    add,
    remove,
}) => {
    return <Box type='solid'>
        {attrs.map((attr) => {
            return <OptionList
                key={attr.id}
                label={attr.name}
                items={values.filter((value) => value.attr_id === attr.id)}
                nameField='value'
                valueField='id'
                searchItems={searchItems && searchItems.length && searchItems[0].attr_id === attr.id ? searchItems : undefined}
                search={(value) => {
                    value && search({attr_id: attr.id, value})
                }}
                create={(value) => {
                    create({value, attr_id: attr.id})
                }}
                add={(id) => {
                    add({attr_id: attr.id, attr_value_id: id})
                }}
                remove={(id) => {
                    remove({attr_id: attr.id, attr_value_id: id})
                }}
            />
        })}
    </Box>
}
