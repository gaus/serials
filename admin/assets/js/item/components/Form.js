import React from 'react'
import FormField from '../../common/components/form/FormField'
import Button from '../../common/components/form/Button'
import Image from '../../common/components/form/Image'
import Box from '../../common/components/content/Box'
import Row from '../../common/components/content/Row'
import Col from '../../common/components/content/Col'
import SimpleData from '../../common/components/data/SimpleData'

export default ({
    data = {},
    errors = {},
    setData,
    saveData,
}) => {
    return <Box type='solid'>
        <form
            onSubmit={(e) => {
                e.preventDefault()
                saveData(data)
            }}
        >
            <Row>
                <Col md='4'>
                    <Image
                        maxHeight='334px'
                        maxWidth='100%'
                        src={data.image_src}
                        label='Изображение'
                        name='image'
                        srcField='image_src'
                        setData={setData}
                    />
                    {data.id && <SimpleData
                        data={data}
                        map={{
                            id: 'ID',
                            created_at: 'Добавлен',
                            updated_at: 'Изменен',
                        }}
                    />}
                </Col>
                <Col md='8'>
                    <FormField
                        type='text'
                        name='name_ru'
                        value={data.name_ru}
                        label='Название на русском'
                        error={errors.name_ru && errors.name_ru.join(' ')}
                        setData={setData}
                    />
                    <FormField
                        type='text'
                        name='name_en'
                        value={data.name_en}
                        label='Название на английском'
                        error={errors.name_en && errors.name_en.join(' ')}
                        setData={setData}
                    />
                    <FormField
                        type='text'
                        name='name_original'
                        value={data.name_original}
                        label='Оригинальное название'
                        error={errors.name_original && errors.name_original.join(' ')}
                        setData={setData}
                    />
                    <FormField
                        type='select'
                        name='category_id'
                        value={data.category_id}
                        label='Категория'
                        withEmpty
                        error={errors.category_id && errors.category_id.join(' ')}
                        setData={setData}
                        list={[
                            {value: 1, name: 'Сериалы'},
                            {value: 2, name: 'Дорамы'},
                            {value: 3, name: 'Мультфильмы'},
                            {value: 4, name: 'Аниме'},
                        ]}
                    />
                    <FormField
                        type='textarea'
                        name='description'
                        value={data.description}
                        label='Описание'
                        error={errors.description && errors.description.join(' ')}
                        setData={setData}
                        rows='5'
                    />

                    <Row>
                        <Col md='6' sm='6' xs='6'>
                            <FormField
                                type='number'
                                name='year_start'
                                value={data.year_start}
                                label='Год выпуска'
                                error={errors.year_start && errors.year_start.join(' ')}
                                setData={setData}
                            />
                        </Col>
                        <Col md='6' sm='6' xs='6'>
                            <FormField
                                type='number'
                                name='year_complete'
                                value={data.year_complete}
                                label='Год завершения'
                                error={errors.year_complete && errors.year_complete.join(' ')}
                                setData={setData}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md='6' sm='6' xs='6'>
                            <FormField
                                type='date'
                                name='started_at'
                                value={data.started_at}
                                label='Дата выпуска'
                                error={errors.started_at && errors.started_at.join(' ')}
                                setData={setData}
                            />
                        </Col>
                        <Col md='6' sm='6' xs='6'>
                            <FormField
                                type='date'
                                name='completed_at'
                                value={data.completed_at}
                                label='Дата завершения'
                                error={errors.completed_at && errors.completed_at.join(' ')}
                                setData={setData}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md='6' sm='6' xs='6'>
                            <FormField
                                type='number'
                                name='duration'
                                value={data.duration}
                                label='Продолжительность, мин.'
                                error={errors.duration && errors.duration.join(' ')}
                                setData={setData}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md='4' sm='4' xs='4'>
                            <FormField
                                type='number'
                                step='0.1'
                                disabled
                                name='rating'
                                value={data.rating}
                                label='Рэйтинг'
                                error={errors.rating && errors.rating.join(' ')}
                                setData={setData}
                            />
                        </Col>
                        <Col md='4' sm='4' xs='4'>
                            <FormField
                                type='number'
                                step='0.1'
                                name='rating_kp'
                                value={data.rating_kp}
                                label='Кинопоиск'
                                error={errors.rating_kp && errors.rating_kp.join(' ')}
                                setData={setData}
                            />
                        </Col>
                        <Col md='4' sm='4' xs='4'>
                            <FormField
                                type='number'
                                step='0.1'
                                name='rating_imdb'
                                value={data.rating_imdb}
                                label='IMDB'
                                error={errors.rating_imdb && errors.rating_imdb.join(' ')}
                                setData={setData}
                            />
                        </Col>
                    </Row>
                    <FormField
                        type='checkbox'
                        name='published'
                        value={data.published}
                        label='Опубликовано'
                        error={errors.published && errors.published.join(' ')}
                        setData={setData}
                    />
                </Col>
            </Row>

            <Button type='submit' className='btn-primary' text='Сохранить' />
        </form>
    </Box>
}
