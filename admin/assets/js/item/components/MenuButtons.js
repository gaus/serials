import React from 'react'
import BtnApp from '../../common/components/content/BtnApp'

export default ({
    item: {data: {id, season_count, series_count}},
    router: {isActive, push}
}) => {
    return <div className='btn-app-block'>
        <BtnApp
            text='Редактировать'
            icon='fa-edit'
            onClick={() => {push(`/admin/items/${id}`)}}
            active={isActive(`/admin/items/${id}`)}
        />
        <BtnApp
            text='Сезоны'
            icon='fa-th-list'
            onClick={() => {push(`/admin/items/${id}/seasons`)}}
            active={isActive(`/admin/items/${id}/seasons`)}
            badge={season_count}
            badgeColor='light-blue'
        />
        <BtnApp
            text='Серии'
            icon='fa-list'
            onClick={() => {push(`/admin/items/${id}/series`)}}
            active={isActive(`/admin/items/${id}/series`)}
            badge={series_count}
            badgeColor='light-blue'
        />
        <BtnApp
            text='Видео'
            icon='fa-youtube-play'
            onClick={() => {push(`/admin/items/${id}/videos`)}}
            active={isActive(`/admin/items/${id}/videos`)}
        />
        <BtnApp
            text='Парсинг'
            icon='fa-database'
            onClick={() => {push(`/admin/items/${id}/parsing`)}}
            active={isActive(`/admin/items/${id}/parsing`)}
        />
        <BtnApp
            text='Удалить'
            icon='fa-trash'
        />
    </div>
}
