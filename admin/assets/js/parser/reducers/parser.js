import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
    requestList: ['params'],
    successList: ['data'],
    failList: [],
    requestData: ['id'],
    successData: ['data'],
    failData: [],
    setData: ['data'],
    saveData: ['data', 'params'],
    successSaveData: ['data'],
    failSaveData: ['errors'],
    deleteData: ['id', 'params'],
    successDeleteData: [],
    failDeleteData: [],
    resetData: [],
}, { prefix: 'parser/' })

export const INITIAL_STATE = {
    isFetching: false,
    list: [],
    data: undefined,
    errors: undefined,
}

export const requestList = (state) => {
    return {...state, isFetching: true}
}

export const successList = (state, { data }) => {
    return {...state, ...{list: data}, isFetching: false}
}

export const failList = (state) => {
    return {...state, isFetching: false}
}

export const requestData = (state) => {
    return {...state, isFetching: true}
}

export const successData = (state, { data }) => {
    return {...state, ...{data}, isFetching: false}
}

export const failData = (state) => {
    return {...state, isFetching: false}
}

export const setData = (state, {data}) => {
    return {...state, data: {...state.data, ...data}}
}

export const saveData = (state) => {
    return {...state, isFetching: true}
}

export const successSaveData = (state, { data }) => {
    return {...state, ...{data}, errors: undefined, isFetching: false}
}

export const failSaveData = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const deleteData = (state) => {
    return {...state, isFetching: true}
}

export const successDeleteData = (state) => {
    return {...state, isFetching: false}
}

export const failDeleteData = (state) => {
    return {...state, isFetching: false}
}

export const resetData = (state) => {
    return {...state, data: undefined, isFetching: false}
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.REQUEST_LIST]: requestList,
    [Types.SUCCESS_LIST]: successList,
    [Types.FAIL_LIST]: failList,
    [Types.REQUEST_DATA]: requestData,
    [Types.SUCCESS_DATA]: successData,
    [Types.FAIL_DATA]: failData,
    [Types.SET_DATA]: setData,
    [Types.SAVE_DATA]: saveData,
    [Types.SUCCESS_SAVE_DATA]: successSaveData,
    [Types.FAIL_SAVE_DATA]: failSaveData,
    [Types.DELETE_DATA]: deleteData,
    [Types.SUCCESS_DELETE_DATA]: successDeleteData,
    [Types.FAIL_DELETE_DATA]: failDeleteData,
    [Types.RESET_DATA]: resetData,
})
