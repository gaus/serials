import { takeLatest } from 'redux-saga/effects'
import Api from '../../common/services/api'
import { Types as ParserTypes } from '../reducers/parser'
import * as parser from './parser'

const api = Api.create()

export default function * sagas () {
    yield [
        takeLatest(ParserTypes.REQUEST_LIST, parser.getList, api),
        takeLatest(ParserTypes.REQUEST_DATA, parser.getData, api),
        takeLatest(ParserTypes.SAVE_DATA, parser.saveData, api),
        takeLatest(ParserTypes.DELETE_DATA, parser.deleteData, api),
    ]
}
