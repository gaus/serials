import { combineReducers } from 'redux'
import commonReducers from './common/reducers/index'
import userReducers from './user/reducers/index'
import itemReducers from './item/reducers/index'
import attrReducers from './attr/reducers/index'
import ItemSeasonReducers from './item-season/reducers/index'
import ItemSeriesReducers from './item-series/reducers/index'
import ItemVideoReducers from './item-video/reducers/index'
import AuthorReducers from './author/reducers/index'
import SourceReducers from './source/reducers/index'
import VideoTypeReducers from './video-type/reducers/index'
import ParserItemReducers from './parser-item/reducers/index'
import ParserReducers from './parser/reducers/index'

const allReducers = Object.assign(
    {},
    commonReducers,
    userReducers,
    itemReducers,
    attrReducers,
    ItemSeasonReducers,
    ItemSeriesReducers,
    ItemVideoReducers,
    AuthorReducers,
    SourceReducers,
    VideoTypeReducers,
    ParserItemReducers,
    ParserReducers,
);

export default combineReducers(allReducers)
