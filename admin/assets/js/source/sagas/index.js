import { takeLatest } from 'redux-saga/effects'
import Api from '../../common/services/api'
import { Types as SourceTypes } from '../reducers/source'
import * as source from './source'

const api = Api.create()

export default function * sagas () {
    yield [
        takeLatest(SourceTypes.REQUEST_LIST, source.getList, api),
        takeLatest(SourceTypes.APPLY_LIST_FILTER, source.applyFilter),
    ]
}
