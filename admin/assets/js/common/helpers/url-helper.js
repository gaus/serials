import URL from 'url-parse'
import isUndefined from 'lodash/isUndefined'
import { stringify } from 'querystringify'

export const mergeQuery = (query) => {
    const url = new URL(window.location.pathname + (window.location.search || ''), null, true)
    const currentQuery = {...url.query}
    for (let key in query) {
        if (isUndefined(query[key])) {
            delete currentQuery[key]
        } else {
            currentQuery[key] = query[key]
        }
    }
    url.set('query', currentQuery)
    return url.toString().replace(url.origin, '')
}

export const queryString = (query, prefix = true) => {
    return stringify(query, prefix)
}
