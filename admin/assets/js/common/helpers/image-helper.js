import isFunction from 'lodash/isFunction'

export const renderImage = (file, callback) =>{
    const reader = new FileReader();
    reader.onload = function(event){
        callback(event.target.result);
    };
    reader.readAsDataURL(file);
}
