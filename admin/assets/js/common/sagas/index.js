import { takeEvery } from 'redux-saga/effects'
import { Types as InformerTypes } from '../reducers/informer'
import { Types as PreloaderTypes } from '../reducers/preloader'
import * as informer from './informer'
import * as preloader from './preloader'

export default function * sagas () {
    yield [
        takeEvery(InformerTypes.PUSH, informer.informer),
        takeEvery(InformerTypes.CLOSE, informer.close),
        takeEvery(PreloaderTypes.STOP, preloader.stop),
    ]
}
