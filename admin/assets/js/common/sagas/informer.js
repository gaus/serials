import { put, call } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { Creators as Action } from '../reducers/informer'

export function * informer ({ id, duration = 3000 }) {
    if (duration > 0) {
        yield call(delay, duration)
        yield put(Action.close(id))
    }
}

export function * close ({ id }) {
    yield call(delay, 400)
    yield put(Action.remove(id))
}
