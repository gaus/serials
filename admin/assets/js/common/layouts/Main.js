import React, { Component } from 'react'
import { connect } from 'react-redux'
import Header from '../components/header/Header'
import Footer from '../components/Footer'
import Sidebar from '../components/sidebar/Sidebar'
import Informers from '../components/informer/Informers'
import MainPreloader from '../components/preloader/MainPreloader'
import { Creators as MainMenuAction } from '../reducers/main-menu'
import { Creators as InformerAction } from '../reducers/informer'

export class Main extends Component {
    constructor (props) {
        super(props)
        this.state = {minHeight: 0}
    }

    componentWillMount () {
        window.addEventListener('resize', this.calculateMinHeight)
    }

    componentWillUnmount () {
        window.removeEventListener('resize', this.calculateMinHeight)
    }

    componentDidMount () {
        this.calculateMinHeight()
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.mainMenu !== nextProps.mainMenu) {
            this.calculateMinHeight()
        }
    }

    toggleSidebar = () => {
        const classes = window.document.body.className.split(/\s+/)
        const key = classes.indexOf('sidebar-collapse')
        if (key !== -1) {
            delete classes[key]
        } else {
            classes.push('sidebar-collapse');
        }
        window.document.body.className = classes.join(' ')
        this.calculateMinHeight()
    }

    calculateMinHeight = () => {
        let minHeight = window.innerHeight
        let sidebarHeight = 0
        const header = window.document.getElementsByClassName('main-header')
        const footer = window.document.getElementsByClassName('main-footer')
        const sidebar = window.document.getElementsByClassName('sidebar')
        if (sidebar.length) {
            sidebarHeight = sidebar[0].offsetHeight
        }
        minHeight = Math.max(minHeight, sidebarHeight)
        if (header.length) {
            minHeight -= header[0].offsetHeight
        }
        if (footer.length) {
            minHeight -= footer[0].offsetHeight
        }
        this.setState({minHeight})
    }

    render () {
        return <div className="wrapper">
            <Header toggleSidebar={this.toggleSidebar} />
            <Sidebar {...this.props} />
            <div className="content-wrapper" style={{minHeight: this.state.minHeight}}>
                {this.props.children}
            </div>
            <Footer />
            <Informers informers={this.props.informer.list} onClose={this.props.informerClose} />
            <MainPreloader {...this.props.preloader} />
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        mainMenuToggle: (path) => dispatch(MainMenuAction.toggle(path)),
        informerClose: (id) => dispatch(InformerAction.close(id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)
