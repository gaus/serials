import apisauce from 'apisauce'
import qs from 'querystringify'
import { isBoolean } from 'lodash'

const toFormData = (data) => {
    const formData = new FormData
    for (let key in data) {
        let value = data[key]
        if (null === data[key]) {
            value = ''
        }
        if (isBoolean(data[key])) {
            value = data[key] ? '1' : '0'
        }
        formData.append(key,value)
    }
    return formData
}

const create = () => {
    const api = apisauce.create({
        baseURL: '/admin/api/',
        headers: {
            'Accept': 'application/json',
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/json; charset=UTF-8',
        },
        timeout: 20000,
    })
    const getUsers = (params) => api.get('user', params)
    const getUser = (id) => api.get(`user/${id}`)
    const putUser = (id, data) => api.put(`user/${id}`, data)
    const postUser = (data) => api.post('user', data)
    const deleteUser = (id) => api.delete(`user/${id}`)
    const getItems = (params) => api.get('item', params)
    const getItem = (id, params) => api.get(`item/${id}`, params)
    const putItem = (id, data, params) => api.post(`item/${id}${qs.stringify(params, true)}`, toFormData({...data, '_method': 'put'}))
    const postItem = (data) => api.post('item', toFormData(data))
    const deleteItem = (id) => api.delete(`item/${id}`)
    const getAttrs = (params) => api.get('attr', params)
    const getAttr = (id) => api.get(`attr/${id}`)
    const putAttr = (id, data) => api.put(`attr/${id}`, data)
    const postAttr = (data) => api.post('attr', data)
    const deleteAttr = (id) => api.delete(`attr/${id}`)
    const getAttrValues = (params) => api.get('attr-value', params)
    const getAttrValue = (id) => api.get(`attr-value/${id}`)
    const putAttrValue = (id, data) => api.put(`attr-value/${id}`, data)
    const postAttrValue = (data) => api.post('attr-value', data)
    const deleteAttrValue = (id) => api.delete(`attr-value/${id}`)
    const getItemAttrs = (params) => api.get('item-attr', params)
    const getItemAttr = (id) => api.get(`item-attr/${id}`)
    const putItemAttr = (id, data) => api.put(`item-attr/${id}`, data)
    const postItemAttr = (data) => api.post('item-attr', data)
    const deleteItemAttr = (id) => api.delete(`item-attr/${id}`)
    const getItemSeasons = (params) => api.get('item-season', params)
    const getItemSeason = (id) => api.get(`item-season/${id}`)
    const putItemSeason = (id, data) => api.put(`item-season/${id}`, data)
    const postItemSeason = (data) => api.post('item-season', data)
    const deleteItemSeason = (id) => api.delete(`item-season/${id}`)
    const getItemSeries = (params) => api.get('item-series', params)
    const getItemSerie = (id) => api.get(`item-series/${id}`)
    const putItemSerie = (id, data) => api.put(`item-series/${id}`, data)
    const postItemSerie = (data) => api.post('item-series', data)
    const deleteItemSerie = (id) => api.delete(`item-series/${id}`)
    const getItemVideos = (params) => api.get('item-video', params)
    const getItemVideo = (id) => api.get(`item-video/${id}`)
    const putItemVideo = (id, data) => api.put(`item-video/${id}`, data)
    const postItemVideo = (data) => api.post('item-video', data)
    const deleteItemVideo = (id) => api.delete(`item-video/${id}`)
    const getAuthors = (params) => api.get('author', params)
    const getAuthor = (id) => api.get(`author/${id}`)
    const putAuthor = (id, data) => api.put(`author/${id}`, data)
    const postAuthor = (data) => api.post('author', data)
    const deleteAuthor = (id) => api.delete(`author/${id}`)
    const getSources = (params) => api.get('source', params)
    const getSource = (id) => api.get(`source/${id}`)
    const putSource = (id, data) => api.put(`source/${id}`, data)
    const postSource = (data) => api.post('source', data)
    const deleteSource = (id) => api.delete(`source/${id}`)
    const getVideoTypes = (params) => api.get('video-type', params)
    const getVideoType = (id) => api.get(`video-type/${id}`)
    const putVideoType = (id, data) => api.put(`video-type/${id}`, data)
    const postVideoType = (data) => api.post('video-type', data)
    const deleteVideoType = (id) => api.delete(`video-type/${id}`)
    const getParserItems = (params) => api.get('parser-item', params)
    const getParserItem = (id, params) => api.get(`parser-item/${id}`, params)
    const putParserItem = (id, data, params) => api.put(`parser-item/${id}${qs.stringify(params, true)}`, data)
    const postParserItem = (data, params) => api.post(`parser-item${qs.stringify(params, true)}`, data)
    const deleteParserItem = (id) => api.delete(`parser-item/${id}`)
    const getParsers = (params) => api.get('parser', params)
    const getParser = (id, params) => api.get(`parser/${id}`, params)
    const putParser = (id, data) => api.put(`parser/${id}`, data)
    const postParser = (data) => api.post('parser', data)
    const deleteParser = (id) => api.delete(`parser/${id}`)

    return {
        getUsers,
        getUser,
        putUser,
        postUser,
        deleteUser,
        getItems,
        getItem,
        putItem,
        postItem,
        deleteItem,
        getAttrs,
        getAttr,
        putAttr,
        postAttr,
        deleteAttr,
        getAttrValues,
        getAttrValue,
        putAttrValue,
        postAttrValue,
        deleteAttrValue,
        getItemAttrs,
        getItemAttr,
        putItemAttr,
        postItemAttr,
        deleteItemAttr,
        getItemSeasons,
        getItemSeason,
        putItemSeason,
        postItemSeason,
        deleteItemSeason,
        getItemSeries,
        getItemSerie,
        putItemSerie,
        postItemSerie,
        deleteItemSerie,
        getItemVideos,
        getItemVideo,
        putItemVideo,
        postItemVideo,
        deleteItemVideo,
        getAuthors,
        getAuthor,
        putAuthor,
        postAuthor,
        deleteAuthor,
        getSources,
        getSource,
        putSource,
        postSource,
        deleteSource,
        getVideoTypes,
        getVideoType,
        putVideoType,
        postVideoType,
        deleteVideoType,
        getParserItems,
        getParserItem,
        putParserItem,
        postParserItem,
        deleteParserItem,
        getParsers,
        getParser,
        putParser,
        postParser,
        deleteParser,
        _axiosInstance: api.axiosInstance,
    }
}

export default { create }
