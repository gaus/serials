import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
    push: ['status', 'title', 'description', 'duration'],
    close: ['id'],
    remove: ['id'],
}, { prefix: 'informer/' })

export const INITIAL_STATE = {
    nextId: 0,
    list: [],
}

export const push = (state, data) => {
    let newState = {...state}
    data.id = newState.nextId
    newState.nextId++
    newState.list.push({...data})
    return newState
}

export const close = (state, { id }) => {
    let newState = {...state}
    newState.list.map(item => {
        if (item.id === id) {
            item.close = true
        }
        return item
    })
    return newState
}

export const remove = (state, { id }) => {
    let newState = {...state}
    newState.list = newState.list.filter(item => item.id !== id)
    return newState
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.PUSH]: push,
    [Types.CLOSE]: close,
    [Types.REMOVE]: remove,
})
