import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
    toggle: ['path'],
}, { prefix: 'main-menu/' })

export const INITIAL_STATE = {
    title: 'Главное меню',
    items: [
        {
            path: '1',
            icon: 'fa-dashboard',
            name: 'Главная',
            link: '/admin',
        },
        {
            path: '2',
            icon: 'fa-users',
            name: 'Пользователи',
            link: '/admin/users',
        },
        {
            path: '3',
            icon: 'fa-film',
            name: 'Сериалы',
            link: '/admin/items',
        },
        {
            path: '4',
            icon: 'fa-recycle',
            name: 'Парсинг',
            items: [
                {
                    path: '4.1',
                    icon: 'fa-reorder',
                    name: 'Страницы парсинга',
                    link: '/admin/parser-items',
                },
            ],
        },
    ],
}

export const toggle = (state, { path }) => {
    const newState = {...state}
    newState.items.forEach((item) => {
        setOpen(item, path)
    })
    return newState
}

const setOpen = (item, path) => {
    if (item.path === path || path.indexOf(item.path + '.') === 0) {
        if (item.path === path && item.open === true) {
            delete item.open
        } else {
            item.open = true
        }
    } else {
        delete item.open
    }
    if (item.items && item.items.length) {
        item.items.forEach((subItem) => {
            setOpen(subItem, path)
        })
    }
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.TOGGLE]: toggle,
})
