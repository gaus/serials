// import { resettableReducer } from 'reduxsauce'
import { routerReducer } from 'react-router-redux'

// const resettable = resettableReducer('RESET')

export default {
    routing: routerReducer,
    mainMenu: require('./main-menu').reducer,
    informer: require('./informer').reducer,
    preloader: require('./preloader').reducer,
}
