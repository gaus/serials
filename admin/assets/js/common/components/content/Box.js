import React from 'react'

export default ({
    children,
    title,
    type,
}) => {
    const classes = ['box']
    type && classes.push('box-' + type)
    return <div className={classes.join(' ')}>
        {title && <div className='box-header'><h3 className='box-title'>{title}</h3></div>}
        <div className='box-body'>{children}</div>
    </div>
}
