import React from 'react'
import { Link } from 'react-router'

export default ({items}) => {
    const last = items.pop()
    return <ol className="breadcrumb">
        <li>
            <Link to="/admin">
                <i className='fa fa-dashboard' /> Главная
            </Link>
        </li>
        {items.map((item, i) =>
            <li key={i}>
                <Link to={item.link}>
                    {item.icon && <i className={'fa ' + item.icon} />} {item.title}
                </Link>
            </li>
        )}
        <li className="active">
            {last.icon && <i className={'fa ' + last.icon} />} {last.title}
        </li>
    </ol>
}
