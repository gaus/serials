import React from 'react'

export default ({className, children, ...props}) => {
    const classes = ['row']
    className && classes.push(className)
    return <div className={classes.join(' ')} {...props} >
        {children}
    </div>
}
