import React, { Component } from 'react'
import { isUndefined } from 'lodash'

export default class Tabs extends Component {
    constructor (props) {
        super(props)
        this.state = {
            active: undefined,
        };
    }

    activate = (index) => {
        this.setState({
            active: index,
        })
    }

    render () {
        const {
            tabs = [],
            defaultTab = 0,
        } = this.props
        const activeTab = isUndefined(this.state.active)
            ? defaultTab
            : this.state.active
        return <div className='nav-tabs-custom'>
            <ul className='nav nav-tabs'>
                {tabs.map((tab, i) => {
                    return <li
                        key={i}
                        className={activeTab === i ? 'active' : ''}
                        onClick={() => this.activate(i)}
                    >
                        <a href='#' onClick={(e) => e.preventDefault()}>{tab.name}</a>
                    </li>
                })}
            </ul>
            <div className='tab-content'>
                {tabs.map((tab, i) => {
                    const classes = ['tab-pane']
                    activeTab === i && classes.push('active')

                    return <div key={i} className={classes.join(' ')}>
                        {tab.content}
                    </div>
                })}
            </div>
        </div>
    }
}
