import React from 'react'

export default ({
    className,
    children,
    xs,
    sm,
    md,
    lg,
    ...props
}) => {
    const classes = []
    xs && classes.push(`col-xs-${xs}`)
    sm && classes.push(`col-sm-${sm}`)
    md && classes.push(`col-md-${md}`)
    lg && classes.push(`col-lg-${lg}`)
    className && classes.push(className)
    return <div className={classes.join(' ')} {...props} >
        {children}
    </div>
}
