import React, { Component } from 'react'

export default class Modal extends Component {
    componentWillMount() {
        this.props.isOpen && this.open()
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.isOpen !== nextProps.isOpen) {
            nextProps.isOpen
                ? this.open()
                : this.close()
        }
    }

    componentWillUnmount() {
        this.props.isOpen && this.close()
    }

    open = () => {
        document.body.style.overflow = 'hidden'
    }

    close = () => {
        document.body.style.overflow = 'initial'
    }

    render() {
        const {
            title,
            footer,
            isOpen,
            close,
            children,
        } = this.props
        const classes = ['modal', 'fade']
        isOpen && classes.push('in')

        return <div
            className={classes.join(' ')}
            onClick={(e) => {
                e.target === e.currentTarget && close()
            }}
            style={{display: isOpen ? 'block' : 'none'}}
        >
            <div className='modal-dialog'>
                {isOpen && <div className='modal-content'>
                    <div className='modal-header'>
                        <button
                            type='button'
                            className='close'
                            onClick={close}
                        >
                            <span aria-hidden='true'>×</span>
                        </button>
                        <h4 className='modal-title'>{title}</h4>
                    </div>
                    <div className='modal-body'>
                        {children}
                    </div>
                    {footer && <div className='modal-footer'>
                        {footer}
                    </div>}
                </div>}
            </div>
        </div>
    }
}
