import React from 'react'
import Breadcrumb from './Breadcrumb'

export default ({
    breadcrumb,
    children,
}) => {
    return <section className="content-header">
        <h1>{children}</h1>
        <Breadcrumb items={breadcrumb} />
    </section>
}
