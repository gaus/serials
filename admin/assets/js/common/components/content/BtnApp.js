import React from 'react'
import isUndefined from 'lodash/isUndefined'

export default ({
    className,
    active,
    text,
    icon,
    badge,
    badgeColor,
    ...params,
}) => {
    const classes = ['btn', 'btn-app']
    active && classes.push('active')
    className && classes.push(className)
    return <button
        className={classes.join(' ')}
        {...params}
    >
        {!isUndefined(badge) && <span className={'badge' + (badgeColor ? ` bg-${badgeColor}` : '')}>{badge}</span>}
        {icon && <i className={`fa ${icon}`} />}
        {text}
    </button>
}
