import React from 'react'

export default () => {
    return <footer className="main-footer">
        <div className="pull-right hidden-xs">
            <b>Версия</b> 0.1
        </div>
        <strong>
            © 2018 Smotri-online.local
        </strong>
    </footer>
}
