import React from 'react'
import { Link } from 'react-router'
import { mergeQuery } from '../../helpers/url-helper'

export default ({
    className,
    current_page,
    last_page,
    from,
    to,
    per_page,
    total,
    size = 3,
}) => {
    if (last_page === 1) {
        return null;
    }
    const pages = []
    const minRange = Math.max(1, current_page - size)
    const maxRange = Math.min(last_page, current_page + size)
    for (let i = minRange; i <= maxRange; i++) {
        pages.push(i);
    }
    return <ul className={'pagination' + (className ? ' ' + className : '')}>
        <li>
            {current_page > 1
                ? <Link to={mergeQuery({page: current_page - 1})}>«</Link>
                : <span className='active'>«</span>
            }
        </li>
        {minRange > 1 && <li>
            <Link to={mergeQuery({page: 1})}>1</Link>
        </li>}
        {minRange > 2 && <li>
            <span>...</span>
        </li>}

        {pages.map((page, i) => {
            return <li key={page}>
                {current_page === page
                    ? <span className='active'>{page}</span>
                    : <Link to={mergeQuery({page: page})}>{page}</Link>
                }
            </li>
        })}

        {maxRange < (last_page - 1) && <li>
            <span>...</span>
        </li>}
        {maxRange < last_page && <li>
            <Link to={mergeQuery({page: last_page})}>{last_page}</Link>
        </li>}
        <li>
            {current_page < last_page
                ? <Link to={mergeQuery({page: current_page + 1})}>»</Link>
                : <span className='active'>»</span>
            }
        </li>
    </ul>
}
