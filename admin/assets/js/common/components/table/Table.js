import React from 'react'

export default ({
    className,
    children,
}) => {
    return <table className={'table' + (className ? ' ' + className : '')}>
        {children}
    </table>
}
