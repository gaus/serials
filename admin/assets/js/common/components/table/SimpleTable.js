import React from 'react'
import Table from './Table'

export default (props) => {
    const keys = []
    const names = []
    for (let i in props.map) {
        keys.push(i)
        names.push(props.map[i])
    }
    return <Table {...props}>
        <thead>
            <tr>
                {names.map((name, i) => <th key={i}>{name}</th>)}
            </tr>
        </thead>
        <tbody>
            {props.items.map((item, i) => {
                return <tr key={i}>{keys.map((key, i) => <td key={i}>{item[key]}</td>)}</tr>
            })}
        </tbody>
    </Table>
}
