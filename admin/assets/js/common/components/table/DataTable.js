import React from 'react'
import Table from './Table'
import { Link } from 'react-router'
import { get, isFunction } from 'lodash'

export default (props) => {
    const {
        className,
        items = [],
        map = [],
        query = [],
        actions,
        onSort,
    } = props
    return <Table className={'data-table' + (className ? ' ' + className : '')}>
        <thead>
            <tr>
                {map.map((item, i) => {
                    let onClick
                    let nextSort
                    const classes = []
                    if (item.sort) {
                        classes.push('sortable')
                        if (item.sort === query.sort) {
                            classes.push('sort-asc')
                            nextSort = `-${item.sort}`
                        } else if (`-${item.sort}` === query.sort) {
                            classes.push('sort-desc')
                        } else {
                            nextSort = item.sort
                        }
                        onClick = (e) => {
                            onSort(e, nextSort)
                        }
                    }
                    return <th key={i} className={classes.join(' ')} onClick={onClick}>
                        {item.title}
                    </th>
                })}
                {actions && <th width={actions.length * 30} />}
            </tr>
        </thead>
        <tbody>
            {items.map((item, i) => {
                return <tr key={i}>
                    {map.map((mapItem, j) => {
                        return <td key={j}>
                            {isFunction(mapItem.field)
                                ? mapItem.field(item)
                                : get(item, mapItem.field)
                            }
                        </td>
                    })}
                    {actions && <td className="actions">
                        {actions.map((actionItem, j) => {
                            if (actionItem.to) {
                                return <Link
                                    to={isFunction(actionItem.to)
                                        ? actionItem.to(item)
                                        : actionItem.to
                                    }
                                >
                                    <i key={j} className={actionItem.className} />
                                </Link>
                            }
                            return  <i
                                key={j}
                                className={actionItem.className}
                                onClick={(e) => {actionItem.onClick(e, item)}}
                            />
                        })}
                    </td>}
                </tr>
            })}
        </tbody>
    </Table>
}
