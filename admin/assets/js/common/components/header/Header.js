import React from 'react'
import Logo from './Logo'
import TopNavigation from './TopNavigation'

export default ({
    toggleSidebar,
}) => {
    return <header className="main-header">
        <Logo />
        <TopNavigation toggleSidebar={toggleSidebar} />
    </header>
}
