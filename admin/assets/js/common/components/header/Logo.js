import React from 'react'

export default () => {
    return <a href="/" className="logo">
        {/* mini logo for sidebar mini 50x50 pixels */}
        <span className="logo-mini"><b>A</b>LT</span>
        {/* logo for regular state and mobile devices */}
        <span className="logo-lg"><b>Admin</b>LTE</span>
    </a>
}
