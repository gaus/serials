import React from 'react'

export default ({
    toggleSidebar,
}) => {
    return <nav className="navbar navbar-static-top">
        {/* Sidebar toggle button */}
        <span className="sidebar-toggle" onClick={toggleSidebar}>
            <span className="sr-only">Свернуть меню</span>
        </span>
    </nav>
}
