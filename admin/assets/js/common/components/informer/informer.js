import React from 'react'

export default ({
    id,
    status,
    title,
    description,
    close,
    onClose,
}) => {
    const icons = {
        danger: 'icon fa fa-ban',
        info: 'icon fa fa-info',
        warning: 'icon fa fa-warning',
        success: 'icon fa fa-check',
    }
    const classes = ['alert']
    status && classes.push('alert-' + status)
    close && classes.push('alert-closed')
    return <div className={classes.join(' ')} >
        <button type="button" className="close" onClick={() => onClose(id)}>×</button>
        <h4>{icons[status] && <i className={icons[status]} />} {title}</h4>
        {description && <p>{description}</p>}
    </div>
}
