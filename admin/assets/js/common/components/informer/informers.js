import React from 'react'
import Informer from './informer'

export default ({
    informers,
    onClose,
}) => {
    return <div className='informer-wrapper'>
        {informers.map(item => <Informer key={item.id} {...item} onClose={onClose} />)}
    </div>
}
