import React from 'react'

export default ({
    started,
    stopped,
}) => {
    const classes = ['preloader-top']
    started
        ? classes.push('preloader-start')
        : classes.push('preloader-hidden')
    stopped && classes.push('preloader-stop')
    return <div className={classes.join(' ')}>
        <div className='progress progress-xxs'>
            <div className='progress-bar progress-bar-danger progress-bar-striped' />
        </div>
    </div>
}
