import React from 'react'

export default ({className, text, ...params}) => {
    return <button
        className={'btn' + (className ? ` ${className}` : '')}
        {...params}
    >{text}</button>
}
