import React from 'react'

export default ({children, className}) => {
    return <div className={'form-group' + (className ? ` ${className}` : '')}>
        {children}
    </div>
}
