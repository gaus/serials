import React from 'react'
import FormGroup from './FormGroup'
import Select from './fields/Select'
import SelectSearch from './fields/SelectSearch'
import Checkbox from './fields/Checkbox'

export default ({
    label,
    type = 'text',
    name,
    error,
    help,
    status,
    setData,
    ...params
}) => {
    const classes = []
    let input
    const onInput = ({target: {value}}) => {
        const inputData = {}
        inputData[name] = value
        setData(inputData)
    }
    error && classes.push('has-error')
    status && !error && classes.push('has-' + status)
    switch (type) {
        case 'textarea':
            input = <textarea
                className='form-control'
                {...{name, onInput, ...params}}
            >{params.value}</textarea>
            break
        case 'select':
            input = <Select
                className='form-control'
                {...{name, onChange: onInput, ...params}}
            />
            break
        case 'select-search':
            input = <SelectSearch
                {...{name, set: setData, ...params}}
            />
            break
        case 'checkbox':
            input = <Checkbox
                {...{name, ...params}}
                set={(value) => {
                    const inputData = {}
                    inputData[name] = value
                    setData(inputData)
                }}
            />
            break
        default:
            input = <input
                className='form-control'
                {...{type, name, onInput, ...params}}
            />
    }
    if (type === 'checkbox') {
        classes.push('checkbox')
        return <div className={classes.join(' ')}>
            <label>{input} {label}</label>
            {(help || error) && <span className='help-block'>{error || help}</span>}
        </div>
    }
    return <FormGroup className={classes.join(' ')}>
        {label && <label className='control-label' >{label}</label>}
        {input}
        {(help || error) && <span className='help-block'>{error || help}</span>}
    </FormGroup>
}
