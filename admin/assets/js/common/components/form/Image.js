import React from 'react'
import { renderImage } from '../../helpers/image-helper'

export default ({
    className,
    src,
    label,
    name,
    srcField,
    error,
    help,
    maxHeight,
    maxWidth,
    setData,
    ...props
}) => {
    const style = {}
    const classes = ['img-form-group']
    maxHeight && (style.maxHeight = maxHeight)
    maxWidth && (style.maxWidth = maxWidth)
    className && classes.push(className)
    error && classes.push('has-error')
    return <div className={classes.join(' ')} {...props} >
        {label && <div><label className="control-label">{label}</label></div>}
        <div className='img-form-control'>
            {src && <div><img src={src} style={style} /></div>}
            {!src && <div className='img-form-noimage' style={style} />}
            {src && <div className='img-form-btn img-form-btn-top'>
                <button
                    className='btn btn-danger btn-social'
                    onClick={(e) => {
                        e.preventDefault()
                        const data = {}
                        data[name] = null
                        data[srcField] = null
                        setData(data)
                    }}
                >
                    <i className='fa fa-trash' /> Удалить изображение
                </button>
            </div>}
            <div className='img-form-btn img-form-btn-bottom'>
                <label className='btn btn-info btn-social'>
                    <i className='fa fa-download' /> Загрузить изображение
                    <input
                        type='file'
                        name={name}
                        onChange={(e) => {
                            renderImage(e.target.files[0], (src) => {
                                const data = {}
                                data[srcField] = src
                                setData(data)
                            })
                            const data = {}
                            data[name] = e.target.files[0]
                            setData(data)
                        }}
                    />
                </label>
            </div>
        </div>
        {(help || error) && <span className='help-block'>{error || help}</span>}
    </div>
}
