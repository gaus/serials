import React from 'react'

export default ({
    value,
    valueOn = true,
    valueOff = false,
    set,
    ...params,
}) => {
    return <input
        {...params}
        type='checkbox'
        onChange={() => {
            set(value === valueOn ? valueOff : valueOn)
        }}
        checked={value === valueOn}
    />
}
