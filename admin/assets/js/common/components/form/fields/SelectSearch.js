import React, { Component } from 'react'
import { isFunction } from 'lodash'

export default class SelectSearch extends Component {
    constructor() {
        super();
        this.state = {
            open: false,
            searchValue: '',
            selectedItem: undefined,
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
        this.props.search('')
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside)
    }

    handleClickOutside = (event) => {
        if (this.selectWrapper && !this.selectWrapper.contains(event.target)) {
            this.setState({open: false})
        }
    };

    open = () => {
        this.setState({open: true});
        setTimeout(() => this.searchInput.focus(), 50)
    };

    render() {
        const {
            name,
            value,
            valueName,
            items = [],
            total,
            itemComponent,
            nameField,
            valueField,
            search,
            set,
            create,
        } = this.props;
        const selectedValue = this.state.selectedItem
            ? this.state.selectedItem[valueField]
            : value;
        const selectedName = this.state.selectedItem
            ? (isFunction(nameField) ? nameField(this.state.selectedItem) : this.state.selectedItem[nameField])
            : valueName;
        const hasMore = total > items.length;
        const classes = ['select-search'];
        this.state.open && classes.push('select-search--open');

        return <div
            className={classes.join(' ')}
            ref={(ref) => {this.selectWrapper = ref}}
        >
            <div
                className='select-search-value'
                onClick={() => {
                    this.open()
                }}
            >
                {selectedName}
                {selectedValue && <i
                    className='fa fa-close'
                    onClick={(e) => {
                        e.stopPropagation();
                        const data = {};
                        data[name] = null;
                        set(data);
                        this.setState({
                            selectedItem: undefined,
                        })
                    }}
                />}
                <i className='fa fa-angle-down' />
            </div>
            <div className='select-search-dropdown'>
                <div className='select-search-search'>
                    <input
                        type='text'
                        value={this.state.searchValue}
                        onInput={({target: {value}}) => {
                            this.setState({searchValue: value});
                            search(value)
                        }}
                        ref={(ref) => {
                            this.searchInput = ref
                        }}
                    />
                    {this.state.searchValue
                        ? <i
                            className='fa fa-plus'
                            onClick={() => {
                                create(this.state.searchValue);
                                this.setState({
                                    selectedItem: undefined,
                                    open: false,
                                })
                            }}
                        />
                        : <i className='fa fa-search' />
                    }
                </div>
                {items.length > 0 && <ul className={hasMore ? 'with-more' : ''}>
                    {items.map((item) => {
                        return <li
                            key={item[valueField]}
                            className={selectedValue === item[valueField] ? 'selected' : ''}
                            onClick={() => {
                                const data = {};
                                data[name] = item[valueField];
                                set(data);
                                this.setState({
                                    selectedItem: item,
                                    open: false,
                                })
                            }}
                        >{isFunction(itemComponent)
                            ? itemComponent(item)
                            : (isFunction(nameField) ? nameField(item) : item[nameField])}
                        </li>
                    })}
                </ul>}
                {hasMore && <div className='more'>Показано {items.length} из {total} записей</div>}
            </div>
        </div>
    }
}
