import React from 'react'
import get from 'lodash/get'

export default ({
    list,
    listValue = 'value',
    listName = 'name',
    withEmpty,
    ...params,
}) => {
    return <select
        {...params}
    >
        {withEmpty && <option key='empty' value='' />}
        {list.map((item) => {
            const value = get(item, listValue);
            return <option
                key={value}
                value={value}
            >{get(item, listName)}</option>
        })}
    </select>
}
