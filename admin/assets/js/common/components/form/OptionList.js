import React, { Component } from 'react'
import FormGroup from './FormGroup'

export default class OptionList extends Component {
    constructor () {
        super()
        this.state = {
            addOpen: false,
            addValue: '',
            addItem: undefined,
        }
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.items.length < nextProps.items.length) {
            this.setState({
                addOpen: false,
                addValue: '',
                addItem: undefined,
            })
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.searchWrapper && !this.searchWrapper.contains(event.target)) {
            this.setState({addOpen: false})
        }
    }

    render () {
        const {
            className,
            label,
            items,
            nameField,
            valueField,
            search,
            create,
            add,
            remove,
            searchItems = [],
        } = this.props
        const showSearch = this.state.addOpen && !this.state.addItem && searchItems.length > 0

        return <FormGroup className={className}>
            {label && <label className='control-label'>{label}</label>}
            <div className='form-option-list'>
                {items.map((item) => {
                    return <span key={item[valueField]} className='form-option'>
                        {item[nameField]}
                        <i
                            className='fa fa-close'
                            onClick={() => {
                                remove(item[valueField])
                            }}
                        />
                    </span>
                })}
                <span
                    className={'form-option-add' + (this.state.addOpen ? ' form-option-add--open' : '')}
                    ref={(ref) => {this.searchWrapper = ref}}
                >
                    <input
                        className={showSearch ? 'with-shadow' : ''}
                        type='text'
                        value={this.state.addItem ? this.state.addItem[nameField] : this.state.addValue}
                        onInput={({target: {value}}) => {
                            this.setState({addValue: value, addItem: undefined})
                            search(value)
                        }}
                        ref={(ref) => {this.input = ref}}
                    />
                    <span
                        className='form-option-add-btn'
                        onClick={() => {
                            if (this.state.addOpen) {
                                this.state.addItem
                                    ? add(this.state.addItem[valueField])
                                    : create(this.state.addValue)
                            } else {
                                this.setState({addOpen: true})
                                this.input.focus()
                            }
                        }}
                    >
                        <i className='fa fa-plus'/>
                    </span>
                    {showSearch && <div className='form-option-add-search'>
                        {searchItems.map((item) => {
                            return <div
                                key={item[valueField]}
                                onClick={() => {
                                    this.setState({addItem: item})
                                }}
                            >
                                {item[nameField]}
                            </div>
                        })}
                    </div>}
                </span>
            </div>
        </FormGroup>
    }
}
