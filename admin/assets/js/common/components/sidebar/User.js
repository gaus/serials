import React from 'react'

export default ({
    image,
    name,
}) => {
    return <div className="user-panel">
        <div className="pull-left image">
            <img src={image} className="img-circle" alt="User Image" />
        </div>
        <div className="pull-left info">
            <p>{name}</p>
            <a href="#"><i className="fa fa-circle text-success" /> Online</a>
        </div>
    </div>
}
