import React from 'react'

export default ({
    search,
}) => {
    return <form method="get" className="sidebar-form" onSubmit={search}>
        <div className="input-group">
            <input type="text" className="form-control" placeholder="Поиск..." />
            <span className="input-group-btn">
                <button type="submit" className="btn btn-flat">
                    <i className="fa fa-search" />
                </button>
            </span>
        </div>
    </form>
}
