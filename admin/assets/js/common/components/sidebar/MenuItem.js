import React from 'react'
import { Link } from 'react-router'

const MenuItem = ({
    path,
    icon,
    name,
    link,
    items,
    mainMenuToggle,
    open,
    active,
}) => {
    const hasItems = items && items.length > 0
    const classes = [];
    hasItems && classes.push('treeview')
    open && classes.push('menu-open')
    active && classes.push('active')
    return <li className={classes.join(' ')}>
        {hasItems && <a href="#" onClick={hasItems && ((e) => {e.preventDefault(); mainMenuToggle(path)})}>
            <i className={'fa ' + icon} /> <span>{name}</span>
            <i className="pull-right fa fa-angle-left" />
        </a>}
        {!hasItems && <Link to={link} >
            <i className={'fa ' + icon} /> <span>{name}</span>
        </Link>}
        {hasItems && <ul className="treeview-menu">
            {items.map((item, i) => <MenuItem key={i} {...item} mainMenuToggle={mainMenuToggle} />)}
        </ul>}
    </li>
}

export default MenuItem
