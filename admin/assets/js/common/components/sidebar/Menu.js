import React from 'react'
import MenuItem from './MenuItem'

export default ({
    title,
    items,
    mainMenuToggle,
}) => {
    return <ul className="sidebar-menu tree">
        {title && <li className="header">{title}</li>}
        {items.map((item, i) => <MenuItem key={i} {...item} mainMenuToggle={mainMenuToggle} />)}
    </ul>
}

