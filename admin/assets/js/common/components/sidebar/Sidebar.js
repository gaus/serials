import React from 'react'
import User from './User'
import Search from './Search'
import Menu from './Menu'

export default ({
    mainMenu : {items, title},
    mainMenuToggle,
}) => {
    return <aside className="main-sidebar">
        <section className="sidebar">
            <User name="Администратор" image="/images/item_no_image.png" />
            <Search />
            <Menu
                title="Главное меню"
                {...{title, items, mainMenuToggle}}
            />
        </section>
    </aside>
}
