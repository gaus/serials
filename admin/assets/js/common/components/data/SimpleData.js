import React from 'react'
import { isUndefined, isArray, isFunction, get } from 'lodash'

export default ({
    className,
    inline,
    data,
    map,
    hideEmpty = false,
}) => {
    const classes = []
    const items = []
    inline && classes.push('dl-horizontal')
    className && classes.push(className)
    if (isArray(map)) {
        map.forEach((mapItem) => {
            const value = isFunction(mapItem.value)
                ? mapItem.value(data)
                : get(data, mapItem.value);
            (!isUndefined(value) || !hideEmpty) && items.push({
                name: mapItem.name,
                value,
            })
        })
    } else {
        for (let key in map) {
            (!isUndefined(data[key]) || !hideEmpty) && items.push({
                name: map[key],
                value: get(data, key),
            })
        }
    }
    return <dl className={classes.join(' ')}>
        {items.map((item, i) => {
            return [
                <dt key={`n${i}`}>{item.name}</dt>,
                <dd key={`v${i}`}>{item.value}</dd>
            ]
        })}
    </dl>
}
