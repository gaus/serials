import { call, put } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { Creators as Action } from '../reducers/attr-value'
import { Creators as ActionPreloader } from '../../common/reducers/preloader'

export function * getList (api, {params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getAttrValues, params)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successList(response.data))
    } else {
        yield put(Action.failList())
    }
}

export function * applyFilter ({params}) {
    yield call(delay, 500)
    yield put(Action.requestList(params))
}
