import { takeLatest } from 'redux-saga/effects'
import Api from '../../common/services/api'
import { Types as AttrTypes } from '../reducers/attr'
import { Types as AttrValueTypes } from '../reducers/attr-value'
import * as attr from './attr'
import * as attrValue from './attr-value'

const api = Api.create()

export default function * sagas () {
    yield [
        takeLatest(AttrTypes.REQUEST_LIST, attr.getList, api),

        takeLatest(AttrValueTypes.REQUEST_LIST, attrValue.getList, api),
        takeLatest(AttrValueTypes.APPLY_LIST_FILTER, attrValue.applyFilter),
    ]
}
