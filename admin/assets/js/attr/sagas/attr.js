import { call, put } from 'redux-saga/effects'
import { Creators as Action } from '../reducers/attr'
import { Creators as ActionPreloader } from '../../common/reducers/preloader'

export function * getList (api, {params}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getAttrs, params)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successList(response.data))
    } else {
        yield put(Action.failList())
    }
}
