
export default {
    attr: require('./attr').reducer,
    attrValue: require('./attr-value').reducer,
}
