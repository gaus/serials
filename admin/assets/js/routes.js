import React from 'react'
import { Route, IndexRoute } from 'react-router'
import Main from './common/layouts/Main'
import Home from './home/pages/Home'
import NotFound from './common/pages/NotFound'
import UserIndex from './user/pages/Index'
import UserEdit from './user/pages/Edit'
import UserAdd from './user/pages/Add'
import ItemIndex from './item/pages/Index'
import ItemAdd from './item/pages/Add'
import ItemEdit from './item/pages/Edit'
import ItemSeasons from './item/pages/ItemSeasons'
import ItemSeries from './item/pages/ItemSeries'
import ItemVideo from './item/pages/ItemVideo'
import ParserItemIndex from './parser-item/pages/Index'
import ParserItemView from './parser-item/pages/View'
import ItemParsing from './item/pages/ItemParsing'

export default (
    <div>
        <Route path='/admin' component={Main}>
            <IndexRoute component={Home} />

            <Route path='users' component={UserIndex} />
            <Route path='users/add' component={UserAdd} />
            <Route path='users/:id' component={UserEdit} />

            <Route path='items' component={ItemIndex} />
            <Route path='items/add' component={ItemAdd} />
            <Route path='items/:id' component={ItemEdit} />
            <Route path='items/:id/seasons' component={ItemSeasons} />
            <Route path='items/:id/series' component={ItemSeries} />
            <Route path='items/:id/videos' component={ItemVideo} />
            <Route path='items/:id/parsing' component={ItemParsing} />

            <Route path='parser-items' component={ParserItemIndex} />
            <Route path='parser-items/:id' component={ParserItemView} />

            <Route path='*' component={NotFound} />
        </Route>
    </div>
)
