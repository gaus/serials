<?php

namespace Admin\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

/**
 * ServiceProvider.
 */
class AdminServiceProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function boot(): void
    {
        View::addNamespace('admin', base_path('admin/views'));
    }

    /**
     * @inheritdoc
     */
    public function register(): void
    {

    }
}
