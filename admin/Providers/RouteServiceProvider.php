<?php

namespace Admin\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

/**
 * Сервис провадер для роутов.
 */
class RouteServiceProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    protected $namespace = 'Admin\Controllers';

    /**
     * @inheritdoc
     */
    public function boot(): void
    {
        parent::boot();
    }

    /**
     * Устанавливает роуты.
     */
    public function map(): void
    {
        Route::prefix('admin')
            ->middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('admin/routes/web.php'));
    }
}
