<?php

namespace Admin\Api\Actions;

use Admin\Api\Models\ParserItem;
use Gaus57\LaravelApi\Actions\UpdateAction;
use Symfony\Component\HttpFoundation\Response;

class ParserItemUpdate extends UpdateAction
{
    public function run(): Response
    {
        if (!$this->validateRequest($errors)) {
            return $this->resourceResponse($this->resourceErrors, $errors, 422);
        }
        /** @var ParserItem $model */
        $model = $this->findModel();
        $model->fill($this->request->all())->save();

        $applyData = $this->request->input('apply_data');
        $applyAttrs = $this->request->input('apply_attrs');
        $applySeries = $this->request->input('apply_series');
        $applyVideos = $this->request->input('apply_videos');
        if ($applyData || $applyAttrs || $applySeries || $applyVideos) {
            $data = $model->mergedParserItemEventsData();
            $applyData && $model->updateItem($data['data'] ?? []);
            $applyAttrs && $model->updateItemAttrs($data['attrs'] ?? []);
            $applySeries && $model->updateItemSeries($data['series'] ?? []);
            $applyVideos && $model->updateItemVideos($data['videos'] ?? []);
        }

        return $this->resourceResponse($this->resource, $model);
    }
}
