<?php

namespace Admin\Api\Requests;

/**
 * Class StoreItem
 */
class StoreItem extends Request
{
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'name_ru' => [
                'nullable',
                'required_without_all:name_en,name_original',
                'string',
                'max:190',
            ],
            'name_en' => [
                'nullable',
                'required_without_all:name_ru,name_original',
                'string',
                'max:190',
            ],
            'name_original' => [
                'nullable',
                'required_without_all:name_en,name_ru',
                'string',
                'max:190',
            ],
            'category_id' => [
                'nullable',
                'integer',
                'exists:categories,id',
            ],
            'started_at' => [
                'nullable',
                'date',
            ],
            'completed_at' => [
                'nullable',
                'date',
            ],
            'year_start' => [
                'nullable',
                'integer',
            ],
            'year_complete' => [
                'nullable',
                'integer',
            ],
            'duration' => [
                'nullable',
                'integer',
            ],
            'image' => [
                'nullable',
                $this->hasFile('image') ? 'image' : 'string',
            ],
            'rating_kp' => [
                'nullable',
                'numeric',
                'min:0',
                'max:10',
            ],
            'rating_imdb' => [
                'nullable',
                'numeric',
                'min:0',
                'max:10',
            ],
            'published' => [
                'nullable',
                'boolean',
            ],
        ]);
    }
}
