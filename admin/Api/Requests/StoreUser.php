<?php

namespace Admin\Api\Requests;

/**
 * Class StoreUser
 */
class StoreUser extends Request
{
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'name' => ['required', 'string', 'max:190'],
            'email' => ['required', 'string', 'max:190', 'email'],
            'password' => ['required', 'string'],
        ]);
    }
}
