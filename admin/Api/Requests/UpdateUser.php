<?php

namespace Admin\Api\Requests;

/**
 * Class UpdateUser
 */
class UpdateUser extends Request
{
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'name' => ['required', 'string', 'max:190'],
            'email' => ['required', 'string', 'max:190', 'email'],
            'password' => ['string'],
        ]);
    }
}
