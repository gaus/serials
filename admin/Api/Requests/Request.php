<?php

namespace Admin\Api\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Class BaseRequest
 */
class Request extends FormRequest
{
    public function authorize(): bool
    {
        if (!Auth::check()) {
            throw new UnauthorizedHttpException();
        }

        return Auth::user()->hasRole(1);
    }

    public function rules(): array
    {
        return [
            'scenario' => 'string',
        ];
    }
}
