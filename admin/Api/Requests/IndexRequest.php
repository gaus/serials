<?php

namespace Admin\Api\Requests;

class IndexRequest extends Request
{
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'per_page' => 'integer',
            'sort' => 'string',
            'page' => 'integer',
        ]);
    }
}
