<?php

namespace Admin\Api\Components;

use Illuminate\Support\Facades\Request;

/**
 * Trait ResourceScenarioTrait
 *
 * @method mixed when($condition, $value, $default = null)
 * @method mixed mergeWhen($condition, $value)
 */
trait ResourceScenarioTrait
{
    public function scenario(): ?string
    {
        return Request::get('scenario');
    }

    public function whenScenario($scenarios, $value, $default = null)
    {
        $scenarios = is_array($scenarios) ? $scenarios : [$scenarios];

        return $this->when(
            \in_array($this->scenario(), $scenarios),
            $value,
            $default
        );
    }

    public function mergeWhenScenario($scenarios, $value)
    {
        $scenarios = is_array($scenarios) ? $scenarios : [$scenarios];

        return $this->mergeWhen(
            \in_array($this->scenario(), $scenarios),
            $value
        );
    }
}
