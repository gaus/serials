<?php

namespace Admin\Api\Components;

class ModelResource extends \Gaus57\LaravelApi\Resources\ModelResource
{
    use ResourceScenarioTrait;
}
