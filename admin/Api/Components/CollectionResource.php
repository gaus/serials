<?php

namespace Admin\Api\Components;

class CollectionResource extends \Gaus57\LaravelApi\Resources\CollectionResource
{
    use ResourceScenarioTrait;
}
