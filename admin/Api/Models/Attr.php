<?php

namespace Admin\Api\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @inheritdoc
 */
class Attr extends \App\Models\Attr implements FieldsResourceInterface
{
    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'sort' => $this->sort,
        ];
    }
}
