<?php

namespace Admin\Api\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourcesInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ItemSeries
 */
class ItemSeries extends \App\Models\ItemSeries implements FieldsResourceInterface, FindResourcesInterface
{
    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'item_season_id' => $this->item_season_id,
            'number' => $this->number,
            'name_ru' => $this->name_ru,
            'name_original' => $this->name_original,
            'released_at' => $this->released_at ? $this->released_at->format('Y-m-d') : null,
            'created_at' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : null,
            'season' => $this->season,
        ];
    }

    public function scopeFindResources(Builder $query, Request $request): Builder
    {
        if ($itemId = $request->get('item_id')) {
            $query->join('item_seasons', 'item_series.item_season_id', '=', 'item_seasons.id')
                ->where('item_seasons.item_id', $itemId)
                ->select(['item_series.*']);
        }
        if ($itemSeasonId = $request->get('item_season_id')) {
            $query->where('item_series.item_season_id', $itemSeasonId);
        }
        $query->with('season');

        return $query;
    }
}
