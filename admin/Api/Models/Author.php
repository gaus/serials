<?php

namespace Admin\Api\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourcesInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Author
 */
class Author extends \App\Models\Author implements FieldsResourceInterface, FindResourcesInterface
{
    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }

    public function scopeFindResources(Builder $query, Request $request): Builder
    {
        if ($name = $request->get('name')) {
            $query->where('name', 'ILIKE', "$name%");
        }

        return $query;
    }
}
