<?php

namespace Admin\Api\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourcesInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ItemVideo
 */
class ItemVideo extends \App\Models\ItemVideo implements FieldsResourceInterface, FindResourcesInterface
{
    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'item_id' => $this->item_id,
            'item_season_id' => $this->item_season_id,
            'item_series_id' => $this->item_series_id,
            'video_type_id' => $this->video_type_id,
            'author_id' => $this->author_id,
            'source_id' => $this->source_id,
            'quality' => $this->quality,
            'src' => $this->src,
            'created_at' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : null,
            'item_season' => $this->itemSeason,
            'item_series' => $this->itemSeries,
            'video_type' => $this->videoType,
            'author' => $this->author,
            'source' => $this->source,
        ];
    }

    public function scopeFindResources(Builder $query, Request $request): Builder
    {
        if ($itemId = $request->get('item_id')) {
            $query->where('item_id', $itemId);
        }
        $query->with([
            'itemSeason',
            'itemSeries',
            'videoType',
            'author',
            'source',
        ]);

        return $query;
    }
}
