<?php

namespace Admin\Api\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourcesInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ItemSeason
 */
class ItemSeason extends \App\Models\ItemSeason implements FieldsResourceInterface, FindResourcesInterface
{
    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'item_id' => $this->item_id,
            'number' => $this->number,
            'started_at' => $this->started_at ? $this->started_at->format('Y-m-d') : null,
            'completed_at' => $this->completed_at ? $this->completed_at->format('Y-m-d') : null,
            'created_at' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function scopeFindResources(Builder $query, Request $request): Builder
    {
        if ($itemId = $request->get('item_id')) {
            $query->where('item_id', $itemId);
        }

        return $query;
    }
}
