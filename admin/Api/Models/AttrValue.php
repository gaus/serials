<?php

namespace Admin\Api\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourcesInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @inheritdoc
 */
class AttrValue extends \App\Models\AttrValue implements FieldsResourceInterface, FindResourcesInterface
{
    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'attr_id' => $this->attr_id,
            'value' => $this->value,
            'sort' => $this->sort,
        ];
    }

    public function scopeFindResources(Builder $query, Request $request): Builder
    {
        if ($attr_id = $request->get('attr_id')) {
            $query->where('attr_id', $attr_id);
        }
        if ($value = $request->get('value')) {
            $query->where('value', 'ILIKE', "$value%");
        }

        return $query;
    }
}
