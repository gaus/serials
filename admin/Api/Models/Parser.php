<?php

namespace Admin\Api\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @inheritdoc
 */
class Parser extends \App\Models\Parser implements FieldsResourceInterface
{
    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'url' => $this->url,
            'class' => $this->class,
            'is_active' => $this->is_active,
            'last_activity' => $this->last_activity,
        ];
    }
}
