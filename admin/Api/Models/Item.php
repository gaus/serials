<?php

namespace Admin\Api\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourcesInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

/**
 * Class Item
 */
class Item extends \App\Models\Item implements FieldsResourceInterface, FindResourcesInterface
{
    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'name_ru' => $this->name_ru,
            'name_en' => $this->name_en,
            'name_original' => $this->name_original,
            'description' => $this->description,
            'image' => $this->image,
            'image_src' => $this->image_src,
            'started_at' => $this->started_at ? $this->started_at->format('Y-m-d') : null,
            'completed_at' => $this->completed_at ? $this->completed_at->format('Y-m-d') : null,
            'season_count' => $this->season_count,
            'series_count' => $this->series_count,
            'rating' => $this->rating,
            'rating_kp' => $this->rating_kp,
            'rating_imdb' => $this->rating_imdb,
            'year_start' => $this->year_start,
            'year_complete' => $this->year_complete,
            'duration' => $this->duration,
            'kinopoisk_id' => $this->kinopoisk_id,
            'published' => $this->published,
            'attrs' => $this->attrs,
            'created_at' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : null,
            $resource->mergeWhenScenario('edit', [
                'attr_values' => $this->attrValues,
            ]),
        ];
    }

    public function scopeFindResources(Builder $query, Request $request): Builder
    {
        if ($q = $request->get('q')) {
            foreach (preg_split('/(,|\s)/', $q) as $v) {
                $query->where(function ($query) use ($v) {
                    $query->orWhere(DB::raw('UPPER(name_ru)'), 'like', '%'.mb_strtoupper($v).'%')
                        ->orWhere(DB::raw('UPPER(name_en)'), 'like', '%'.mb_strtoupper($v).'%')
                        ->orWhere(DB::raw('UPPER(name_original)'), 'like', '%'.mb_strtoupper($v).'%');

                });
            }
        }

        $sortAvailable = [
            'id',
            'name_ru',
            'name_en',
            'name_original',
            'year_start',
            'year_complete',
            'created_at',
        ];
        $sort = $request->get('sort');
        if ($sort) {
            if (starts_with($sort, '-')) {
                $direction = 'desc';
                $sort = substr($sort, 1);
            } else {
                $direction = 'asc';
            }
            if (\in_array($sort, $sortAvailable)) {
                $query->orderBy($sort, $direction);
            }
        }

        return $query;
    }

    public function save(array $options = [])
    {
        /** @var Request $request */
        $request = app(Request::class);
        if ($file = $request->file('image')) {
            $fileName = md5_file($file->getRealPath()) . '.' . $file->getClientOriginalExtension();
            $fullPath = $this->uploadPath($fileName);
            if (!is_dir(dirname($fullPath))) {
                mkdir(dirname($fullPath), 0775, true);
            }
            $file->move(dirname($fullPath), pathinfo($fileName, PATHINFO_BASENAME));
            $this->image = $fileName;
        }
        return parent::save($options);
    }

    public function getImageSrcAttribute($value)
    {
        return $this->image ?
            url(self::IMAGE_PATH . $this->image) :
            null;
    }
}
