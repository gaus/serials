<?php

namespace Admin\Api\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourceInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemAttr extends \App\Models\ItemAttr implements FieldsResourceInterface, FindResourceInterface
{
    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'item_id' => $this->item_id,
            'attr_id' => $this->attr_id,
            'attr_value_id' => $this->attr_value_id,
        ];
    }

    public function findResource(Request $request, string $id): ?Model
    {
        $split = explode(',', $id);
        if (count($split) === 3) {
            $result = self::where('item_id', $split[0])
                ->where('attr_id', $split[1])
                ->where('attr_value_id', $split[2])
                ->first();
        } else {
            $result = self::find($id);
        }

        return $result;
    }
}
