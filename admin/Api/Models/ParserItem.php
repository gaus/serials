<?php

namespace Admin\Api\Models;

use App\Models\ParserItemEvent;
use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourcesInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

/**
 * @inheritdoc
 */
class ParserItem extends \App\Models\ParserItem implements FieldsResourceInterface, FindResourcesInterface
{
    public function scopeFindResources(Builder $query, Request $request): Builder
    {
        $query->with(['item', 'parser']);
        if ($request->get('scenario') === 'show') {
            $query->with('parserItemEvents');
        }
        $sortAvailable = [
            'id',
            'parser_id',
            'item_id',
            'url',
            'is_active',
            'last_activity',
            'created_at',
            'updated_at',
        ];
        $sort = $request->get('sort');
        if ($sort) {
            if (starts_with($sort, '-')) {
                $direction = 'desc';
                $sort = substr($sort, 1);
            } else {
                $direction = 'asc';
            }
            if (\in_array($sort, $sortAvailable)) {
                $query->orderBy($sort, $direction);
            }
        }
        if ($itemId = $request->get('item_id')) {
            $query->where('item_id', $itemId);
        }
        if ($parserId = $request->get('parser_id')) {
            $query->where('parser_id', $parserId);
        }
        if ($q = $request->get('q')) {
            $query->whereExists(function ($query) use ($q) {
                $query
                    ->select(DB::raw(1))
                    ->from('parser_item_events')
                    ->whereRaw('parser_item_events.parser_item_id = parser_items.id')
                    ->where(function ($query) use ($q) {
                        foreach (preg_split('/(,|\s)/', $q) as $v) {
                            $query->where(function ($query) use ($v) {
                                $query->orWhere(DB::raw("data->'data'->>'name_ru'"), 'ilike', "%$v%")
                                    ->orWhere(DB::raw("data->'data'->>'name_en'"), 'ilike', "%$v%")
                                    ->orWhere(DB::raw("data->'data'->>'name_original'"), 'ilike', "%$v%");

                            });
                        }
                    });

                return $query;
            });
        }

        return $query;
    }

    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'parser_id' => $this->parser_id,
            'item_id' => $this->item_id,
            'url' => $this->url,
            'is_active' => $this->is_active,
            'last_activity' => $this->last_activity,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'item' => new JsonResource($this->item),
            'parser' => new JsonResource($this->parser),
            $resource->mergeWhenScenario('show', [
                'data' => $this->mergedParserItemEventsData(),
            ]),
        ];
    }

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id', 'id');
    }
}
