<?php

namespace Admin\Api\Models;

use Gaus57\LaravelApi\Interfaces\FieldsResourceInterface;
use Gaus57\LaravelApi\Interfaces\FindResourcesInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class User
 */
class User extends \App\Models\User implements FieldsResourceInterface, FindResourcesInterface
{
    public function fieldsResource(JsonResource $resource, Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
        ];
    }

    public function scopeFindResources(Builder $query, Request $request): Builder
    {
        switch ($request->get('sort')) {
            case 'id':
                $query->orderBy('id');
                break;
            case '-id':
                $query->orderBy('id', 'desc');
                break;
            case 'name':
                $query->orderBy('name');
                break;
            case '-name':
                $query->orderBy('name', 'desc');
                break;
            case 'email':
                $query->orderBy('email');
                break;
            case '-email':
                $query->orderBy('email', 'desc');
                break;
            case 'created_at':
                $query->orderBy('created_at');
                break;
            case '-created_at':
                $query->orderBy('created_at', 'desc');
                break;
        }
        return $query;
    }
}
