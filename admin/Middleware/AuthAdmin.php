<?php

namespace Admin\Middleware;

use Illuminate\Auth\Middleware\Authenticate;

/**
 * Посредник дляпроверки доступа к админке.
 */
class AuthAdmin extends Authenticate
{
    /**
     * @inheritdoc
     */
    protected function authenticate(array $guards)
    {
        parent::authenticate($guards);
        // @TODO Дописать проверку доступа к админке
    }
}
