<?php

Route::post('/login', 'MainController@login')
    ->name('admin.login');

Route::get('/admin.js', function () {
    return Storage::download(base_path('/admin/assets/admin.js'));
})->middleware('auth.admin')->name('admin.script');

Route::get('/{path?}', 'MainController@index')
    ->middleware('guest')
    ->where(['path' => '[0-9a-zA-Z\\-_/]*'])
    ->name('admin');
