<?php

namespace Admin\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Контроллер админки.
 */
class MainController extends Controller
{
    public function index()
    {
        if (!Auth::check() || !Auth::user()->hasRole(1)) {
            return $this->auth();
        }

        return view('admin::index');
    }

    private function auth()
    {
        return view('admin::auth');
    }

    public function login(Request $request)
    {
        $userName = $request->input('username');
        $password = $request->input('password');
        $remember = (bool) $request->input('password');
        if (Auth::attempt(['email' => $userName, 'password' => $password], $remember)) {
            return redirect()->back();
        }
        return view('admin::auth', [
            'error' => 'Неверный логин или пароль.',
        ]);
    }
}
