<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Вход в админку</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/css/admin.css?v={{ filemtime(public_path('/css/admin.css')) }}">
</head>
<body class="skin-blue sidebar-mini sidebar-collapse" cz-shortcut-listen="true">
    <div id="root"></div>
    {{-- @TODO Сделать получение скрипта через закрытый роут --}}
    {{--<script src="{{ route('admin.script') }}"></script>--}}
    <script src="/js/admin.js?v={{ filemtime(public_path('/js/admin.js')) }}"></script>
</body>
</html>
