<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Вход в админку</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/css/admin.css">
</head>
<body class="hold-transition login-page" cz-shortcut-listen="true">
    <div class="login-box">
        <div class="login-logo">
            <a href="/"><b>Smotri</b>online</a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Войти в админку</p>
            <form action="{{ route('admin.login') }}" method="post">
                @csrf
                <div class="form-group has-feedback">
                    <input name="username" type="email" class="form-control" placeholder="Логин">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input name="password" type="password" class="form-control" placeholder="Пароль">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                @if (isset($error))
                    <span class="text-red">{{ $error }}</span>
                @endif
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox">
                            <label>
                                <input name="remember" type="checkbox" value="1">
                                Запомнить меня
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
                    </div>
                </div>
            </form>
            <a href="#">Забыли пароль?</a><br>
        </div>
    </div>
</body>
</html>
