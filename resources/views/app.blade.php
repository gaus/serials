<!DOCTYPE html>
<html>
<head>
    <title>{{ $seoTitle or '' }}</title>
    <meta name="description" content="{{ $seoDescription or '' }}">
    <meta name="keywords" content="{{ $seoKeywords or '' }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="application-name" content="Телесериальчик">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="#55AEFF">
    <meta name="apple-mobile-web-app-title" content="Телесериальчик">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    @if ($isMobileVersion)
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="/css/mobile-app.css?v6">
    @else
        <meta name="viewport" content="width=1240">
        <link rel="stylesheet" type="text/css" href="/css/app.css?v4">
    @endif

    @if ($item)
        <meta property="og:url" content="{!! $request->url() !!}" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Смотреть сериал «{{ $item->name }}» все серии онлайн" />
        <meta property="og:description" content="{{ $item->description }}" />
        @if ($item->image)
            <meta property="og:image" content="{!! $item->image_src !!}" />
        @endif
    @endif
</head>
<body>
    @if (env('APP_ENV') === 'production')
        <script src="https://cdn.ravenjs.com/3.24.0/raven.min.js" crossorigin=anonymous></script>
        <script>
            Raven.config('{{ env('SENTRY_LARAVEL_DSN') }}', {
                release: '0-0-0',
                environment: 'development-test',
            }).install();
        </script>
    @endif

    <div id="root">
        <script type="text/javascript">
            window.authUser = {!! Auth::check() ? json_encode(Auth::user()->toArray()) : 'null' !!};
            window.lists = {!! $listsJson !!};
            window.isMobileVersion = {!! $isMobileVersion ? 'true' : 'false' !!};
        </script>
    </div>
    <div id="vk_api_transport"></div>

    @if (env('APP_ENV') === 'production')
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter49598884 = new Ya.Metrika2({
                            id:49598884,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/tag.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks2");
        </script>
        {{--<noscript><div><img src="https://mc.yandex.ru/watch/49598884" style="position:absolute; left:-9999px;" alt="" /></div></noscript>--}}
        <!-- /Yandex.Metrika counter -->
    @endif

    <script type="text/javascript" src="/js/app.js?v6"></script>
</body>
</html>