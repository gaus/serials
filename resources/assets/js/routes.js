import React from 'react'
import { Route, IndexRoute } from 'react-router'
import Main from './layouts/Main'
import Home from './pages/Home'
import Item from './pages/Item'
import List from './pages/List'
import NotFound from './pages/NotFound'

export default (
    <div>
        <Route path='/' component={Main}>
            <IndexRoute component={Home} />
            <Route path='/item/:id' component={Item} />
            <Route path='/list' component={List} />

            <Route path='*' component={NotFound} />
        </Route>
    </div>
)
