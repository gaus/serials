export function numberOf (n, words) {
    // не будем склонять отрицательные числа
    n = Math.abs(n);
    const keys = [2, 0, 1, 1, 1, 2];
    const mod = n % 100;
    let words_key = (mod > 4 && mod < 20) ? 2 : keys[Math.min(mod % 10, 5)];

    return words[words_key];
}
