import mobile from 'is-mobile'

export const isMobile = () => mobile()
