import { isUndefined } from 'lodash'

export const mergeQuery = (query1, query2) => {
    const query = {...query1, ...query2}
    for (let key in query) {
        isUndefined(query[key]) && delete query[key]
    }

    return query
}
