
export function sortVideoPriority (active, list) {
    const videos = list.slice()
    const rang = (video) => {
        let videoRang = 0
        video.video_type_id === active.video_type_id && (videoRang =+ 2)
        video.author_id === active.author_id && videoRang++
        video.source_id === active.source_id && videoRang++

        return videoRang
    }

    return videos.sort((a, b) => {
        return rang(b) - rang(a)
    })
}
