import { VK_APP_ID } from '../constants'

export const initVK = (callback) => {
    if (typeof VK === 'undefined') {
        window.vkAsyncInit = function() {
            VK.init({
                apiId: VK_APP_ID,
            });
            callback(VK)
        };
        setTimeout(function() {
            const el = document.createElement("script");
            el.type = "text/javascript";
            el.src = "https://vk.com/js/api/openapi.js?156";
            el.async = true;
            document.getElementById("vk_api_transport").appendChild(el);
        }, 0);
    } else {
        callback(VK)
    }
};
