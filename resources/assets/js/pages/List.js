import React, { Component } from 'react'
import { connect } from 'react-redux'
import { get } from 'lodash'
import { Creators as ListAction } from '../reducers/list'
import ListItems from '../components/list/ListItems'
import Chopick from '../components/common/Chopick'
import { IS_MOBILE_VERSION } from '../constants'

export class List extends Component {
    componentDidMount() {
        this.props.fetchLists({scenario: 'user-list'});
        document.title = 'Мой список';
        document
            .querySelector('meta[name="description"]')
            .setAttribute('content', '');
        document
            .querySelector('meta[name="keywords"]')
            .setAttribute('content', '');
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.user.data !== nextProps.user.data) {
            this.props.fetchLists({scenario: 'user-list'})
        }
    }

    render () {
        const lists = get(this.props.list, 'list.data', []);

        if (IS_MOBILE_VERSION) {
            return <div className='my-page'>
                {lists.map(list => {
                    return <ListItems
                        key={list.id}
                        list={list}
                        updateListItem={this.props.updateListItem}
                    />
                })}
            </div>
        }

        return <div className='my-page site-width row'>
            <div className="col-1">
                {lists.map(list => {
                    return <ListItems
                        key={list.id}
                        list={list}
                        updateListItem={this.props.updateListItem}
                    />
                })}
            </div>
            <div className="col-2">
                <Chopick position='list-aside' />
            </div>
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchLists: (query) => dispatch(ListAction.fetchLists(query)),
        updateListItem: (id, data) => dispatch(ListAction.updateListItem(id, data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(List)
