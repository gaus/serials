import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isEqual } from 'lodash'
import InfiniteScroll from '../components/common/InfiniteScroll'
import { mergeQuery } from '../helpers/url'
import { Creators as ItemAction } from '../reducers/item'
import { Creators as ListAction } from '../reducers/list'
import ItemsList from '../components/item/List'
import Filters from '../components/item/Filters'
import Chopick from '../components/common/Chopick'
import { IS_MOBILE_VERSION } from '../constants'
import {Creators as UserAction} from '../reducers/user'

export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenFilterPopup: false,
        }
    }

    toggleFilterPopup = () => {
        this.setState({
            isOpenFilterPopup: !this.state.isOpenFilterPopup,
        })
    };

    componentDidMount() {
        const { query } = this.props.routing.locationBeforeTransitions;
        if (!isEqual(this.props.item.listQuery, query)) {
            this.props.reset();
            this.props.fetchItems(query);
            this.props.fetchFilters(query)
        }

        document.title = 'Сериалы. Смотреть онлайн';
        document
            .querySelector('meta[name="description"]')
            .setAttribute('content', 'Сериалы. Смотреть онлайн');
        document
            .querySelector('meta[name="keywords"]')
            .setAttribute('content', 'Сериалы. Смотреть онлайн');
    }

    componentWillReceiveProps (nextProps) {
        if (
            this.props.routing !== nextProps.routing
            || this.props.user.data !== nextProps.user.data
        ) {
            const { query } = nextProps.routing.locationBeforeTransitions;
            this.props.fetchItems(query)
        }
    }

    moreItems = () => {
        const nextPage = this.props.item.list.meta.current_page + 1;
        const query = {...this.props.routing.locationBeforeTransitions.query};
        query.page = nextPage;

        this.props.fetchMoreItems(query)
    };

    applyFilter = (params) => {
        const { query } = this.props.routing.locationBeforeTransitions;
        this.props.applyFilter(mergeQuery(query, params))
    };

    addList = (itemId, listId = 1) => {
        if (this.props.user.data && this.props.user.data.id) {
            this.props.addListItem({
                user_id: this.props.user.data.id,
                list_id: listId,
                item_id: itemId,
            })
        } else {
            this.props.needAuth()
        }
    };

    render () {
        const { query } = this.props.routing.locationBeforeTransitions;
        const {
            data: items,
            meta: pagination
        } = (this.props.item.list || {});
        const hasMore = pagination && pagination.current_page < pagination.last_page;

        if (IS_MOBILE_VERSION) {
            return <div className="category-page">
                <Filters
                    filter={this.props.item.filter}
                    apply={this.applyFilter}
                    query={query}
                    isOpenFilterPopup={this.state.isOpenFilterPopup}
                    toggleFilterPopup={this.toggleFilterPopup}
                />
                {!this.state.isOpenFilterPopup &&
                    <InfiniteScroll
                        loadMore={this.moreItems}
                        hasMore={hasMore}
                        loadingMore={this.props.item.isListFetching}
                        loader={<div className="items-more">Загрузка...</div>}
                    >
                        <ItemsList items={items} add={this.addList}/>
                    </InfiniteScroll>
                }
            </div>
        }

        return <div className="category-page site-width row">
            <div className="col-1">
                <InfiniteScroll
                    loadMore={this.moreItems}
                    hasMore={hasMore}
                    loadingMore={this.props.item.isListFetching}
                    loader={<div className="items-more">Загрузка...</div>}
                >
                    <ItemsList items={items} add={this.addList} />
                </InfiniteScroll>
            </div>

            <div className="col-2">
                <Chopick position='home-aside-top' />
                <Filters
                    filter={this.props.item.filter}
                    apply={this.applyFilter}
                    query={query}
                />
                {/*<Chopick position='home-aside-bottom' />*/}
            </div>
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchItems: (query) => dispatch(ItemAction.fetchItems(query)),
        fetchMoreItems: (query) => dispatch(ItemAction.fetchMoreItems(query)),
        fetchFilters: (query) => dispatch(ItemAction.fetchFilters(query)),
        applyFilter: (query) => dispatch(ItemAction.applyFilter(query)),
        addListItem: (data) => dispatch(ListAction.addListItem(data)),
        reset: () => dispatch(ItemAction.resetList()),
        needAuth: () => dispatch(UserAction.toggleNeedAuthPopup(true)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
