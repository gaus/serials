import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Creators as ItemAction } from '../reducers/item'
import { Creators as VideoAction } from '../reducers/video'
import { Creators as ListAction } from '../reducers/list'
import { Creators as UserAction } from '../reducers/user'
import Info from '../components/item/Info'
import Player from '../components/item/Player'
import SelectList from '../components/item/SelectList'
import Rating from '../components/item/Rating'
import Chopick from '../components/common/Chopick'
import { IS_MOBILE_VERSION } from '../constants'

export class Item extends Component {
    componentDidMount() {
        this.props.fetchItem(this.props.params.id)
    }

    componentWillUnmount() {
        this.props.resetVideo();
        this.props.resetItem()
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.params.id !== nextProps.params.id) {
            this.props.resetVideo();
            this.props.resetItem();
            this.props.fetchItem(nextProps.params.id)
        }
        if (this.props.user.data !== nextProps.user.data) {
            this.props.fetchItem(nextProps.params.id)
        }
        if (this.props.item.data !== nextProps.item.data && nextProps.item.data) {
            document.title = nextProps.item.data.name;
            document
                .querySelector('meta[name="description"]')
                .setAttribute('content', nextProps.item.data.name);
            document
                .querySelector('meta[name="keywords"]')
                .setAttribute('content', nextProps.item.data.name);
        }
    }

    addListItem = (listId = 1) => {
        if (this.props.user.data && this.props.user.data.id) {
            this.props.addListItem({
                user_id: this.props.user.data.id,
                item_id: this.props.item.data.id,
                list_id: listId,
            })
        }
    };

    render () {
        const {
            item: {data: item},
            user: {data: user},
        } = this.props;
        if (!item) {
            return null
        }
        const isAuth = user && user.id;
        const name_original = item.name_original || item.name_en;

        if (IS_MOBILE_VERSION) {
            return <div className="item-page site-width">
                <h1 className="item-info-name">
                    {item.name}
                    {name_original && name_original !== item.name &&
                    <span>{name_original}</span>
                    }
                </h1>
                <div className="item-info-image">
                    <img src={item.image_src} />
                </div>
                <Rating
                    item={item}
                    user={user}
                    save={isAuth ? this.props.saveUserRating : this.props.needAuth}
                />
                <Info item={item} />
                <SelectList
                    lists={this.props.list.list && this.props.list.list.data}
                    active={item.user_item && item.user_item.list_id}
                    addListItem={isAuth ? this.addListItem : this.props.needAuth}
                />
                <Chopick position='item-info' />
                {item && item.has_video && <Player {...this.props} />}
            </div>
        }

        return <div>
            <div className="item-page site-width">
                <h1 className="item-info-name">
                    {item.name}
                    {name_original && name_original !== item.name &&
                        <span>{name_original}</span>
                    }
                </h1>

                <div className="row">
                    <div className="col-1">
                        <div className="item-info-image">
                            <img src={item.image_src} />
                        </div>
                        <Rating
                            item={item}
                            user={user}
                            save={isAuth ? this.props.saveUserRating : this.props.needAuth}
                        />
                        <SelectList
                            lists={this.props.list.list && this.props.list.list.data}
                            active={item.user_item && item.user_item.list_id}
                            addListItem={isAuth ? this.addListItem : this.props.needAuth}
                        />
                    </div>

                    <div className="col-2">
                        <Info item={item} />
                        <Chopick position='item-info' />
                    </div>
                </div>
            </div>

            {item && item.has_video && <Player {...this.props} />}
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchItem: (id) => dispatch(ItemAction.fetchItem(id)),
        fetchVideo: (id, query) => dispatch(VideoAction.fetchVideo(id, query)),
        fetchVideos: (query) => dispatch(VideoAction.fetchVideos(query)),
        setVideo: (data) => dispatch(VideoAction.successFetchVideo(data)),
        watchVideo: (data) => dispatch(VideoAction.watchVideo(data)),
        addListItem: (data) => dispatch(ListAction.addListItem(data)),
        updateListItem: (id, data) => dispatch(ListAction.updateListItem(id, data)),
        saveUserRating: (data) => dispatch(ItemAction.saveUserRating(data)),
        resetVideo: () => dispatch(VideoAction.reset()),
        resetItem: () => dispatch(ItemAction.resetData()),
        needAuth: () => dispatch(UserAction.toggleNeedAuthPopup(true)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Item)
