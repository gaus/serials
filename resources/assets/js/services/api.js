import apisauce from 'apisauce'

const create = () => {
    const api = apisauce.create({
        baseURL: '/api/v2/',
        headers: {
            'Accept': 'application/json',
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/json; charset=UTF-8',
        },
        timeout: 20000,
    });
    const getUser = (id) => api.get(`user/${id}`);
    const putUser = (id, data) => api.put(`user/${id}`, data);
    const postUser = (data) => api.post('user', data);
    const getItems = (params) => api.get('item', params);
    const getItem = (id, params) => api.get(`item/${id}`, params);
    const getFilters = (params) => api.get('filter', params);
    const getItemVideos = (params) => api.get('item-video', params);
    const getItemVideo = (id, params) => api.get(`item-video/${id}`, params);
    const postUserWatch = (data) => api.post('user-watch', data);
    const getLists = (params) => api.get('list', params);
    const postUserItem = (data) => api.post('user-item', data);
    const putUserItem = (id, data) => api.put(`user-item/${id}`, data);
    const postUserRating = (data) => api.post('user-rating', data);
    const login = (data) => api.post('auth', data);
    const logout = () => api.delete('auth/main');
    const socialAuth = (data) => api.post('social-auth', data);

    return {
        getUser,
        putUser,
        postUser,
        getItems,
        getItem,
        getFilters,
        getItemVideos,
        getItemVideo,
        postUserWatch,
        getLists,
        postUserItem,
        putUserItem,
        postUserRating,
        login,
        logout,
        socialAuth,
        _axiosInstance: api.axiosInstance,
    }
};

export default { create }
