export const list = {
    ID_PLANNED: 1,
    ID_VIEWED: 2,
    ID_ABANDONED: 3,
    ID_WATCH: 4,
};
export const VK_APP_ID = 6070842;
export const IS_MOBILE_VERSION = window.isMobileVersion;
