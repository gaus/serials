import { createStore, applyMiddleware } from 'redux'
import { browserHistory } from 'react-router'
import { routerMiddleware } from 'react-router-redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction'
import rootReducer from './reducers/index'
import sagas from './sagas/index'

const middleware = routerMiddleware(browserHistory)
const sagaMiddleware = createSagaMiddleware()

export default function configureStore() {
    const store = composeWithDevTools(
        applyMiddleware(sagaMiddleware),
        applyMiddleware(middleware)
    )(createStore)(rootReducer)
    sagaMiddleware.run(sagas)

    return store
}
