import React, { Component } from 'react'
import { connect } from 'react-redux'
import Modal from 'react-modal'
import { Creators as UserAction } from '../reducers/user'
import { Creators as SearchAction } from '../reducers/search'
import { initVK } from '../helpers/vk'
import MainPreloader from '../components/preloader/MainPreloader'
import Header from '../components/common/Header'
import Footer from '../components/common/Footer'
import Auth from '../components/common/Auth'
import Registration from '../components/common/Registration'
import NeedAuth from '../components/common/NeedAuth'
import { IS_MOBILE_VERSION } from '../constants'

export class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMenuOpen: false,
        }
    }

    toggleMenu = () => {
        this.setState({isMenuOpen: !this.state.isMenuOpen})
    };

    componentWillReceiveProps (nextProps) {
        if (this.props.routing !== nextProps.routing) {
            this.props.resetSearch()
        }
    }

    render () {
        return <div className={'wrapper' + (IS_MOBILE_VERSION ? ' mobile-version' : '')}>
            <Header
                {...this.props}
                toggleMenu={this.toggleMenu}
                isMenuOpen={this.state.isMenuOpen}
            />
            {this.props.children}
            <Footer {...this.props} />
            <MainPreloader {...this.props.preloader} />
            <Modal
                isOpen={this.props.user.authPopup}
                className='modal'
                overlayClassName='modal-overlay'
                onRequestClose={() => this.props.toggleAuthPopup(false)}
            >
                <Auth
                    errors={this.props.user.errors}
                    submit={(e) => {
                        e.preventDefault();
                        this.props.login(new FormData(e.target))
                    }}
                    close={() => this.props.toggleAuthPopup(false)}
                    vkAuth={this.props.vkAuth}
                />
            </Modal>
            <Modal
                isOpen={this.props.user.registrationPopup}
                className='modal'
                overlayClassName='modal-overlay'
                onRequestClose={() => this.props.toggleRegistrationPopup(false)}
            >
                <Registration
                    errors={this.props.user.errors}
                    submit={(e) => {
                        e.preventDefault();
                        this.props.registration(new FormData(e.target))
                    }}
                    close={() => this.props.toggleRegistrationPopup(false)}
                    vkAuth={this.props.vkAuth}
                />
            </Modal>
            <Modal
                isOpen={this.props.user.needAuthPopup}
                className='modal'
                overlayClassName='modal-overlay'
                onRequestClose={() => this.props.toggleNeedAuthPopup(false)}
            >
                <NeedAuth
                    openAuth={() => this.props.toggleAuthPopup(true)}
                    openRegistration={() => this.props.toggleRegistrationPopup(true)}
                    close={() => this.props.toggleNeedAuthPopup(false)}
                />
            </Modal>
        </div>
    }
}

function mapStateToProps(state) {
    return { ...state }
}

function mapDispatchToProps(dispatch) {
    return {
        toggleAuthPopup: (value) => dispatch(UserAction.toggleAuthPopup(value)),
        toggleRegistrationPopup: (value) => dispatch(UserAction.toggleRegistrationPopup(value)),
        toggleNeedAuthPopup: (value) => dispatch(UserAction.toggleNeedAuthPopup(value)),
        logout: () => dispatch(UserAction.logout()),
        login: (data) => dispatch(UserAction.login(data)),
        registration: (data) => dispatch(UserAction.registration(data)),
        applySearch: (query) => dispatch(SearchAction.applySearch(query)),
        searchItems: (query) => dispatch(SearchAction.search(query)),
        resetSearch: () => dispatch(SearchAction.reset()),
        setSearch: (q) => dispatch(SearchAction.setSearch(q)),
        vkAuth: () => initVK((vk) => {
            vk.Auth.login((response) => {
                response.session && dispatch(UserAction.loginVk(response.session))
            })
        }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)
