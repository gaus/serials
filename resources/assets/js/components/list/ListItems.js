import React from 'react'
import ListItem from './ListItem'
import { IS_MOBILE_VERSION } from '../../constants'

export default ({
    list,
    updateListItem,
}) => {
    if (!list.items || !list.items.length) {
        return null
    }

    return <div>
        <div className="ttl">{list.name}</div>
        <div className="my-list">
            {!IS_MOBILE_VERSION &&
                <div className="my-list-head row">
                    <div className="col col-1">Название</div>
                    <div className="col col-2">{/*Новые серии*/}</div>
                    <div className="col col-3">Просмотрено</div>
                </div>
            }
            {list.items.map(item => {
                return <ListItem
                    key={item.id}
                    item={item}
                    updateListItem={updateListItem}
                />
            })}
        </div>
    </div>
}
