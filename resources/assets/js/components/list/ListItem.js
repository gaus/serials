import React from 'react'
import { Link } from 'react-router'
import { first, findIndex, isNull } from 'lodash'
import { numberOf } from '../../helpers/number'
import Tooltip from '../item/Tooltip'
import Select from '../common/form/Select'
import { IS_MOBILE_VERSION } from '../../constants'

export default ({
    item,
    updateListItem,
}) => {
    const name_original = item.name_original || item.name_en;
    const season = !isNull(item.pivot.season_number)
        ? item.seasons[findIndex(item.seasons, {number: item.pivot.season_number})]
        : first(item.seasons);
    const series = season && (
        !isNull(item.pivot.series_number)
            ? season.series[findIndex(season.series, {number: item.pivot.series_number})]
            : first(season.series)
    );

    return <div className="my-list-item row">
        <div className="col col-1">
            <span className="my-list-item__name">
                <Link to={`/item/${item.id}`}>
                    {item.name}
                    {name_original && name_original !== item.name ? ' / ' + name_original : null}
                </Link>
                <Tooltip item={item} withImage />
            </span>
            <div className="my-list-item__duration">
                { item.season_count } { numberOf(item.season_count, ['сезон ', 'сезона ', 'сезонов ']) }
                <small>{ item.series_count } { numberOf(item.series_count, ['серия', 'серии', 'серий']) }</small>
            </div>
        </div>
        <div className="col col-2">
            {/*<div className="timer">*/}
                {/*<div className="timer__col">*/}
                    {/*<div className="timer__value">3</div>*/}
                    {/*<div className="timer__unit">дня</div>*/}
                {/*</div>*/}
                {/*<div className="timer__col">*/}
                    {/*<div className="timer__value">16</div>*/}
                    {/*<div className="timer__unit">часов</div>*/}
                {/*</div>*/}
                {/*<div className="timer__col">*/}
                    {/*<div className="timer__value">35</div>*/}
                    {/*<div className="timer__unit">минут</div>*/}
                {/*</div>*/}
            {/*</div>*/}
        </div>
        <div className="col col-3">
            <div className="clearfix">
                {item.seasons.length > 0 && <Select
                    className="my-list-item__select"
                    onChange={({target: {value}}) => {
                        updateListItem(item.pivot.id, {
                            season_number: value,
                        })
                    }}
                    items={item.seasons}
                    valueField='number'
                    nameField={(s => `${s.number} ${numberOf(s.number, ['сезон ', 'сезона ', 'сезонов '])}`)}
                    value={season && season.number}
                />}
                {season && season.series.length > 0 && <Select
                    className="my-list-item__select"
                    onChange={({target: {value}}) => {
                        updateListItem(item.pivot.id, {
                            series_number: value,
                        })
                    }}
                    items={season.series}
                    valueField='number'
                    nameField={(s => `${s.number} ${numberOf(s.number, ['серия', 'серии', 'серий'])}`)}
                    value={series && series.number}
                />}
            </div>
        </div>
        <div className="col col-4">
            <Link to={`/item/${item.id}#player`}  className="my-list-item__play">
                <i className="fa fa-play" />
                <span>смотреть</span>
            </Link>
        </div>
    </div>
}
