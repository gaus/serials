import React from 'react'

export default () => {
    return <footer>
        <div className="site-width">
            <div className="copyright">© {(new Date).getFullYear()} - Teleserialchik.ru</div>
        </div>
    </footer>
}
