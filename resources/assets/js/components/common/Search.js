import React, { Component } from 'react'
import { Link } from 'react-router'
import Tooltip from '../item/Tooltip'

export default class Search extends Component {
    constructor() {
        super()
        this.state = {
            isOpen: false,
        }
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.search.list !== nextProps.search.list) {
            nextProps.search.list
                ? this.openSearch()
                : this.closeSearch()
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.searchWrapper && !this.searchWrapper.contains(event.target)) {
            this.closeSearch()
        }
    }

    searchItems = ({target: {value}}) => {
        this.props.setSearch(value)
        this.props.applySearch({
            q: value,
            'per_page': 10,
        })
    }

    openSearch = () => {
        this.setState({isOpen: true})
    }

    closeSearch = () => {
        this.setState({isOpen: false})
    }

    render () {
        const searchItems = (this.props.search.list && this.props.search.list.data) || []
        return <div ref={(ref) => {this.searchWrapper = ref}}>
            <input
                className='search-top__field'
                type='text'
                value={this.props.search.q}
                onInput={this.searchItems}
                onFocus={() => {
                    searchItems.length > 0 && this.openSearch()
                }}
            />
            <button className='search-top__btn'>
                <i className='fa fa-search' />
            </button>
            <div
                className={'search-top__results' + (this.state.isOpen ? '' : ' hide')}
            >
                {searchItems.map(item => {
                    return <div
                        key={item.id}
                        className="search-top__results-item"
                    >
                        <span className="search-top__results-item__name">
                            <Link to={'/item/' + item.id}>
                                {item.name}
                            </Link>
                        </span>
                        <span className="search-top__results-item__year">
                            {item.year_start && `(${item.year_start})`}
                        </span>
                        <Tooltip item={item} withImage leftSide />
                    </div>
                })}
                {searchItems.length === 0 &&
                    <div className="search-top__results-item">
                        <span>ничего не найдено...</span>
                    </div>
                }
            </div>
        </div>
    }
}
