import React from 'react'

export default ({
    vkAuth,
}) => {
    return <div className="soc-auth">
        <span
            className="coc-auth__vk"
            onClick={vkAuth}
        >Использовать аккаунт Вконтакте</span>
    </div>
}
