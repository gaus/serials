import React from 'react'
import SocAuth from './SocAuth'

export default ({
    errors,
    submit,
    close,
    vkAuth,
}) => {
    return <div>
        <span className='modal-close' onClick={close}>×</span>

        <h1 className="main-block-ttl">Авторизация</h1>
        <form className="form" onSubmit={submit}>
            <SocAuth vkAuth={vkAuth} />
            <div className="form-group">
                <label className="form-label">Email</label>
                <input className="form-field" type="text" name="email" />
                {errors && errors.email &&
                    <div className="form-error">{errors.email.join(' ')}</div>
                }
            </div>
            <div className="form-group">
                <label className="form-label">Пароль</label>
                <input className="form-field" type="password" name="password" />
                {errors && errors.password &&
                    <div className="form-error">{errors.password.join(' ')}</div>
                }
            </div>
            <div className="form-group">
                <button className="form-btn" type="submit">Войти</button>
            </div>
        </form>
    </div>
}
