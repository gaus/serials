import React from 'react'

export default ({
    openAuth,
    openRegistration,
    close,
 }) => {
    return <div>
        <span className='modal-close' onClick={close}>×</span>

        <h1 className="main-block-ttl">Зарегистрируйтесь на сайте</h1>
        <div className='need-auth'>
            Для зарегистрированных пользователей доступно:
            <ul>
                <li>Управление списками интересующих вас сериалов. Добавляйте сериалы в список чтобы иметь к ним быстрый доступ.</li>
                <li>Мы запомним какую серию вы смотрели последней и в следующий раз вам не придется вспоминать на чем вы остановились.</li>
            </ul>
            <div className='need-auth__actions'>
                <span
                    onClick={() => {
                        close();
                        openAuth()
                    }}
                >Вход</span>
                <span
                    onClick={() => {
                        close();
                        openRegistration()
                    }}
                >Регистрация</span>
            </div>
        </div>
    </div>
}
