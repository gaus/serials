import React from 'react'
import { get, isFunction } from 'lodash'

export default ({
    items = [],
    valueField = 'value',
    nameField = 'name',
    enableField,
    value,
    onChange,
    ...params,
}) => {
    return <span className='select-wrap'>
        <select
            {...params}
            value={value}
            onChange={onChange}
        >
            {items.map((item) => {
                const value = isFunction(valueField) ? valueField(item) : get(item, valueField);

                return <option
                    key={value}
                    value={value}
                    disabled={enableField && !get(item, enableField)}
                >{isFunction(nameField) ? nameField(item) : get(item, nameField)}</option>
            })}
        </select>
    </span>
}
