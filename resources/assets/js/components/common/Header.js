import React from 'react'
import { Link } from 'react-router'
import Search from './Search'
import { IS_MOBILE_VERSION } from '../../constants'

export default (props) => {
    const {
        user: { data: user },
        isMenuOpen,
        toggleMenu,
    } = props;
    const isAuth = user && user.id;

    if (IS_MOBILE_VERSION) {
        return <header className={isMenuOpen ? 'menu-open' : ''}>
            <ul className="menu-top">
                <li key="menu" className="menu-top__item">
                    <a
                        className='menu-toggle'
                        onClick={(e) => {
                            e.preventDefault();
                            toggleMenu()
                        }}
                        href="#"
                    ><i className='fa fa-navicon' /></a>
                </li>
                <li key="search" className="search-top">
                    <Search {...props} />
                </li>
            </ul>
            {isMenuOpen &&
                <div className='mobile-side-menu-wrap'>
                    <div className='mobile-side-menu'>
                        <Link to="/" onClick={toggleMenu}>
                            <img className='logo' src='/images/logo.png' />
                        </Link>
                        <ul>
                            {!isAuth &&
                                <li key="reg" className="mobile-side-menu__item">
                                    <a
                                        href='#'
                                        onClick={(e) => {
                                            e.preventDefault();
                                            props.toggleRegistrationPopup(true)
                                        }}
                                    >Регистрация</a>
                                </li>
                            }
                            {!isAuth &&
                                <li key="auth" className="mobile-side-menu__item">
                                    <a
                                        href='#'
                                        onClick={(e) => {
                                            e.preventDefault();
                                            props.toggleAuthPopup(true)
                                        }}
                                    >Вход</a>
                                </li>
                            }
                            {isAuth &&
                            <li key="my" className="mobile-side-menu__item">
                                <Link to="/list" onClick={toggleMenu}>Мой список</Link>
                            </li>
                            }
                            {isAuth &&
                                <li key="logout" className="mobile-side-menu__item">
                                    <a
                                        className='logout'
                                        onClick={(e) => {
                                            e.preventDefault();
                                            props.logout()
                                        }}
                                        href="#"
                                    ><i className='fa fa-sign-out' /> Выйти</a>
                                </li>
                            }
                        </ul>
                    </div>
                </div>
            }
        </header>
    }

    return <header>
        <ul className="menu-top site-width">
            <li key="home" className="menu-top__item">
                <Link to="/">
                    <img className='logo' src='/images/logo.png' />
                </Link>
            </li>
            <li key="search" className="search-top">
                <Search {...props} />
            </li>
            {!isAuth &&
                <li key="reg" className="menu-top__item pull-right">
                    <a
                        href='#'
                        onClick={(e) => {
                            e.preventDefault();
                            props.toggleRegistrationPopup(true)
                        }}
                    >Регистрация</a>
                </li>
            }
            {!isAuth &&
                <li key="auth" className="menu-top__item pull-right">
                    <a
                        href='#'
                        onClick={(e) => {
                            e.preventDefault();
                            props.toggleAuthPopup(true)
                        }}
                    >Вход</a>
                </li>
            }
            {isAuth &&
                <li key="logout" className="menu-top__item pull-right">
                    <a
                        className='logout'
                        onClick={(e) => {
                            e.preventDefault();
                            props.logout()
                        }}
                        href="#"
                    ><i className='fa fa-sign-out' /></a>
                </li>
            }
            {isAuth &&
                <li key="my" className="menu-top__item pull-right">
                    <Link to="/list">Мой список</Link>
                </li>
            }
        </ul>
    </header>
}
