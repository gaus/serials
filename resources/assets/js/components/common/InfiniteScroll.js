import React, { Component } from 'react'

export default class InfiniteScroll extends Component {
    componentDidMount() {
        this.attachScrollListener()
    }

    componentDidUpdate () {
        this.attachScrollListener()
    }

    componentWillUnmount() {
        this.detachScrollListener()
    }

    attachScrollListener = () => {
        if (!this.props.hasMore || this.props.loadingMore) {
            return
        }
        window.addEventListener('scroll', this.scroll, true);
        window.addEventListener('resize', this.scroll, true);
        this.scroll()
    };

    detachScrollListener = () => {
        window.removeEventListener('scroll', this.scroll, true);
        window.removeEventListener('resize', this.scroll, true)
    };

    scroll = () => {
        let bottomPosition = this.bottomPosition();
        if (bottomPosition < 800) {
            this.detachScrollListener();
            this.props.hasMore && !this.props.loadingMore && this.props.loadMore()
        }
    };

    bottomPosition = () => {
        let windowScrollTop = (window.pageYOffset !== undefined)
            ? window.pageYOffset
            : (document.documentElement || document.body.parentNode || document.body).scrollTop;
        let totalHeight = document.documentElement.scrollHeight;

        return totalHeight - windowScrollTop - window.innerHeight
    };

    render () {
        return <div>
            {this.props.children}
            {this.props.hasMore && this.props.loader}
        </div>
    }
}
