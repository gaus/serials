import React from 'react'
import { Link } from 'react-router'
import { numberOf } from '../../helpers/number'
import Tooltip from './Tooltip'

export default ({
    item,
    add,
}) => {
    const videoTypes = [];
    if (item.video_types) {
        for (let key in item.video_types) {

            let label = '';
            if (item.video_types[key].id === 1) { label = 'voice' }
            if (item.video_types[key].id === 2) { label = 'sub' }
            if (item.video_types[key].id === 3) { label = 'origin' }
            videoTypes.push(
                <span
                    key={item.video_types[key].id}
                    className={"item-block__label item-block__label--"+label}
                >{item.video_types[key].name}</span>
            )
        }
    }

    return (
        <div className="item-block">
            <div className="item-block__image">
                <Link to={`/item/${item.id}`}><img src={item.image_src} /></Link>
            </div>
            <div className="item-block__name">
                <Link className="link" to={`/item/${item.id}`}>{item.name}</Link>
            </div>
            <div className="item-block__duration">
                {item.season_count} {numberOf(item.season_count, ['сезон ', 'сезона ', 'сезонов '])}
                <small>
                    {item.series_count} {numberOf(item.series_count, ['серия', 'серии', 'серий'])}
                </small>
            </div>
            {/*<div className="item-block__labels">{videoTypes}</div>*/}
            <Tooltip {...{item, add, withImage: false}} />
        </div>
    )
}
