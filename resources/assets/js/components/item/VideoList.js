import React from 'react'

export default ({
    videos,
    activeVideo,
    selectVideo,
}) => {

    return <div className="player__list">
        <div className={'player__list-block player__list-block--single'}>
            <div className="player__list-block-ttl">
                Варианты видео
            </div>
            <div className="player__list-block-items">
                {videos.map(video => {
                    const name = [];
                    video.source && name.push(video.source.name);
                    video.author && name.push(video.author.name);
                    !name.length && name.push('Не определено');
                    return <div
                        key={video.id}
                        className={'player__list-block-item'+(video.id === activeVideo ? ' active' : '')}
                    >
                        <a
                            href='#'
                            onClick={(e) => {
                                e.preventDefault();
                                selectVideo(video)
                            }}
                        >
                            {name.join(' - ')}
                        </a>
                    </div>
                })}
            </div>
        </div>
    </div>
}
