import React from 'react'
import { Link } from 'react-router'
import { numberOf } from '../../helpers/number'
import { IS_MOBILE_VERSION } from '../../constants'

export default ({
    item,
    add,
    withImage = true,
    leftSide = false,
}) => {
    if (IS_MOBILE_VERSION && withImage) {
        return null
    }
    const attrs = [];
    const classes = [
        'item-tooltip',
        `item-tooltip--${leftSide ? 'left' : 'right'}`,
    ];
    withImage && classes.push('item-tooltip--image');
    let {
        description,
    } = item;
    let rating = item.rating || item.rating_kp || item.rating_imdb;
    rating && (rating = Math.floor(rating)+'.'+Math.floor((rating%1)*10));
    if (description && description.length > 320) {
        description = description.substr(0, 300) + '...'
    }
    if (item.year_start) {
        attrs.push(
            <div key="year" className="item-tooltip__props-row">
                <span>Год:</span>
                {item.year_start === item.year_complete
                    ? item.year_start
                    : item.year_start + ' - ' + (item.year_complete || '...')
                }
            </div>
        )
    }
    for (let key in item.attrs) {
        let attr = item.attrs[key];
        let attrValues = [];
        for (let key2 in attr.values) {
            attrValues.push(attr.values[key2].value)
        }
        attrs.push(
            <div
                key={attr.id}
                className="item-tooltip__props-row"
            >
                <span>{ attr.name }:</span> { attrValues.join(', ') }
            </div>
        )
    }
    if (IS_MOBILE_VERSION && !withImage) {
        attrs.push(
            <div
                key='duration'
                className="item-tooltip__props-row"
            >
                <span>Продолжительность:</span>
                {item.season_count} {numberOf(item.season_count, ['сезон ', 'сезона ', 'сезонов '])}
                <small>
                    {item.series_count} {numberOf(item.series_count, ['серия', 'серии', 'серий'])}
                </small>
            </div>
        )
    }
    const actionBtn = (item.user_item || !add)
        ? <Link
            to={`/item/${item.id}#player`}
            className="item-btn item-btn--play"
        ><i className="fa fa-play" />Смотреть</Link>
        : <span
            className="item-btn item-btn--add"
            onClick={() => add(item.id)}
        ><i className="fa fa-plus" />Добавить в список</span>;

    return withImage
        ? <div className={classes.join(' ')}>
            <div className="item-tooltip-content row">
                {/*<i className="item-tooltip__close fa fa-close" />*/}
                <div className="col-1">
                    <Link to={'/item/' + item.id} className="item-tooltip__image">
                        <img src={item.image_src} />
                    </Link>
                </div>
                <div className="col-2">
                    <div className="item-tooltip__name">
                        <Link className="link" to={'/item/' + item.id}>{item.name}</Link>
                    </div>
                    <div className="item-tooltip__description">{description}</div>
                    <div className="item-tooltip__props">{attrs}</div>
                    <div className="item-tooltip__group">
                        {actionBtn}
                        <span className="item-tooltip__rating">{rating}</span>
                    </div>
                </div>
            </div>
        </div>
        : <div className={classes.join(' ')}>
            <div className="item-tooltip-content">
                {/*<i className="item-tooltip__close fa fa-close" />*/}
                <div className="item-tooltip__name">
                    <Link className="link" to={'/item/' + item.id}>{item.name}</Link>
                </div>
                <div className="item-tooltip__description">{description}</div>
                <div className="item-tooltip__props">{attrs}</div>
                <div className="item-tooltip__group">
                    {actionBtn}
                    <span className="item-tooltip__rating">{rating}</span>
                </div>
            </div>
        </div>
}
