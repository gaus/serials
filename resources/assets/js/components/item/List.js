import React from 'react'
import ListItem from './ListItem'
import Chopick from '../common/Chopick'

export default ({
    items = [],
    add,
}) => {
    return <div className="block-items">
        {items.map((item, i) => {
            return [
                ((i+1)%48 === 1 && i > 1 && <Chopick position='home-inlist' />),
                <ListItem key={item.id} {...{item, add}} />
            ]
        })}
    </div>
}
