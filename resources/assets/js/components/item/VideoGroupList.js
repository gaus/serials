import React, { Component } from 'react'

export default class VideoGroupList extends Component {
    constructor (props) {
        super(props);
        this.state = {
            activeGroup: undefined,
        }
    }

    render () {
        const {
            videos,
            activeVideo,
            selectVideo,
        } = this.props;
        const groups = {};
        const list = [];
        videos.forEach(video => {
            if (!groups[video.video_type_id]) {
                groups[video.video_type_id] = {
                    id: video.video_type_id,
                    name: video.video_type.name,
                    videos: [],
                }
            }
            groups[video.video_type_id].videos.push(video)
        });
        for (let key in groups) {
            list.push(groups[key])
        }
        const activeGroup = groups[this.state.activeGroup]
            ? this.state.activeGroup
            : list[0] && list[0].id;

        return <div className="player__list">
            {list.map(group => {
                const isActiveGroup = activeGroup === group.id;

                return <div
                    key={group.id}
                    className={'player__list-block'+(isActiveGroup ? ' active' : '')}
                >
                    <div
                        className="player__list-block-ttl"
                        onClick={() => this.setState({activeGroup: group.id})}
                    >
                        {group.name}
                        <i className={isActiveGroup ? 'fa fa-angle-down' : 'fa fa-angle-left'} />
                    </div>
                    <div className="player__list-block-items">
                        {group.videos.map(video => {
                            const name = [];
                            video.source && name.push(video.source.name);
                            video.author && name.push(video.author.name);
                            !name.length && name.push('Не определено');
                            return <div
                                key={video.id}
                                className={'player__list-block-item'+(video.id === activeVideo ? ' active' : '')}
                            >
                                <a
                                    href='#'
                                    onClick={(e) => {
                                        e.preventDefault();
                                        selectVideo(video)
                                    }}
                                >
                                    {name.join(' - ')}
                                </a>
                            </div>
                        })}
                    </div>
                </div>
            })}
        </div>
    }
}
