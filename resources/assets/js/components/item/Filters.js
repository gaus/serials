import React, { Component } from 'react'
import { isUndefined } from 'lodash'
import classNames from 'classnames'
import Select from '../common/form/Select'
import { IS_MOBILE_VERSION } from '../../constants'

export default class Filters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeFilter: undefined,
        }
    }

    setActiveFilter = (value) => {
        IS_MOBILE_VERSION && this.setState({activeFilter: value})
    };

    render() {
        const {
            filter = [],
            query,
            apply,
            isOpenFilterPopup,
            toggleFilterPopup,
        } = this.props;
        const allowed = filter.map(f => f.value);
        const selected = {};
        for (let key in query) {
            if (allowed.includes(key)) {
                selected[key] = query[key].split(',')
            }
        }

        return <div>
            <div key='0' className='block-filter filter filter-top'>
                <Select
                    className='filter-select'
                    items={[
                        {value: 'rating', name: 'По рейтингу'},
                        {value: 'started_at', name: 'По новизне'},
                    ]}
                    value={query.sort}
                    onChange={({target: {value}}) => apply({sort: value})}
                />
                {IS_MOBILE_VERSION &&
                    <span
                        className='filter-menu-btn'
                        onClick={(e) => {
                            e.stopPropagation();
                            toggleFilterPopup()
                        }}
                    >
                        {isOpenFilterPopup && <span><i className='fa fa-check'/> Применить</span>}
                        {!isOpenFilterPopup && <span><i className='fa fa-sliders'/> Фильтр</span>}
                    </span>
                }
            </div>
            {(!IS_MOBILE_VERSION || isOpenFilterPopup) &&
                <div className='filter-akordeon'>
                    {filter.map(f => {
                        const filterKey = String(f.value);
                        const active = this.state.activeFilter === f.value;

                        return <div
                            key={f.value}
                            className={classNames('block-filter', {'active': active})}
                        >
                            <div
                                className='filter-ttl'
                                onClick={() => this.setActiveFilter(active ? undefined : f.value)}
                            >
                                {f.name}
                                {IS_MOBILE_VERSION &&
                                    <i className={classNames('fa', active ? 'fa-angle-up' : 'fa-angle-down')} />
                                }
                            </div>
                            <div className="filter">
                                {f.values.map(v => {
                                    const valueKey = String(v.value);
                                    const isSelected = selected[filterKey]
                                        && selected[filterKey].includes(valueKey);
                                    const classes = ['fa'];
                                    isSelected
                                        ? classes.push('fa-check-square-o')
                                        : classes.push('fa-square-o');

                                    return <div
                                        key={v.value}
                                        className="filter__item"
                                        onClick={() => {
                                            const params = {};
                                            const values = isUndefined(selected[filterKey])
                                                ? []
                                                : selected[filterKey];
                                            isSelected
                                                ? values.splice(values.indexOf(valueKey), 1)
                                                : values.push(valueKey);
                                            params[filterKey] = values.length > 0
                                                ? values.join(',')
                                                : undefined;
                                            apply(params)
                                        }}
                                    >
                                        <i className={classes.join(' ')}/>
                                        {v.name}
                                    </div>
                                })}
                            </div>
                        </div>
                    })}
                </div>
            }
        </div>
    }
}
