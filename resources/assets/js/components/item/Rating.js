import React, { Component } from 'react'
import { IS_MOBILE_VERSION } from '../../constants'

export default class Rating extends Component {
    constructor () {
        super();
        this.state = {
            ratingHover: undefined,
        }
    }

    hoverStar = (rating) => {
        this.setState({ratingHover: rating})
    };

    saveRating = (rating) => {
        this.props.save({
            item_id: this.props.item.id,
            user_id: (this.props.user && this.props.user.id) || null,
            value: rating,
        });
    };

    ratingFormat (rating) {
        return Math.floor(rating)+'.'+Math.floor((rating%1)*10)
    }

    render () {
        let { rating, rating_kp, rating_imdb } = this.props.item;
        const userRating = this.props.item.user_rating && this.props.item.user_rating.value;
        const { ratingHover } = this.state;
        rating = ratingHover || rating || userRating;
        if (rating) rating = this.ratingFormat(rating);
        if (rating_kp) rating_kp = this.ratingFormat(rating_kp);
        if (rating_imdb) rating_imdb = this.ratingFormat(rating_imdb);
        var stars = [];
        for (let i = 0; i < 10; i+=2) {
            let className = '';
            let step = rating - i;
            if (!rating || step <= 0) {
                className = 'fa fa-star-o'
            } else if (step <= 1) {
                className = 'fa fa-star-half-full'
            } else {
                className = 'fa fa-star'
            }
            stars.push(<i
                key={i}
                className={className}
            >
                <span
                    onMouseEnter={() => this.hoverStar(i+1)}
                    onMouseLeave={() => this.hoverStar(undefined)}
                    onClick={() => this.saveRating(i+1)}
                />
                <span
                    onMouseEnter={() => this.hoverStar(i+2)}
                    onMouseLeave={() => this.hoverStar(undefined)}
                    onClick={() => this.saveRating(i+2)}
                />
            </i>)
        }

        return <div className="item-rating">
            <div className="item-rating__stars">
                {stars}
                <span className="item-rating__stars-value">{rating || '-'}</span>
                {IS_MOBILE_VERSION && <select
                    className="item-rating__select"
                    onChange={({target: {value}}) => this.saveRating(value)}
                >
                    {[1,2,3,4,5,6,7,8,9,10].map(i => {
                        return <option value={i}>{i}</option>
                    })}
                </select>}
            </div>
            {userRating && <div className="item-rating__user" title='Мой рейтинг'>
                <i className='fa fa-user' />
                {userRating}
            </div>}
            {rating_kp && <div className="item-rating__kp" title='Рейтинг на сайте kinopoisk.ru'>
                {rating_kp}
            </div>}
            {rating_imdb && <div className="item-rating__imdb" title='Рейтинг на сайте imdb.com'>
                {rating_imdb}
            </div>}
        </div>
    }
}
