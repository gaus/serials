import React, { Component } from 'react'
import { findIndex, first } from 'lodash'
import { list as listConstant } from '../../constants'
import { sortVideoPriority } from '../../helpers/array'
import VideoList from './VideoList'
import Share from './Share'
import Select from '../common/form/Select'
import Chopick from '../common/Chopick'
import { IS_MOBILE_VERSION } from '../../constants'

export default class Player extends Component {
    constructor (props) {
        super(props);
        this.state = {
            selectedSeason: undefined,
            selectedSeries: undefined,
        }
    }

    componentDidMount() {
        this.props.fetchVideo('next', {item_id: this.props.params.id})
    }

    componentWillUpdate (nextProps, nextState) {
        // При ввыборе другого сезона или серии загружаем новый список видео
        if (
            nextState.selectedSeason
            && nextState.selectedSeries
            && (
                this.state.selectedSeason !== nextState.selectedSeason
                || this.state.selectedSeries !== nextState.selectedSeries
            )
        ) {
            this.props.fetchVideos({
                item_season_id: nextState.selectedSeason,
                item_series_id: nextState.selectedSeries,
            })
        }
        // При загрузке видео устанавливаем выбор соответствующего сезона и серии
        if (this.props.video.data !== nextProps.video.data && nextProps.video.data) {
            this.setState({
                selectedSeason: nextProps.video.data.item_season_id,
                selectedSeries: nextProps.video.data.item_series_id,
            })
        }
        // При загрузке нового списка видео, выбираем подходящее видео
        if (
            this.props.video.list !== nextProps.video.list
            && nextProps.video.list
            && (
                !nextProps.video.data
                || findIndex(nextProps.video.list.data, {id: nextProps.video.data.id}) === -1
            )
        ) {
            this.props.setVideo(
                first(
                    nextProps.video.data
                        ? sortVideoPriority(nextProps.video.data, nextProps.video.list.data)
                        : nextProps.video.list.data
                )
            )
        }
    }

    selectedSeason () {
        const item = this.props.item.data;
        const selectedId = this.state.selectedSeason;

        return item
            && (
                (selectedId && item.seasons[findIndex(item.seasons, {id: selectedId})])
                || first(item.seasons)
            )
    }

    selectedSeries () {
        const season = this.selectedSeason();
        const selectedId = this.state.selectedSeries;

        return season
            && (
                (selectedId && season.series[findIndex(season.series, {id: selectedId})])
                || first(season.series)
            )
    }

    changeSeason = ({target: {value}}) => {
        value = Number(value);
        const item = this.props.item.data;
        const season = item.seasons[findIndex(item.seasons, {id: value})];
        const series = first(season.series);

        this.setState({
            selectedSeason: value,
            selectedSeries: series && series.id,
        })
    };

    changeSeries = ({target: {value}}) => {
        this.setState({selectedSeries: Number(value)})
    };

    watchVideo = () => {
        if (this.props.video.data) {
            if (this.props.user.data && this.props.user.data.id) {
                // Отмечаем видео как просмотренное
                this.props.watchVideo({
                    user_id: this.props.user.data.id,
                    item_video_id: this.props.video.data.id,
                    item_id: this.props.video.data.item_id,
                    item_series_id: this.props.video.data.item_series_id,
                });
                // Добовляем в список или редактируем инфу в имеющемся
                const season = this.selectedSeason();
                const series = this.selectedSeries();
                if (this.props.item.data.user_item && this.props.item.data.user_item.id) {
                    this.props.updateListItem(
                        this.props.item.data.user_item.id,
                        {
                            season_number: season && season.number,
                            series_number: series && series.number,
                        }
                    )
                } else {
                    this.props.addListItem({
                        user_id: this.props.user.data.id,
                        item_id: this.props.video.data.item_id,
                        list_id: listConstant.ID_WATCH,
                        season_number: season && season.number,
                        series_number: series && series.number,
                    })
                }
            }
            // Загружаем следующее видео
            this.props.fetchVideo('next', {video_id: this.props.video.data.id})
        }
    };

    render () {
        const {
            item: {data: item = {}},
            video: {
                data: video,
                list = {},
            },
        } = this.props;
        if (!item.id) {
            return null
        }

        const {
            data: videos = [],
        } = list;
        const season = this.selectedSeason();
        const series = this.selectedSeries();

        if (IS_MOBILE_VERSION) {
            return <div id='player' className="player-page player">
                <div className="player__select-block">
                    <div className="clearfix">
                        {item.seasons.length > 0 && <Select
                            className="player__select"
                            onChange={this.changeSeason}
                            items={item.seasons}
                            valueField='id'
                            nameField={(season => `${season.number} сезон`)}
                            enableField='has_video'
                            value={season && season.id}
                        />}
                        {season && season.series.length > 0 && <Select
                            className="player__select"
                            onChange={this.changeSeries}
                            items={season && season.series}
                            valueField='id'
                            nameField={(s => `${s.number} серия`)}
                            enableField='has_video'
                            value={series && series.id}
                        />}
                    </div>
                </div>
                <div className='mobile-iframe-contenr'>
                    <iframe
                        src={ (video && video.src) || '' }
                        frameBorder='0'
                        scrolling='no'
                        allowFullScreen
                    />
                </div>
                <div className="player__select-block">
                    {video && <div className="player__check" onClick={this.watchVideo}>
                        <i className="fa fa-check"/> Просмотрено
                    </div>}
                </div>
                <Share
                    url={window.location.href}
                    title={`Смотреть сериал "${item.name_ru || item.name_en || item.name_original}" все серии онлайн`}
                    description={
                        item.description
                        && (item.description.length > 200
                                ? `${item.description.substr(0, 200)}...`
                                : item.description
                        )
                    }
                    image={item.image_src}
                />
                <VideoList
                    videos={videos}
                    activeVideo={video && video.id}
                    selectVideo={this.props.setVideo}
                />
            </div>
        }

        return <div id='player' className="player-page site-width">
            <div className="player row">
                <div className="col-1">
                    <iframe
                        src={ (video && video.src) || '' }
                        frameBorder="0"
                        scrolling="no"
                        allowFullScreen
                    />
                </div>

                <div className="col-2">
                    <div className="player__select-block">
                        <div className="clearfix">
                            {item.seasons.length > 0 && <Select
                                className="player__select"
                                onChange={this.changeSeason}
                                items={item.seasons}
                                valueField='id'
                                nameField={(season => `${season.number} сезон`)}
                                enableField='has_video'
                                value={season && season.id}
                            />}
                            {season && season.series.length > 0 && <Select
                                className="player__select"
                                onChange={this.changeSeries}
                                items={season && season.series}
                                valueField='id'
                                nameField={(s => `${s.number} серия`)}
                                enableField='has_video'
                                value={series && series.id}
                            />}
                        </div>

                        {video && <div className="player__check" onClick={this.watchVideo}>
                            <i className="fa fa-check"/> Просмотрено
                        </div>}
                    </div>
                    <Share
                        url={window.location.href}
                        title={`Смотреть сериал "${item.name_ru || item.name_en || item.name_original}" все серии онлайн`}
                        description={
                            item.description
                                && (item.description.length > 200
                                    ? `${item.description.substr(0, 200)}...`
                                    : item.description
                                )
                        }
                        image={item.image_src}
                    />
                    <VideoList
                        videos={videos}
                        activeVideo={video && video.id}
                        selectVideo={this.props.setVideo}
                    />
                    <Chopick position='item-player' />
                </div>
            </div>
        </div>
    }
}
