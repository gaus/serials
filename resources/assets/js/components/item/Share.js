import React from 'react'
import qs from 'querystringify'

export default ({
    url = '',
    title = '',
    description = '',
    image = '',
    noparse = 'true',
}) => {
    description = description || title
    return <div className='soc-share'>
        <span className='soc-share-label'>Поделиться:</span>
        <a
            className='soc-icon soc-icon--vk'
            target='_blank'
            href={
                'http://vkontakte.ru/share.php'
                + qs.stringify({url, title, description, image, noparse}, true)
            }
        />
        <a
            className='soc-icon soc-icon--fb'
            target='_blank'
            href={
                'http://www.facebook.com/sharer.php'
                + qs.stringify({
                    's': '100',
                    'p[url]': url,
                    'p[title]': title,
                    'p[summary]': description,
                    'p[images][0]': image,
                }, true)
            }
        />
        <a
            className='soc-icon soc-icon--tw'
            target='_blank'
            href={
                'https://twitter.com/intent/tweet'
                + qs.stringify({
                    url,
                    'text': title,
                }, true)
            }
        />
        <a
            className='soc-icon soc-icon--ok'
            target='_blank'
            href={
                'https://connect.ok.ru/offer'
                + qs.stringify({
                    url,
                    title,
                    'imageUrl': image,
                }, true)
            }
        />
    </div>
}
