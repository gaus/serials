import React from 'react'
import { numberOf } from '../../helpers/number'

export default ({
    item,
}) => {
    const attrs = []
    if (item.year_start) {
        attrs.push({
            id: 'year',
            name: 'Год',
            value: item.year_start === item.year_complete
                ? item.year_start
                : item.year_start + ' - ' + (item.year_complete || '...'),
        })
    }
    if (item.season_count || item.series_count) {
        let seasons = item.season_count
            ? `${item.season_count} ${numberOf(item.season_count, ['сезон ', 'сезона ', 'сезонов '])}`
            : ''
        let series = item.series_count
            ? `${item.series_count} ${numberOf(item.series_count, ['серия', 'серии', 'серий'])}`
            : ''
        attrs.push({
            id: 'length',
            name: 'Продолжительность',
            value: seasons && series ? `${seasons} (${series})` : (seasons || series),
        })
    }
    for (let key in item.attrs_all) {
        let attr = item.attrs_all[key]
        attrs.push({
            id: attr.id,
            name: attr.name,
            value: attr.values.map(attr => attr.value).join(', '),
        })
    }
    if (item.duration) {
        attrs.push({
            id: 'duration',
            name: 'Время',
            value: `${item.duration} мин.`,
        })
    }

    return <div className="item-info">
        <div className="item-info__props">
            {attrs.map(attr => {
                return <div key={attr.id} className="item-info__props-row">
                    <span>{attr.name}:</span> {attr.value}
                </div>
            })}
        </div>
        <div className="item-info__description">{item.description}</div>
    </div>
}
