import React from 'react'
import { findIndex } from 'lodash'

export default ({
    lists = [],
    active,
    addListItem,
}) => {
    const activeList = active && lists[findIndex(lists, {id: active})]

    return <div className="item-actions">
        {activeList &&
            <span className="item-actions__btn">
                <i className="fa fa-check" /> {activeList.name}
            </span>
        }
        {!activeList &&
            <span
                className="item-actions__btn"
                onClick={() => addListItem()}
            ><i className="fa fa-plus" /> Добавить в список</span>
        }
        <span className="item-actions__toggle"><i className="fa fa-chevron-down" /></span>
        <ul className="item-actions__list">
            {lists.map((list) => {
                return <li
                    key={list.id}
                    className={'item-actions__list-item'+(list.id === active ? ' active' : '')}
                    onClick={() => addListItem(list.id)}
                >{list.name}</li>
            })}
        </ul>
    </div>
}
