import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
    start: [],
    stop: [],
    remove: [],
}, { prefix: 'preloader/' })

export const INITIAL_STATE = {
    started: false,
    stopped: false,
}

export const start = (state) => {
    return {...state, started: true, stopped: false}
}

export const stop = (state) => {
    return {...state, stopped: true}
}

export const remove = (state) => {
    return {...state, started: false, stopped: false}
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.START]: start,
    [Types.STOP]: stop,
    [Types.REMOVE]: remove,
})
