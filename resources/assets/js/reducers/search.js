import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
    setSearch: ['q'],
    search: ['query'],
    applySearch: ['query'],
    successSearch: ['data'],
    failSearch: ['errors'],
    reset: [],
}, { prefix: 'search/' })

export const INITIAL_STATE = {
    isFetching: false,
    q: '',
    list: undefined,
    errors: undefined,
}

export const setSearch = (state, { q }) => {
    return {...state, q: q}
}

export const search = (state) => {
    return {...state, isFetching: true}
}

export const applySearch = (state) => {
    return {...state}
}

export const successSearch = (state, { data }) => {
    return {...state, ...{list: data}, isFetching: false}
}

export const failSearch = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const reset = (state) => {
    return {...INITIAL_STATE}
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.SET_SEARCH]: setSearch,
    [Types.SEARCH]: search,
    [Types.APPLY_SEARCH]: applySearch,
    [Types.SUCCESS_SEARCH]: successSearch,
    [Types.FAIL_SEARCH]: failSearch,
    [Types.RESET]: reset,
})
