import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
    fetchItems: ['query'],
    successFetchItems: ['data'],
    failFetchItems: ['errors'],
    fetchMoreItems: ['query'],
    successFetchMoreItems: ['data'],
    failFetchMoreItems: ['errors'],
    fetchItem: ['id'],
    successFetchItem: ['data'],
    failFetchItem: ['errors'],
    fetchFilters: ['query'],
    successFetchFilters: ['data'],
    failFetchFilters: ['errors'],
    applyFilter: ['query'],
    successAddListItem: ['data'],
    saveUserRating: ['data'],
    successSaveUserRating: ['data'],
    failSaveUserRating: ['errors'],
    resetData: [],
    resetList: [],
}, { prefix: 'item/' })

export const INITIAL_STATE = {
    isFetching: false,
    isListFetching: false,
    isFiltersFetching: false,
    listQuery: undefined,
    list: undefined,
    data: undefined,
    filter: undefined,
    errors: undefined,
}

export const fetchItems = (state, { query }) => {
    return {...state, ...{listQuery: query}, isListFetching: true}
}

export const successFetchItems = (state, { data }) => {
    return {...state, ...{list: data}, isListFetching: false}
}

export const failFetchItems = (state, { errors }) => {
    return {...state, ...{errors}, isListFetching: false}
}

export const fetchMoreItems = (state) => {
    return {...state, isListFetching: true}
}

export const successFetchMoreItems = (state, { data }) => {
    const oldData = {...state.list}
    const newData = {...data}
    newData.data = oldData.data.concat(newData.data)
    return {...state, ...{list: newData}, isListFetching: false}
}

export const failFetchMoreItems = (state, { errors }) => {
    return {...state, ...{errors}, isListFetching: false}
}

export const fetchItem = (state) => {
    return {...state, isFetching: true}
}

export const successFetchItem = (state, { data }) => {
    return {...state, ...{data}, isFetching: false}
}

export const failFetchItem = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const fetchFilters = (state) => {
    return {...state, isFiltersFetching: true}
}

export const successFetchFilters = (state, { data }) => {
    return {...state, ...{filter: data}, isFiltersFetching: false}
}

export const failFetchFilters = (state, { errors }) => {
    return {...state, ...{errors}, isFiltersFetching: false}
}

export const applyFilter = (state) => {
    return {...state}
}

export const successAddListItem = (state, { data }) => {
    const newState = {...state}
    if (newState.list && newState.list.data.length > 0) {
        for (let key in newState.list.data) {
            newState.list.data[key].id === data.item_id
                && (newState.list.data[key].user_item = {...data})
        }
    }
    if (newState.data) {
        newState.data.id === data.item_id
        && (newState.data.user_item = {...data})
    }

    return newState
}

export const saveUserRating = (state) => {
    return {...state, isFetching: true}
}

export const successSaveUserRating = (state, { data }) => {
    const newState = {...state}
    if (newState.list && newState.list.data.length > 0) {
        for (let key in newState.list.data) {
            newState.list.data[key].id === data.item_id
            && (newState.list.data[key].user_rating = {...data})
        }
    }
    if (newState.data) {
        newState.data.id === data.item_id
        && (newState.data.user_rating = {...data})
    }

    return newState
}

export const failSaveUserRating = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const resetData = (state) => {
    return {...state, data: undefined, errors: undefined, isFetching: false}
}

export const resetList = (state) => {
    return {...state,
        listQuery: undefined,
        list: undefined,
        filter: undefined,
        errors: undefined,
        isListFetching: false,
        isFetching: false,
    }
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.FETCH_ITEMS]: fetchItems,
    [Types.SUCCESS_FETCH_ITEMS]: successFetchItems,
    [Types.FAIL_FETCH_ITEMS]: failFetchItems,
    [Types.FETCH_MORE_ITEMS]: fetchMoreItems,
    [Types.SUCCESS_FETCH_MORE_ITEMS]: successFetchMoreItems,
    [Types.FAIL_FETCH_MORE_ITEMS]: failFetchMoreItems,
    [Types.FETCH_ITEM]: fetchItem,
    [Types.SUCCESS_FETCH_ITEM]: successFetchItem,
    [Types.FAIL_FETCH_ITEM]: failFetchItem,
    [Types.FETCH_FILTERS]: fetchFilters,
    [Types.SUCCESS_FETCH_FILTERS]: successFetchFilters,
    [Types.FAIL_FETCH_FILTERS]: failFetchFilters,
    [Types.APPLY_FILTER]: applyFilter,
    [Types.SUCCESS_ADD_LIST_ITEM]: successAddListItem,
    [Types.SAVE_USER_RATING]: saveUserRating,
    [Types.SUCCESS_SAVE_USER_RATING]: successSaveUserRating,
    [Types.FAIL_SAVE_USER_RATING]: failSaveUserRating,
    [Types.RESET_DATA]: resetData,
    [Types.RESET_LIST]: resetList,
})
