import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

export default combineReducers({
    routing: routerReducer,
    preloader: require('./preloader').reducer,
    user: require('./user').reducer,
    item: require('./item').reducer,
    search: require('./search').reducer,
    video: require('./video').reducer,
    list: require('./list').reducer,
})
