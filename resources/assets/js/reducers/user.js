import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
    loginVk: ['data'],
    login: ['data'],
    successLogin: ['data'],
    failLogin: ['errors'],
    registration: ['data'],
    successRegistration: ['data'],
    failRegistration: ['errors'],
    logout: [],
    successLogout: [],
    failLogout: [],
    toggleAuthPopup: ['value'],
    toggleRegistrationPopup: ['value'],
    toggleNeedAuthPopup: ['value'],
}, { prefix: 'user/' });

export const INITIAL_STATE = {
    isFetching: false,
    data: window.authUser,
    errors: undefined,
    authPopup: false,
    registrationPopup: false,
    needAuthPopup: false,
};

export const loginVk = (state) => {
    return {...state, errors: undefined, isFetching: true}
};

export const login = (state) => {
    return {...state, errors: undefined, isFetching: true}
};

export const successLogin = (state, { data }) => {
    return {...state, ...{data}, isFetching: false}
};

export const failLogin = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
};

export const registration = (state) => {
    return {...state, errors: undefined, isFetching: true}
};

export const successRegistration = (state, { data }) => {
    return {...state, ...{data}, isFetching: false}
};

export const failRegistration = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
};

export const logout = (state) => {
    return {...state, isFetching: true}
};

export const successLogout = (state) => {
    return {...state, data: undefined, isFetching: false}
};

export const failLogout = (state) => {
    return {...state, isFetching: false}
};

export const toggleAuthPopup = (state, { value }) => {
    return {...state, authPopup: value}
};

export const toggleRegistrationPopup = (state, { value }) => {
    return {...state, registrationPopup: value}
};

export const toggleNeedAuthPopup = (state, { value }) => {
    return {...state, needAuthPopup: value}
};

export const reducer = createReducer(INITIAL_STATE, {
    [Types.LOGIN_VK]: loginVk,
    [Types.LOGIN]: login,
    [Types.SUCCESS_LOGIN]: successLogin,
    [Types.FAIL_LOGIN]: failLogin,
    [Types.REGISTRATION]: registration,
    [Types.SUCCESS_REGISTRATION]: successRegistration,
    [Types.FAIL_REGISTRATION]: failRegistration,
    [Types.LOGOUT]: logout,
    [Types.SUCCESS_LOGOUT]: successLogout,
    [Types.FAIL_LOGOUT]: failLogout,
    [Types.TOGGLE_AUTH_POPUP]: toggleAuthPopup,
    [Types.TOGGLE_REGISTRATION_POPUP]: toggleRegistrationPopup,
    [Types.TOGGLE_NEED_AUTH_POPUP]: toggleNeedAuthPopup,
});
