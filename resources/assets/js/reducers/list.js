import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
    fetchLists: ['query'],
    successFetchLists: ['data'],
    failFetchLists: ['errors'],
    addListItem: ['data'],
    successAddListItem: [],
    failAddListItem: [],
    updateListItem: ['id', 'data'],
    successUpdateListItem: ['data'],
    failUpdateListItem: ['errors'],
}, { prefix: 'list/' })

export const INITIAL_STATE = {
    isFetching: false,
    list: {data: window.lists},
    errors: undefined,
}

export const fetchLists = (state) => {
    return {...state, isFetching: true}
}

export const successFetchLists = (state, { data }) => {
    return {...state, ...{list: data}, isFetching: false}
}

export const failFetchLists = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const addListItem = (state) => {
    return {...state, isFetching: true}
}

export const successAddListItem = (state) => {
    return {...state, isFetching: false}
}

export const failAddListItem = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const updateListItem = (state) => {
    return {...state, isFetching: true}
}

export const successUpdateListItem = (state, { data }) => {
    const newState = {...state}
    for (let listKey in newState.list.data) {
        if (newState.list.data[listKey].id === data.list_id) {
            for (let key in newState.list.data[listKey].items) {
                if (newState.list.data[listKey].items[key].id === data.item_id) {
                    newState.list.data[listKey].items[key].pivot = {...data}
                }
            }
        }
    }

    return {...newState, isFetching: false}
}

export const failUpdateListItem = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.FETCH_LISTS]: fetchLists,
    [Types.SUCCESS_FETCH_LISTS]: successFetchLists,
    [Types.FAIL_FETCH_LISTS]: failFetchLists,
    [Types.ADD_LIST_ITEM]: addListItem,
    [Types.SUCCESS_ADD_LIST_ITEM]: successAddListItem,
    [Types.FAIL_ADD_LIST_ITEM]: failAddListItem,
    [Types.UPDATE_LIST_ITEM]: updateListItem,
    [Types.SUCCESS_UPDATE_LIST_ITEM]: successUpdateListItem,
    [Types.FAIL_UPDATE_LIST_ITEM]: failUpdateListItem,
})
