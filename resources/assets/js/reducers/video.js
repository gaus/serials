import { createActions, createReducer } from 'reduxsauce'

export const { Types, Creators } = createActions({
    fetchVideos: ['query'],
    successFetchVideos: ['data'],
    failFetchVideos: ['errors'],
    fetchVideo: ['id', 'query'],
    successFetchVideo: ['data'],
    failFetchVideo: ['errors'],
    watchVideo: ['data'],
    successWatchVideo: [],
    failWatchVideo: [],
    reset: [],
}, { prefix: 'video/' })

export const INITIAL_STATE = {
    isFetching: false,
    list: undefined,
    data: undefined,
    errors: undefined,
}

export const fetchVideos = (state) => {
    return {...state, isFetching: true}
}

export const successFetchVideos = (state, { data }) => {
    return {...state, ...{list: data}, isFetching: false}
}

export const failFetchVideos = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const fetchVideo = (state) => {
    return {...state, isFetching: true}
}

export const successFetchVideo = (state, { data }) => {
    return {...state, ...{data}, isFetching: false}
}

export const failFetchVideo = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const watchVideo = (state) => {
    return {...state, isFetching: true}
}

export const successWatchVideo = (state) => {
    return {...state, isFetching: false}
}

export const failWatchVideo = (state, { errors }) => {
    return {...state, ...{errors}, isFetching: false}
}

export const reset = (state) => {
    return {...INITIAL_STATE}
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.FETCH_VIDEOS]: fetchVideos,
    [Types.SUCCESS_FETCH_VIDEOS]: successFetchVideos,
    [Types.FAIL_FETCH_VIDEOS]: failFetchVideos,
    [Types.FETCH_VIDEO]: fetchVideo,
    [Types.SUCCESS_FETCH_VIDEO]: successFetchVideo,
    [Types.FAIL_FETCH_VIDEO]: failFetchVideo,
    [Types.WATCH_VIDEO]: watchVideo,
    [Types.SUCCESS_WATCH_VIDEO]: successWatchVideo,
    [Types.FAIL_WATCH_VIDEO]: failWatchVideo,
    [Types.RESET]: reset,
})
