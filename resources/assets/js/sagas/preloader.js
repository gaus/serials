import { put, call } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { Creators as Action } from '../reducers/preloader'

export function * stop () {
    yield call(delay, 200)
    yield put(Action.remove())
}
