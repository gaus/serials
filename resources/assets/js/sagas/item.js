import { put, call } from 'redux-saga/effects'
import qs from 'querystringify'
import { push } from 'react-router-redux'
import { Creators as Action } from '../reducers/item'
import { Creators as ActionPreloader } from '../reducers/preloader'

export function * fetchItems (api, {query}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getItems, query)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successFetchItems(response.data))
    } else {
        yield put(Action.failFetchItems())
    }
}

export function * fetchMoreItems (api, {query}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getItems, query)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successFetchMoreItems(response.data))
    } else {
        yield put(Action.failFetchMoreItems())
    }
}

export function * fetchFilters (api, {query}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getFilters, query)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successFetchFilters(response.data.data))
    } else {
        yield put(Action.failFetchFilters())
    }
}

export function * applyFilter ({query}) {
    yield put(push(qs.stringify(query, true)))
}

export function * fetchItem (api, {id}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getItem, id, {scenario: 'show'})
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successFetchItem(response.data.data))
    } else {
        yield put(Action.failFetchItem())
    }
}

export function * saveUserRating (api, {data}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.postUserRating, data)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successSaveUserRating(response.data.data))
    } else {
        yield put(Action.failSaveUserRating())
    }
}
