import { put, call } from 'redux-saga/effects'
import { Creators as Action } from '../reducers/video'
import { Creators as ActionPreloader } from '../reducers/preloader'

export function * fetchVideo(api, {id, query}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getItemVideo, id, query)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successFetchVideo(response.data.data))
    } else {
        yield put(Action.failFetchVideo())
    }
}

export function * fetchVideos (api, {query}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getItemVideos, query)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successFetchVideos(response.data))
    } else {
        yield put(Action.failFetchVideos())
    }
}

export function * watchVideo (api, {data}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.postUserWatch, data)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successWatchVideo(response.data))
    } else {
        yield put(Action.failWatchVideo())
    }
}
