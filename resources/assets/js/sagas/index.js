import { takeEvery, takeLatest } from 'redux-saga/effects'
import Api from '../services/api'
import { Types as PreloaderTypes } from '../reducers/preloader'
import { Types as ItemTypes } from '../reducers/item'
import { Types as VideoTypes } from '../reducers/video'
import { Types as ListTypes } from '../reducers/list'
import { Types as UserTypes } from '../reducers/user'
import { Types as SearchTypes } from '../reducers/search'
import * as preloader from './preloader'
import * as item from './item'
import * as video from './video'
import * as list from './list'
import * as user from './user'
import * as search from './search'

const api = Api.create();

export default function * sagas () {
    yield [
        takeEvery(PreloaderTypes.STOP, preloader.stop),
        takeLatest(ItemTypes.FETCH_ITEMS, item.fetchItems, api),
        takeEvery(ItemTypes.FETCH_MORE_ITEMS, item.fetchMoreItems, api),
        takeEvery(ItemTypes.FETCH_FILTERS, item.fetchFilters, api),
        takeLatest(ItemTypes.APPLY_FILTER, item.applyFilter),
        takeLatest(ItemTypes.FETCH_ITEM, item.fetchItem, api),
        takeLatest(VideoTypes.FETCH_VIDEO, video.fetchVideo, api),
        takeLatest(VideoTypes.FETCH_VIDEOS, video.fetchVideos, api),
        takeEvery(VideoTypes.WATCH_VIDEO, video.watchVideo, api),
        takeLatest(ListTypes.FETCH_LISTS, list.fetchLists, api),
        takeEvery(ListTypes.ADD_LIST_ITEM, list.addListItem, api),
        takeEvery(ListTypes.UPDATE_LIST_ITEM, list.updateListItem, api),
        takeEvery(UserTypes.LOGOUT, user.logout, api),
        takeEvery(UserTypes.LOGIN, user.login, api),
        takeEvery(UserTypes.LOGIN_VK, user.loginVk, api),
        takeEvery(UserTypes.REGISTRATION, user.registration, api),
        takeLatest(SearchTypes.SEARCH, search.search, api),
        takeLatest(SearchTypes.APPLY_SEARCH, search.applySearch),
        takeLatest(ItemTypes.SAVE_USER_RATING, item.saveUserRating, api),
    ]
}
