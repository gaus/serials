import { put, call } from 'redux-saga/effects'
import { Creators as Action } from '../reducers/list'
import { Creators as ActionItem } from '../reducers/item'
import { Creators as ActionPreloader } from '../reducers/preloader'

export function * fetchLists (api, {query}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getLists, query)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successFetchLists(response.data))
    } else {
        yield put(Action.failFetchLists())
    }
}

export function * addListItem (api, {data}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.postUserItem, data)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successAddListItem(response.data.data))
        yield put(ActionItem.successAddListItem(response.data.data))
    } else {
        yield put(Action.failAddListItem())
    }
}

export function * updateListItem (api, {id, data}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.putUserItem, id, data)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successUpdateListItem(response.data.data))
    } else {
        yield put(Action.failUpdateListItem())
    }
}
