import { put, call } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { Creators as Action } from '../reducers/search'
import { Creators as ActionPreloader } from '../reducers/preloader'

export function * search (api, {query}) {
    yield put(ActionPreloader.start())
    const response = yield call(api.getItems, query)
    yield put(ActionPreloader.stop())
    if (response.ok) {
        yield put(Action.successSearch(response.data))
    } else {
        yield put(Action.failSearch())
    }
}

export function * applySearch ({query}) {
    if (query.q) {
        yield call(delay, 500)
        yield put(Action.search(query))
    } else {
        yield put(Action.reset())
    }
}
