import { put, call } from 'redux-saga/effects'
import { Creators as Action } from '../reducers/user'
import { Creators as ActionPreloader } from '../reducers/preloader'

export function * logout (api) {
    yield put(ActionPreloader.start());
    const response = yield call(api.logout);
    yield put(ActionPreloader.stop());
    if (response.ok) {
        yield put(Action.successLogout())
    } else {
        yield put(Action.failLogout())
    }
}

export function * loginVk (api, {data}) {
    yield put(ActionPreloader.start());
    const response = yield call(api.socialAuth, {type: 'vk', data});
    yield put(ActionPreloader.stop());
    if (response.ok) {
        yield put(Action.successLogin(response.data.data));
        yield put(Action.toggleAuthPopup(false));
        yield put(Action.toggleRegistrationPopup(false))
    } else {
        yield put(Action.failLogin(
            response.status === 422
                ? response.data.errors
                : undefined
        ))
    }
}

export function * login (api, {data}) {
    yield put(ActionPreloader.start());
    const response = yield call(api.login, data);
    yield put(ActionPreloader.stop());
    if (response.ok) {
        yield put(Action.successLogin(response.data.data));
        yield put(Action.toggleAuthPopup(false))
    } else {
        yield put(Action.failLogin(
            response.status === 422
                ? response.data.errors
                : undefined
        ))
    }
}

export function * registration (api, {data}) {
    yield put(ActionPreloader.start());
    const response = yield call(api.postUser, data);
    yield put(ActionPreloader.stop());
    if (response.ok) {
        yield put(Action.successRegistration(response.data.data));
        yield put(Action.toggleRegistrationPopup(false))
    } else {
        yield put(Action.failRegistration(
            response.status === 422
                ? response.data.errors
                : undefined
        ))
    }
}
