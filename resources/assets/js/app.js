import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Router, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import { syncHistoryWithStore } from 'react-router-redux'
import Modal from 'react-modal'
import configureStore from './store'
import routes from './routes'

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

export function action(type, payload) {
    store.dispatch({type, payload})
}

Modal.setAppElement('#root');

render(
    <Provider store={store}>
        <Router history={history} routes={routes} />
    </Provider>,
    document.getElementById('root')
);

module.hot.accept();
